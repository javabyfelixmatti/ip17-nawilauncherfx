# NaWiLauncherFX #
## Bedienungshinweise ##
Die Verbindung zwischen einem PC in der Schule und einem Gerät im Schüler-WLAN verursacht durch die Firewallprobleme

## Dokumentation ##
***In der Entwicklung!!!***
Mit dem NaWiLauncher kann man ein Quiz erstellen. Im Hauptmenü kann man zwischen dem Präsentiermodus und Bearbeitungsmodus wählen. Im Bearbeitungsmodus können Fragen hinzugefügt werden, welche Bilder oder auch Videos beinhalten können. Es sind 4 Antwortmöglichkeiten einsetzbar. Man kann zudem Kategorien festlegen, wie es nach dem NaWigator Vorbild üblich ist. Nach dem Speichern kann das Quiz präsentiert werden. Bei Bedarf können die Schüler ihre Antworten über den Browser ihrer Laptops abgeben, alternativ können diese jedoch auch am Hauptrechner eingegeben werden.
### Anwendungsbereiche ###
Die Software soll in den 6. Klassen für den NaWigator-Wettbewerb genutzt werden, um die alten PowerPoint-Präsentationen zu ersetzen und somit einfacher die Quizstruktur zu verwalten, Punkte zu zählen, umfangreicher auszuwerten sowie die Abstimmung über die Netbooks der Schüler zu ermöglichen. Theoretisch kann sie auch zum Erstellen anderer Quiz (auch in anderen Fächern) benutzt werden, die Software orientiert sich aber dennoch an den Vorgaben des NaWigator-Wettbewerbs.
### Umsetzung ###
Wir verwenden die Programmiersprache Java 8 mit dem Framework JavaFX. Durch diese Programmiersprache ist die Anwendung portabel, betriebssystemunabhängig sowie zukünftig problemlos erweiterbar. Für das Starten eines Webservers werden zahlreiche vorgefertigte Klassen mitgeliefert, sodass die Schüler ihre Antworten über den Browser statt über ein Extraprogramm abgeben können, wodurch keine Administratorrechte benötigt werden bzw. keine Firewall-Probleme auftreten. 
Zur Entwicklung der Software kommt die IDE NetBeans von Oracle mit der integrierten Versionsverwaltungskontrolle "Git" zum Einsatz, um etwaige Fehler rückgängig zu machen und jeden Schritt zurück verfolgen zu können. Als Speicherort für das Repository nutzen wir eine kostenlose Webseite namens BitBucket, die es uns ermöglicht, parallel am Projekt zu arbeiten. 
Die JavaFX Scenes werden mit der Zusatzsoftware „JavaFX Scene Builder“ von Oracle erstellt.
### Zielbestimmung ###
#### Musskriterien ####
Mit Hilfe des NaWiLaunchers müssen sich eigene Quiz1 nach den Vorgaben des NaWigator-Wettbewerbs erstellen und speichern lassen. Dazu gehört, dass folgende Fragentypen unterstützt werden müssen: Einfache Auswahl unter vier Antworten, Antworten in die richtige Reihenfolge bringen, Schätzen, Textantwort. Des Weiteren müssen Medien (Bilder, Videos) angezeigt und in die Fragen eingebunden werden können. Die einzelnen Fragen bringen unterschiedliche Punktzahlen; am Ende werden die Punkte zusammengerechnet und das Quiz ausgewertet. Die Darstellung erfolgt in einer Art Präsentationsmodus im Vollbild.
#### Sollkriterien ####
Es soll mithilfe eines lokalen Webservers möglich sein, über den Browser eines Schülergerätes Informationen zum Quiz anzuzeigen und die Antwort abzugeben. Eine Zeitbegrenzung soll optional aktivierbar sein.
#### Kannkriterien ####
Wenn noch genügend Zeit ist, können weitere Funktionen wie wählbare Designs oder andere Quizmodi hinzugefügt werden. Möglicherweise könnte bei erweiterten Bildschirmen eine Referentenansicht während der Präsentation (PowerPoint-ähnlich) eingebaut werden. Das Programm könnte zudem im Hintergrund nach Updates suchen und sich nach Abfrage automatisch updaten.

Felix Kleinsteuber & Matti Frind