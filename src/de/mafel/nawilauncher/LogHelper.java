package de.mafel.nawilauncher;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Verantwortlich für Debug-Ausgaben.
 * 
 * Loggt Ausgaben als Textdatei und gibt sie über die Konsole aus.
 *
 * @author Felix, Matti
 */
public class LogHelper {
    private static final Logger LOGGER = Logger.getLogger("");
    
    static {
        LOGGER.setLevel(Level.INFO);
        try {
            FileHandler fileTxt = new FileHandler("NaWiLauncher_log.log");
            SimpleFormatter formatterTxt = new SimpleFormatter();
            fileTxt.setLevel(Level.INFO);
            fileTxt.setEncoding("UTF-8");
            fileTxt.setFormatter(formatterTxt);
            LOGGER.addHandler(fileTxt);
        } catch (IOException | SecurityException ex) {
            error(ex);
        }
    }

    public static Logger getLogger() {
        return LOGGER;
    }
    
    /**
     * Bestimmt, ob Debugausgaben geloggt werden sollen.
     *
     * @param debug
     */
    public static void setDebugMode(boolean debug) {
        LOGGER.setLevel(debug ? Level.ALL : Level.INFO);
    }
    
    /**
     * Loggt einen schwerwiegenden Fehler.
     * 
     * Logging-Level: SEVERE
     *
     * @param msg Fehlernachricht
     */
    public static void error(String msg) {
        LOGGER.severe(msg);
    }
    
    /**
     * Loggt einen schwerwiegenden Fehler.
     * 
     * Logging-Level: SEVERE
     *
     * @param t Fehlerursache
     */
    public static void error(Throwable t) {
        LOGGER.log(Level.SEVERE, t.getMessage(), t);
    }
    
    /**
     * Loggt einen schwerwiegenden Fehler.
     * 
     * Logging-Level: SEVERE
     *
     * @param msg Fehlernachricht
     * @param t Fehlerursache
     */
    public static void error(String msg, Throwable t) {
        LOGGER.log(Level.SEVERE, msg, t);
    }
    
    /**
     * Loggt eine Warnmeldung.
     * 
     * Logging-Level: WARNING
     *
     * @param msg Nachricht
     */
    public static void warning(String msg) {
        LOGGER.warning(msg);
    }
    
    /**
     * Loggt eine Warnmeldung.
     * 
     * Logging-Level: WARNING
     *
     * @param t Fehlerursache
     */
    public static void warning(Throwable t) {
        LOGGER.log(Level.WARNING, t.getMessage(), t);
    }
    
    /**
     * Loggt eine Warnmeldung.
     * 
     * Logging-Level: WARNING
     *
     * @param msg Nachricht
     * @param t Fehlerursache
     */
    public static void warning(String msg, Throwable t) {
        LOGGER.log(Level.WARNING, msg, t);
    }
    
    /**
     * Loggt eine Infonachricht.
     * 
     * Logging-Level: INFO
     *
     * @param msg Nachricht
     */
    public static void info(String msg) {
        LOGGER.info(msg);
    }
    
    /**
     * Loggt eine Debugausgabe. Hat keinen Effekt, wenn der Debug-Modus
     * deaktiviert ist.
     * 
     * Logging-Level: DEBUG
     *
     * @param msg Debugnachricht
     * @see LogHelper#setDebugMode(boolean) 
     */
    public static void debug(String msg) {
        LOGGER.log(Level.INFO, "Debug//" + msg);
    }
    
}
