/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.mafel.nawilauncher.database;

import de.mafel.nawilauncher.data.questions.Question;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author matti
 */
public class Database {
    
    private static ObservableList<Question> questions = FXCollections.observableArrayList();

    public static ObservableList<Question> getQuestions() {
        return questions;
    }

    public static void setQuestions(ObservableList<Question> questions) {
        Database.questions = questions;
    }
    
    public static void addQuestion(Question q) {
        questions.add(q);
    }
    
    
    
    
    
    
}
