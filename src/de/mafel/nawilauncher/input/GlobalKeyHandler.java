package de.mafel.nawilauncher.input;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.fileio.NWQFormat;
import de.mafel.nawilauncher.scenes.Scenes;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.function.Consumer;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Zuständig für das Abfangen von Tastatureingaben. Folgende globale
 * Tastenkombinationen stehen zur Verfügung:
 * Strg+S: Speichern
 * Strg+Umschalt+S: Speichern unter
 * Strg+O: Öffnen
 * F11: Vollbild
 * F5: Szene neu laden (Behebt eventuelle Grafikfehler)
 * F1: Hilfe über Webinterface anfordern
 *
 * @author Felix
 */
public class GlobalKeyHandler implements EventHandler<KeyEvent> {
    
    public static final KeyCombination CTRL_S = new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN),
            CTRL_O = new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN),
            CTRL_SHIFT_S = new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN);
    
    private final ArrayList<Consumer<KeyEvent>> preEventHandlers = new ArrayList<>(), postEventHandlers = new ArrayList<>();
    
    /**
     * Registriert einen EventHandler (Consumer), der nach jedem Tastendruck,
     * jedoch vor Verarbeitung durch den GlobalKeyHandler aufgerufen wird.
     *
     * @param handler EventHandler
     */
    public void addPreEventHandler(Consumer<KeyEvent> handler) {
        preEventHandlers.add(handler);
    }
    
    /**
     * Registriert einen EventHandler (Consumer), der nach jedem Tastendruck,
     * nach Verarbeitung durch den GlobalKeyHandler aufgerufen wird.
     *
     * @param handler EventHandler
     */
    public void addPostEventHandler(Consumer<KeyEvent> handler) {
        postEventHandlers.add(handler);
    }

    private boolean blockFullscreen = false;
    
    @Override
    public void handle(KeyEvent event) {
        //Event Handler aufrufen
        preEventHandlers.forEach(c -> c.accept(event));
        
        if(CTRL_SHIFT_S.match(event)) {
            //Speichern unter
            File f = GUtils.getFileChooser("Quiz speichern").showSaveDialog(Main.getMainScene().getWindow());
            if(f != null) NWQFormat.saveFile(f);
        } else if(CTRL_S.match(event)) {
            //Speichern
            try {
                NWQFormat.saveAndOverwrite();
            } catch(IllegalStateException ex) {
                //Keine Datei geöffnet; Speichern unter.
                File f = GUtils.getFileChooser("Quiz speichern").showSaveDialog(Main.getMainScene().getWindow());
                if(f != null) NWQFormat.saveFile(f);
            }
        } else if(CTRL_O.match(event)) {
            //Öffnen
            File f = GUtils.getFileChooser("Quiz öffnen").showOpenDialog(Main.getMainScene().getWindow());
            if(f != null) {
                NWQFormat.openFile(f);
            }
        } else if(event.getCode() == KeyCode.F11) {
            //Vollbild
            Stage s = Main.getStage();
            if(!blockFullscreen && !s.isFullScreen()) {
                blockFullscreen = true;
                s.setFullScreen(true);
            } else blockFullscreen = false;
        } else if(event.getCode() == KeyCode.F5) {
            //Szene neu laden
            Scenes.reloadScene();
        } else if(event.getCode() == KeyCode.F1) {
            //Hilfe anfordern
            LogHelper.error("Hilfe! Bitten um Aufmerksamkeit des IT-Teams.");
        }
        
        //EventHandler aufrufen
        postEventHandlers.forEach(c -> c.accept(event));
    }

}
