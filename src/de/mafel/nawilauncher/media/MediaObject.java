package de.mafel.nawilauncher.media;

import javafx.stage.FileChooser;

/**
 *
 * @author matti
 */
public abstract class MediaObject {
    
    public static final FileChooser.ExtensionFilter[] SUPPORTED_MEDIA_FILES_FILTERS = new FileChooser.ExtensionFilter[]{
        new FileChooser.ExtensionFilter("Unterstützte Bilddateien (*.png, *.jpg, *.jpeg, *.gif, *.bmp)", "*.png", "*.jpg", "*.jpeg", "*.gif", "*.bmp"),
        new FileChooser.ExtensionFilter("Unterstützte Videodateien (*.mp4, *.m4v, *.fxm, *.flv)", "*.mp4", "*.m4v", "*.fxm", "*.flv"),
        new FileChooser.ExtensionFilter("Unterstützte Audiodateien (*.mp3, *.wav, *.aif, *.aiff, *.m4a)", "*.aif", "*.aiff", "*.mp3", "*.m4a", "*.wav")
    };
    
    private boolean loaded;
    private String mediaPath;
    
    /**
     * Erstellt ein neues MediaObject mit dem übergebenen Pfad.
     *
     * @param mediaPath Der Medienpfad innerhalb des Medienordners
     * @param name Bezeichner
     * @param load Bestimmt, ob dynamisches Laden aktiviert werden soll.
     * @see MediaObject MediaObject
     */
    public MediaObject(String mediaPath, String name, boolean load) {
        this.mediaPath = mediaPath;
        loaded = false;
        if(load) load();
    }

    /**
     * Gibt den zugewiesenen Medienpfad zurück.
     *
     * @return Medienpfad
     */
    public String getMediaPath() {
        return mediaPath;
    }

    protected void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    /**
     * Gibt true zurück, wenn das Bild am gegebenen Medienpfad bereits geladen wurde.
     * 
     * Gibt immer true zurück, wenn dynamisches Laden deaktiviert ist.
     *
     * @return true, wenn das Bild geladen wurde
     * @see MediaObject#load() MediaObject.load()
     */
    public boolean isLoaded() {
        return loaded;
    }
    
    /**
     * Lädt das Medium aus dem gespeicherten Medienpfad.
     * 
     * Das Medium wird nur geladen, wenn es noch nicht zuvor geladen wurde oder
     * das aktuelle Medium null entspricht.
     * 
     * Wird im Konstruktor aufgerufen, sofern dynamisches Laden deaktiviert ist.
     *
     */
    public abstract void load();
    
}
