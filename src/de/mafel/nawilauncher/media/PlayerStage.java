package de.mafel.nawilauncher.media;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizState;
import static java.lang.Math.floor;
import static java.lang.String.format;
import java.util.Objects;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author matti
 */
public final class PlayerStage extends Stage {

    Duration duration;
    Label time;
    Media media;
    MediaPlayer mediaPlayer;
    MediaView view;
    Image playButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Play.png"));
    Image pauseButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Pause.png"));
    Image lastButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/End.png"));
    Image forwardButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Fast Forward.png"));
    Image reloadButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Repeat.png"));
    Image backwardButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Rewind.png"));
    Image firstButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Skip to Start.png"));
    Image stopButtonImage = new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Stop.png"));
    
    StackPane root = new StackPane();
    HBox toolbar;
    Button playButton;
    Scene player;
    private final BooleanProperty videoHasEnded = new SimpleBooleanProperty(false);
    boolean newVideo = false;

    public boolean isVideoHasEnded() {
        return videoHasEnded.get();
    }

    public void setVideoHasEnded(boolean value) {
        videoHasEnded.set(value);
    }

    public BooleanProperty videoHasEndedProperty() {
        return videoHasEnded;
    }
    
    public PlayerStage() {
        root.setAlignment(Pos.BOTTOM_CENTER);
        player = new Scene(root);
        videoHasEndedProperty().addListener((o, v1, v2) -> {
            if(!Objects.equals(v2, v1) && Navigator.getQuizState() != QuizState.EXPLANATION) Navigator.setQuizState(v2 ? QuizState.QUESTION : QuizState.CATEGORIES);
        });
    }

    public void setVideo(VideoObject video) {
        media = video.getVideo();
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setOnEndOfMedia(() -> {
            this.close();
            newVideo = false;
            setVideoHasEnded(true);
        });
        toolbar = getToolbar();
        view = new MediaView();
        view.setMediaPlayer(mediaPlayer);
        view.setPreserveRatio(true);
        view.setLayoutX(0);
        view.setLayoutY(0);
        view.setX(0);
        view.setY(0);
        view.fitHeightProperty().bind(root.heightProperty());
        view.fitWidthProperty().bind(root.widthProperty());
        root.getChildren().addAll(view, toolbar);
        newVideo = true;
    }
    
    public void openVideo() {
        Navigator.setQuizState(QuizState.CATEGORIES);
        this.setScene(player);
        this.setFullScreen(true);
        this.setVideoHasEnded(false);

        //KeyEvent
        addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            switch(event.getCode()) {
                case ESCAPE:
                    this.close();
                    mediaPlayer.stop();
                    setVideoHasEnded(true);
                    newVideo = false;
                    break;
                case SPACE: togglePlayState(true); event.consume();  break;
                case RIGHT: mediaPlayer.seek(mediaPlayer.getCurrentTime().add(new Duration(5000))); event.consume(); break;
                case LEFT: mediaPlayer.seek(mediaPlayer.getCurrentTime().subtract(new Duration(5000))); event.consume(); break;
            }
        });
        mediaPlayer.play();
        updatePlayState();
        this.show();
        toolbar.setLayoutY(this.getHeight() - 30);
    }
    
    public boolean isVideoSet() {
        return newVideo;
    }
    
    private HBox getToolbar() {
        //Buttons
        playButton = getPlayButton();
        
        Button stopButton = getStopButton();

        Button forwardButton = getForwardButton();

        Button backwardButton = getBackwardButton();

        Button firstButton = getFirstButton();

        Button lastButton = getLastButton();

        Button reloadButton = getReloadButton();

        time = getTimeLabel();

        //Toolbar
        HBox toolbar = new HBox(firstButton, backwardButton, playButton, stopButton, forwardButton, lastButton, reloadButton, time);
        toolbar.setBackground(new Background(new BackgroundFill(Color.web("#000000", 0.3), new CornerRadii(5, 5, 0, 0, false), Insets.EMPTY)));
        toolbar.setPrefHeight(1200);
        toolbar.setMaxHeight(120);
        toolbar.setPrefWidth(1000);
        toolbar.setMaxWidth(1000);
        toolbar.setAlignment(Pos.CENTER);
        toolbar.setSpacing(5);
        return toolbar;
    }
    
    
    private Button getPlayButton() {
        Button playButton = new Button();
        playButton.setGraphic(new ImageView(playButtonImage));
        playButton.setStyle("-fx-background-color: Black");
        playButton.setFocusTraversable(true);
        
        playButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            playButton.setStyle("-fx-background-color: Black");
            playButton.setStyle("-fx-body-color: Black");
        });
            playButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            playButton.setStyle("-fx-background-color: Black");
        });
        
        playButton.setOnAction(event -> togglePlayState(true));
        return playButton;
    }
    
    private Button getForwardButton() {
        Button forwardButton = new Button();
        forwardButton.setGraphic(new ImageView(forwardButtonImage));
        forwardButton.setStyle("-fx-background-color: Black");
        forwardButton.setFocusTraversable(true);
        
        forwardButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            forwardButton.setStyle("-fx-background-color: Black");
            forwardButton.setStyle("-fx-body-color: Black");
        });
            forwardButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            forwardButton.setStyle("-fx-background-color: Black");
        });
        
        
        forwardButton.setOnAction(event -> {
            mediaPlayer.seek(mediaPlayer.getCurrentTime().add(new Duration(5000)));
        });
        return forwardButton;
    }
    
    private Button getBackwardButton() {
        Button backwardButton = new Button();
        backwardButton.setGraphic(new ImageView(backwardButtonImage));
        backwardButton.setStyle("-fx-background-color: Black");
        backwardButton.setFocusTraversable(true);
        
        backwardButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            backwardButton.setStyle("-fx-background-color: Black");
            backwardButton.setStyle("-fx-body-color: Black");
        });
            backwardButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            backwardButton.setStyle("-fx-background-color: Black");
        });
        
        
        backwardButton.setOnAction(event -> {
            mediaPlayer.seek(mediaPlayer.getCurrentTime().subtract(new Duration(5000)));
        });
        return backwardButton;
    }
    
    private Button getFirstButton() {
        Button firstButton = new Button();
        firstButton.setGraphic(new ImageView(firstButtonImage));
        firstButton.setStyle("-fx-background-color: Black");
        firstButton.setFocusTraversable(true);
        
        firstButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            firstButton.setStyle("-fx-background-color: Black");
            firstButton.setStyle("-fx-body-color: Black");
        });
            firstButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            firstButton.setStyle("-fx-background-color: Black");
        });
        
        
        firstButton.setOnAction(event -> {
            mediaPlayer.seek(mediaPlayer.getStartTime());
            mediaPlayer.play();
            updatePlayState();
        });
        return firstButton;
    }
    
    private Button getLastButton() {
        Button lastButton = new Button();
        lastButton.setGraphic(new ImageView(lastButtonImage));
        lastButton.setStyle("-fx-background-color: Black");
        lastButton.setFocusTraversable(true);
        
        lastButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            lastButton.setStyle("-fx-background-color: Black");
            lastButton.setStyle("-fx-body-color: Black");
        });
            lastButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            lastButton.setStyle("-fx-background-color: Black");
        });
        
        lastButton.setOnAction(event -> {
            mediaPlayer.seek(mediaPlayer.getTotalDuration());
            mediaPlayer.stop();
            updatePlayState();
        });
        return lastButton;
    }
    
    private Button getReloadButton() {
        Button reloadButton = new Button();
        reloadButton.setGraphic(new ImageView(reloadButtonImage));
        reloadButton.setFocusTraversable(true);
        reloadButton.setStyle("-fx-background-color: Black");
        
        reloadButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            reloadButton.setStyle("-fx-background-color: Black");
            reloadButton.setStyle("-fx-body-color: Black");
        });
            reloadButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            reloadButton.setStyle("-fx-background-color: Black");
        });
        
        reloadButton.setOnAction(event -> {
            mediaPlayer.seek(mediaPlayer.getStartTime());
            updatePlayState();
        });
        return reloadButton;
    }
    
    private Label getTimeLabel() {
        time = new Label();
        time.setTextFill(Color.YELLOW);
        time.setStyle("-fx-font-size: 20");
        time.setPrefWidth(160);

        mediaPlayer.currentTimeProperty().addListener((Observable ov) -> {
            updateValues();
        });

        mediaPlayer.setOnReady(() -> {
            duration = mediaPlayer.getMedia().getDuration();
            updateValues();
        });
        return time;
    }
    
    private Button getStopButton() {
        Button stopButton = new Button();
        stopButton.setGraphic(new ImageView(stopButtonImage));
        stopButton.setStyle("-fx-background-color: Black");
        stopButton.setFocusTraversable(true);
        
        stopButton.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
            stopButton.setStyle("-fx-background-color: Black");
            stopButton.setStyle("-fx-body-color: Black");
        });
            stopButton.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
            stopButton.setStyle("-fx-background-color: Black");
        });
            
        stopButton.setOnAction(event -> {
            mediaPlayer.stop();
            this.close();
            newVideo = false;
            setVideoHasEnded(true);
        });
        return stopButton;
    }
    
    private void updatePlayState() {
        playButton.setGraphic(new ImageView(mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING) ? playButtonImage : pauseButtonImage));
        
    }
    
    private void togglePlayState(boolean updateGraphics) {
        switch(mediaPlayer.getStatus()) {
            case PLAYING: mediaPlayer.pause();  break;
            case PAUSED: case READY: case STALLED: case STOPPED: mediaPlayer.play(); break;
            default: LogHelper.warning("Warnung: Es wurde versucht, den Video Player zu starten/zu pausieren, während dieser nicht verfügbar war.");
        }
        if(updateGraphics) updatePlayState();
    }
    
    protected void updateValues() {
        if (time != null) {
            Platform.runLater(() -> {
                Duration currentTime = mediaPlayer.getCurrentTime();
                time.setText(formatTime(currentTime, duration));
            });
        }
    }

    private static String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int) floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60 - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int) floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 - durationMinutes * 60;
            if (durationHours > 0) {
                return format("%d:%02d:%02d/%d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds, durationHours, durationMinutes, durationSeconds);
            } else {
                return format("%02d:%02d/%02d:%02d", elapsedMinutes, elapsedSeconds, durationMinutes, durationSeconds);
            }
        } else if (elapsedHours > 0) {
            return format("%d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds);
        } else {
            return format("%02d:%02d", elapsedMinutes, elapsedSeconds);
        }
    }
}
