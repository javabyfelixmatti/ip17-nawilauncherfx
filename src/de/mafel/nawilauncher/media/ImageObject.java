package de.mafel.nawilauncher.media;

import de.mafel.nawilauncher.fileio.MediaStore;
import javafx.scene.image.Image;

/**
 * Repräsentiert ein Bildobjekt, welches einer Quizfrage hinzugefügt werden kann.
 * 
 * Unterstützt dynamisches Laden; ist dieses aktiviert, wird das enthaltene 
 * <code>Image</code>-Objekt erst bei Anfrage geladen.
 *
 * @see ImageObject#ImageObject(java.lang.String, boolean) ImageObject()
 * @see ImageObject#getImage() ImageObject.getImage()
 * @see ImageObject#load() ImageObject.load()
 * @author Matti, Felix
 */
public class ImageObject extends MediaObject {
    
    private Image image;
    
    /**
     * Erstellt ein neues ImageObject mit dem übergebenen Pfad.
     *
     * @param mediaPath Der Medienpfad innerhalb des Medienordners
     * @param name Bezeichner
     * @param load Bestimmt, ob dynamisches Laden aktiviert werden soll.
     * @see ImageObject ImageObject
     */
    public ImageObject(String mediaPath, String name, boolean load) {
        super(mediaPath, name, load);
    }

    /**
     * Gibt das Bild, welches aus dem zugewiesenen Medienpfad ausgelesen wurde, zurück.
     *
     * @return Bild
     */
    public Image getImage() {
        if(!isLoaded()) load();
        return image;
    }

    /**
     * Ändert das zugewiesene Bild manuell. Sollte im Normalfall nicht verwendet
     * werden, insbesondere nicht, wenn dynamisches Laden aktiviert ist.
     *
     * @param image Bild
     * @deprecated Nicht zur Verwendung empfohlen
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * Lädt das Bild aus dem gespeicherten Medienpfad.
     * 
     * Das Bild wird nur geladen, wenn es noch nicht zuvor geladen wurde oder
     * das aktuelle Bild null entspricht.
     * 
     * Wird im Konstruktor aufgerufen, sofern dynamisches Laden deaktiviert ist.
     *
     */
    @Override
    public void load() {
        if(!isLoaded() || image == null) {
            image = new Image(MediaStore.getURL(getMediaPath()).toExternalForm(), true);
            setLoaded(true);
        }
    }
    
}
