/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.mafel.nawilauncher.media;

import de.mafel.nawilauncher.fileio.MediaStore;
import javafx.scene.media.Media;

/**
 *
 * @author matti
 */
public class VideoObject extends MediaObject {
    private Media video;
    
    /**
     * Erstellt ein neues VideoObject mit dem übergebenen Pfad.
     *
     * @param mediaPath Der Medienpfad innerhalb des Medienordners
     * @param name Bezeichner
     * @param load Bestimmt, ob dynamisches Laden aktiviert werden soll.
     * @see VideoObject VideoObject
     */
    public VideoObject(String mediaPath, String name, boolean load) {
        super(mediaPath, name, load);
    }

    /**
     * Gibt das zugehörige Video zurück.
     *
     * @return Video
     */
    public Media getVideo() {
        if(!isLoaded()) load();
        return video;
    }

    /**
     * Ändert das zugewiesene Video manuell. Sollte im Normalfall nicht verwendet
     * werden, insbesondere nicht, wenn dynamisches Laden aktiviert ist.
     *
     * @param video Neues Video
     * @deprecated Nicht zur Verwendung empfohlen
     */
    public void setVideo(Media video) {
        this.video = video;
    }

    /**
     * Lädt das Video aus dem gespeicherten Medienpfad.
     * 
     * Das Video wird nur geladen, wenn es noch nicht zuvor geladen wurde oder
     * das aktuelle Video null ist.
     * 
     * Wird im Konstruktor aufgerufen, sofern dynamisches Laden deaktiviert ist.
     *
     */
    @Override
    public void load() {
        if(!isLoaded() || video == null) {
            video = new Media(MediaStore.getURL(getMediaPath()).toExternalForm());
            setLoaded(true);
        }
    }
}
