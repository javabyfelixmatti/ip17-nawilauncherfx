package de.mafel.nawilauncher.media;

import de.mafel.nawilauncher.fileio.MediaStore;
import javafx.scene.media.AudioClip;

/**
 * Repräsentiert ein Soundobjekt, welches einer Quizfrage hinzugefügt werden kann.
 * 
 * Unterstützt dynamisches Laden; ist dieses aktiviert, wird das enthaltene 
 * <code>Image</code>-Objekt erst bei Anfrage geladen.
 *
 * @see ImageObject#ImageObject(java.lang.String, boolean) ImageObject()
 * @see ImageObject#getImage() ImageObject.getImage()
 * @see ImageObject#load() ImageObject.load()
 * @author Matti, Felix
 */
public class SoundObject extends MediaObject {
    private AudioClip sound;
    
    /**
     * Erstellt ein neues ImageObject mit dem übergebenen Pfad.
     *
     * @param mediaPath Der Medienpfad innerhalb des Medienordners
     * @param name Bezeichner
     * @param load Bestimmt, ob dynamisches Laden aktiviert werden soll.
     * @see ImageObject ImageObject
     */
    public SoundObject(String mediaPath, String name, boolean load) {
        super(mediaPath, name, load);
    }

    /**
     * Gibt den Sound, welchen aus dem zugewiesenen Medienpfad ausgelesen wurde, zurück.
     *
     * @return Sound
     */
    public AudioClip getSound() {
        if(!isLoaded()) load();
        return sound;
    }

    /**
     * Ändert den zugewiesenen Sound manuell. Sollte im Normalfall nicht verwendet
     * werden, insbesondere nicht, wenn dynamisches Laden aktiviert ist.
     *
     * @param sound
     * @deprecated Nicht zur Verwendung empfohlen
     */
    public void setSound(AudioClip sound) {
        this.sound = sound;
    }

    /**
     * Lädt den Sound aus dem gespeicherten Medienpfad.
     * 
     * Der Sound wird nur geladen, wenn es noch nicht zuvor geladen wurde oder
     * der aktuelle Sound null entspricht.
     * 
     * Wird im Konstruktor aufgerufen, sofern dynamisches Laden deaktiviert ist.
     *
     */
    @Override
    public void load() {
        if(!isLoaded() || sound == null) {
            sound = new AudioClip(MediaStore.getURL(getMediaPath()).toExternalForm());
            setLoaded(true);
        }
    }
    
}
