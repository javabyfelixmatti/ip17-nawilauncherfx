package de.mafel.nawilauncher.net;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class NetErrorHandler extends Handler {
    
    private NetErrorWebsocketServer server;
    
    public NetErrorHandler() throws UnknownHostException {
        this.server = new NetErrorWebsocketServer();
        this.server.start();
    }
    
    /**
     * Fährt den für das Logging zuständigen WebSocketServer herunter.
     * Die gesamte Instanz kann im Anschluss nicht mehr verwendet werden.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void shutdown() throws IOException, InterruptedException {
        server.stop();
    }

    @Override
    public void publish(LogRecord record) {
        if(!isLoggable(record)) return;
        JSONArray arr = new JSONArray();
        JSONObject json = new JSONObject();
        json.put("t", record.getLevel().getName());
        json.put("m", getFormatter().format(record));
        arr.put(json);
        server.sendMessage(arr);
    }

    @Override
    public void flush() {
        //ignore
    }

    @Override
    public void close() throws SecurityException {
        
    }

}
