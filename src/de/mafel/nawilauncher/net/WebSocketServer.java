package de.mafel.nawilauncher.net;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.questions.EstimateQuestion;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.questions.SequenceQuestion;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.scenes.components.ProgressDialog;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Task;
import javafx.scene.paint.Color;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.json.JSONArray;

/**
 *
 * @author Felix
 */
public class WebSocketServer extends org.java_websocket.server.WebSocketServer {
    
    private final HashMap<WebSocket, Team> clients;
    private final HashMap<InetAddress, Team> ipMappings;
    private final HashMap<InetAddress, WebMethod> methods;
    private final HashMap<WebSocket, Object> individualAnswers;
    private WebSocketRequestHandler requestHandler;
    
    public WebSocketServer() {
        super(new InetSocketAddress(1079));
        clients = new HashMap<>();
        ipMappings = new HashMap<>();
        methods = new HashMap<>();
        individualAnswers = new HashMap<>();
        this.requestHandler = new WebSocketRequestHandler(this);
        Navigator.quizStateProperty().addListener(this::quizStateChanged);
        TeamManager.getTeams().addListener((ListChangeListener.Change<? extends Team> c) -> {
            while(c.next()) {
                final List<? extends Team> tr = c.getRemoved();
                //Zuweisung en an entferntes Team ermitteln
                WebSocket[] removedClients = clients.entrySet()
                        .stream()
                        .filter(e -> tr.contains(e.getValue()))
                        .map(e -> e.getKey())
                        .toArray(WebSocket[]::new);
                //Zuordnung entfernen; zur Auswahl eines neuen Teams auffordern
                for(WebSocket removedClient : removedClients) {
                    removedClient.send("m('Dein Team wurde entfernt!');");
                    clients.put(removedClient, null);
                    sendActiveScreen(removedClient);
                }
                //Zuordnungen in ipMappings entfernen
                ipMappings.entrySet().removeIf(e -> tr.contains(e.getValue()));
            }
        });
    }
    
    public void broadcastMessage(String message) {
        clients.forEach((client, team) -> {
            if(client.isOpen()) client.send(message);
        });
    }
    
    public void broadcastMessages(String teamMessage, String individualMessage) {
        clients.forEach((client, team) -> {
            if(client.isOpen()) client.send(methods.get(client.getRemoteSocketAddress().getAddress()) == WebMethod.TEAM ? teamMessage : individualMessage);
        });
    }
    
    public void broadcastMessageIfAssigned(String message) {
        clients.entrySet().stream()
                .filter(entry -> entry.getKey().isOpen() && entry.getValue() != null)
                .forEach(entry -> entry.getKey().send(message));
    }
    
    public HashMap<WebSocket, Team> getClients() {
        return clients;
    }
    
    public HashMap<InetAddress, Team> getIPMappings() {
        return ipMappings;
    }
    
    public Team getTeamForClient(WebSocket client) {
        return clients.get(client);
    }
    
    public synchronized void setClientTeam(WebSocket client, Team t) {
        clients.put(client, t);
    }
    
    public Team getTeamForIP(InetAddress ip) {
        return ipMappings.get(ip);
    }
    
    public synchronized void setIPTeam(InetAddress ip, Team t) {
        ipMappings.put(ip, t);
    }
    
    public synchronized void setIPMethod(InetAddress ip, WebMethod m) {
        methods.put(ip, m);
    }
    
    public WebMethod getMethodForIP(InetAddress ip) {
        return methods.get(ip);
    }
    
    public void kickIndividualClients() {
        for(InetAddress ip : methods.entrySet()
                .stream()
                .filter(e -> e.getValue() == WebMethod.INDIVIDUAL)
                .map(e -> e.getKey())
                .toArray(InetAddress[]::new)) {
            methods.remove(ip);
            ipMappings.remove(ip);
            try {
                sendActiveScreen(clients.keySet().stream().filter(c -> c.getRemoteSocketAddress().getAddress().equals(ip)).findAny().get());
            } catch (NoSuchElementException e) { }
        }
    }
    
    public void resetIndividualAnswers() {
        individualAnswers.clear();
    }
    
    public void setIndividualAnswer(WebSocket client, Object answer) {
        individualAnswers.put(client, answer);
    }
    
    public boolean hasAnsweredIndividual(WebSocket client) {
        return individualAnswers.containsKey(client);
    }
    
    public Object getIndividualAnswer(WebSocket client) {
        return individualAnswers.get(client);
    }
    
    private void quizStateChanged(ObservableValue<? extends QuizState> observable, QuizState oldValue, QuizState newValue) {
        switch(newValue) {
            case NOT_STARTED:
                broadcastMessage("m('Das Quiz wurde beendet.');c('');");
                break;
            case SETTINGS:
                broadcastMessages("m('Ihr dürft nun eure Teams auswählen.');c('teaminput');", "c('');");
                break;
            case CATEGORIES:
                broadcastMessageIfAssigned("c('');");
                clients.entrySet().stream()
                        .filter(entry -> entry.getKey().isOpen() && entry.getValue() == null)
                        .forEach(entry -> sendActiveScreen(entry.getKey()));
                break;
            case QUESTION:
                if(Navigator.getActiveQuestion().getCategory().getType() && QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.MULTIPLE_ANSWERS_PER_TEAM)
                    clients.entrySet().stream()
                        .filter(entry -> entry.getKey().isOpen() && entry.getValue() != null)
                        .forEach((e) -> Navigator.getActiveQuestion().sendToClient(e.getKey()));
                else
                    clients.entrySet().stream()
                        .filter(entry -> entry.getKey().isOpen() && entry.getValue() != null && methods.get(entry.getKey().getRemoteSocketAddress().getAddress()) == WebMethod.TEAM)
                        .forEach((e) -> Navigator.getActiveQuestion().sendToClient(e.getKey()));
                break;
            case EXPLANATION:
                broadcastMessageIfAssigned("c('');");
                break;
            case FINISHED:
                broadcastMessage("m('Das Quiz ist beendet!');c('');");
                break;
            default:
                broadcastMessage("m('Ein unbekannter Zustand ist aufgetreten.');c('');");
        }
        if(oldValue == QuizState.SETTINGS && newValue == QuizState.CATEGORIES) broadcastMessage("m('Das Quiz wurde gestartet!');");
    }
    
    public void sendActiveScreen(WebSocket client) {
        QuizState state = Navigator.getQuizState();
        LogHelper.debug("Sende Quiz State " + state.name());
        //Methodenauswahl
        WebMethod method = methods.get(client.getRemoteSocketAddress().getAddress());
        if(method == null) {
            client.send("c('method');" + (QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.MULTIPLE_ANSWERS_PER_TEAM ? "$('#ind-input').show()" : "$('#ind-input').hide()"));
            return;
        }
        //Teamauswahl
        Team team = clients.get(client);
        if(team == null) {
            if(method == WebMethod.TEAM && state == QuizState.SETTINGS) {
                //Neues Team erstellen
                client.send("c('teaminput');tn('',255,255,255);");
            } else if(state == QuizState.SETTINGS || state == QuizState.NOT_STARTED) {
                //Warten
                client.send("c('');m('Bitte warte, bis das Spiel angefangen hat.');");
            } else {
                //Teamauswahl
                client.send("teamchoose(" + new JSONArray(TeamManager.getTeams().stream().map(Team::getName).toArray()).toString() + ");");
            }
            return;
        }
        switch(Navigator.getQuizState()) {
            case NOT_STARTED:
                client.send("m('Das Quiz wurde noch nicht gestartet.');c('');");
                break;
            case SETTINGS:
                Color teamColor = team.getColor();
                client.send("c('teaminput');tn('" + team.getName() + "'," + ((int) (teamColor.getRed() * 255)) + "," + ((int) (teamColor.getGreen() * 255)) + "," + ((int) (teamColor.getBlue() * 255)) + ");");
                break;
            case CATEGORIES:
                client.send("c('');");
                break;
            case QUESTION:
                if(methods.get(client.getRemoteSocketAddress().getAddress()) == WebMethod.TEAM || (Navigator.getActiveQuestion().getCategory().getType() && QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.MULTIPLE_ANSWERS_PER_TEAM))
                    Navigator.getActiveQuestion().sendToClient(client);
                else
                    client.send("c('');");
                break;
            case EXPLANATION:
                client.send("c('');");
                break;
            case FINISHED:
                client.send("m('Das Quiz ist beendet!');c('');");
                break;
            default:
                client.send("m('Ein unbekannter Zustand ist aufgetreten.');c('');");
        }
    }
    
    public void sendScore(Team team) {
        getClientsForTeam(team).forEach(this::sendScore);
    }
    
    public void sendScore(WebSocket client) {
        if(QuizData.isWebDisplayScore()) client.send("s(" + getTeamForClient(client).getScore() + ");");
    }
    
    /**
     * Gibt alle WebSocket Clients zurück, die aktuell geöffnet und mit dem übergebenen Team assoziiert sind.
     *
     * @param team Das gesuchte Team
     * @return Zugehörige WebSocket Clients
     */
    public List<WebSocket> getClientsForTeam(Team team) {
        ArrayList<WebSocket> clientsForTeam = new ArrayList<>();
        clients.entrySet().stream().filter(entry -> entry != null && entry.getKey() != null && entry.getValue() != null && entry.getKey().isOpen() && entry.getValue().equals(team)).forEach(entry -> clientsForTeam.add(entry.getKey()));
        return clientsForTeam;
    }
    
    /**
     * Gibt alle WebSocket Clients der Publikumsspieler zurück, die aktuell
     * geöffnet und mit dem übergebenen Team assoziiert sind.
     *
     * @param team Das gesuchte Team
     * @return Zugehörige WebSocket Clients
     */
    public List<WebSocket> getIndividualClientsForTeam(Team team) {
        ArrayList<WebSocket> clientsForTeam = new ArrayList<>();
        clients.entrySet()
                .stream()
                .filter(
                        entry -> entry != null &&
                                entry.getKey() != null &&
                                entry.getValue() != null &&
                                entry.getKey().isOpen() &&
                                entry.getValue().equals(team) &&
                                methods.get(entry.getKey().getRemoteSocketAddress().getAddress()) == WebMethod.INDIVIDUAL
                        )
                .forEach(entry -> clientsForTeam.add(entry.getKey()));
        return clientsForTeam;
    }
    
    /**
     * Gibt alle WebSocket Clients der Teamspieler zurück, die aktuell
     * geöffnet und mit dem übergebenen Team assoziiert sind.
     *
     * @param team Das gesuchte Team
     * @return Zugehörige WebSocket Clients
     */
    public List<WebSocket> getTeamClientsForTeam(Team team) {
        ArrayList<WebSocket> clientsForTeam = new ArrayList<>();
        clients.entrySet()
                .stream()
                .filter(
                        entry -> entry != null &&
                                entry.getKey() != null &&
                                entry.getValue() != null &&
                                entry.getKey().isOpen() &&
                                entry.getValue().equals(team) &&
                                methods.get(entry.getKey().getRemoteSocketAddress().getAddress()) == WebMethod.TEAM
                        )
                .forEach(entry -> clientsForTeam.add(entry.getKey()));
        return clientsForTeam;
    }
    
    public void forceCollectAllAnswers(Runnable finished) {
        ProgressDialog dialog = new ProgressDialog("Sammle Antworten...");
        Task<Void> task = new Task<Void>() {
            
            @Override
            protected Void call() throws Exception {
                Question activeQuestion = Navigator.getActiveQuestion();
                String collectOrder;
                if(activeQuestion instanceof TextQuestion) collectOrder = "txtf();";
                else if(activeQuestion instanceof SequenceQuestion) collectOrder = "seqf();";
                else if(activeQuestion instanceof EstimateQuestion) collectOrder = "estf();";
                else { //ABCDQuestion: Antworten werden live bei Auswahl einer Antwort übertragen
                    finished.run();
                    return null;
                }
                clients.entrySet().stream().forEach(e -> {
                    Team t = e.getValue();
                    WebSocket ws = e.getKey();
                    if(t == null || ws == null || !ws.isOpen()) return;
                    ws.send(collectOrder);
                    for(int i = 0; i < 40 && !t.hasAnswered(); i++) try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        LogHelper.error(ex);
                    }
                });
                TeamManager.getTeams().forEach(t -> System.out.println(t.getAnswer()));
                finished.run();
                return null;
            }
            
        };
        dialog.activate(task);
        new Thread(task, "AnswerCollector").start();
    }

    @Override
    public void onOpen(final WebSocket conn, ClientHandshake handshake) {
        LogHelper.info("Verbunden mit " + conn.getRemoteSocketAddress().getAddress().getHostAddress());
        synchronized(clients) {
            InetAddress ip = conn.getRemoteSocketAddress().getAddress();
            Entry<InetAddress, Team> oldClient = ipMappings
                    .entrySet()
                    .stream()
                    .filter(
                            entry -> entry.getKey() != null && 
                            entry.getKey().equals(ip) && 
                            entry.getValue() != null
                    )
                    .findAny()
                    .orElse(null);
            Entry<InetAddress, WebMethod> oldMethod = methods
                    .entrySet()
                    .stream()
                    .filter(
                            entry -> entry.getKey() != null && 
                            entry.getKey().equals(ip) && 
                            entry.getValue() != null
                    )
                    .findAny()
                    .orElse(null);
            if(oldClient == null || oldMethod == null) {
                clients.put(conn, null);
                ipMappings.remove(ip);
                methods.remove(ip);
                System.out.println("Kein Team/Methode zuweisbar");
                sendActiveScreen(conn);
            } else {
                Team t = oldClient.getValue();
                boolean conflictingClient = oldMethod.getValue() == WebMethod.TEAM && clients
                        .entrySet()
                        .stream()
                        .filter(entry -> entry.getKey() != null && entry.getValue() != null && entry.getValue().equals(t) && entry.getKey().isOpen())
                        .findAny().isPresent();
                if(conflictingClient) {
                    methods.remove(ip);
                    ipMappings.remove(ip);
                    clients.put(conn, null);
                    sendActiveScreen(conn);
                } else {
                    clients.put(conn, t);
                    conn.send("m('Du wurdest automatisch dem Team \"" + t.getName() + "\" zugewiesen.');");
                    LogHelper.debug("Automatisch Team \"" + t.getName() + "\" zugewiesen");
                    sendActiveScreen(conn);
                    sendScore(conn);
                }
            }
        }
    }

    @Override
    public void onClose(final WebSocket conn, int code, String reason, boolean remote) {
        LogHelper.info(conn.getRemoteSocketAddress().getAddress().getHostAddress() + " hat die Verbindung verloren. Grund: " + code + " - " + reason);
        synchronized(clients) {
            clients.remove(conn);
        }
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        LogHelper.debug(conn.getRemoteSocketAddress().getAddress().getHostAddress() + " sendet: " + message);
        requestHandler.handle(conn, message);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        if(ex instanceof BindException) {
            new ExceptionDialog(ex, "Fehler beim Hochfahren des Servers. Versuchen Sie alle Instanzen des NaWiLaunchers zu beenden oder den PC neuzustarten. Stellen Sie sicher, dass der NaWiLauncher nicht durch Ihre Firewall blockiert wird.");
            Main.getServer().stop();
        }
       LogHelper.error("Error at " + (conn != null && conn.getRemoteSocketAddress() != null ? conn.getRemoteSocketAddress().getAddress().getHostAddress() : "Unknown Client") + ": " + ex.getMessage() + " / Stacktrace:", ex);
    }
    
}
