package de.mafel.nawilauncher.net;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * @author Felix
 */
public class NetErrorFormatter extends Formatter {
    
    private SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");

    @Override
    public String format(LogRecord record) {
        StringWriter stackTraceWriter = new StringWriter();
        if(record.getThrown() != null) record.getThrown().printStackTrace(new PrintWriter(stackTraceWriter));
        return "<i>" + record.getLevel().getLocalizedName() + " " + calcDate(record.getMillis()) + " @ " + record.getThreadID() + "</i><br><b>" + record.getMessage().replace("\n", "<br>") + "</b>" + stackTraceWriter.toString().replace("\n", "<br>");
    }
    
    private String calcDate(long millisecs) {
        return format.format(new Date(millisecs));
    }

}
