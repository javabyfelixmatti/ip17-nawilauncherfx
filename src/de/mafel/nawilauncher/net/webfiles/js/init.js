var webSocket;
var webSocketOpened = false;

function c(id) {
    t(0,0);
    $(".opcon").hide();
    $("#index-banner").css("display", "");
    $(".opcon-" + id).fadeIn();
}

function m(msg) {
    Materialize.toast(msg, 4000);
}

function teamchoose(teams) {
    var $select = $("#team-choose-select");
    $select.children("option.opd").remove();
    teams.forEach(function(el) {
        $select.append('<option class="opd" value="' + el + '">' + el + '</option>');
    });
    $select.material_select();
    c("teamchoose");
}

function abcd(data) {
    $("#abcdq").html(data.q);
    $("[data-abcd]").each(function(i, el) {
        $(this).removeClass("answered").html("<p>" + data.a[i] + "</p>");
        if(data.d) $(this).addClass("disabled");
        else $(this).removeClass("disabled");
    });
    c("abcdq");
    setTimeout(heightfix, 100);
}

function heightfix() {
    //Height Fix
    $("[data-abcd]").each(function(i, el) {
        $(this).css("height", "auto");
    });
    var maxheight = 0;
    $("[data-abcd]").each(function(i, el) {
        var height = $(this).height();
        if(height > maxheight) maxheight = height;
    });
    $("[data-abcd]").each(function(i, el) {
        $(this).css("height", maxheight + "px");
    });
}

function txtq(data) {
    $("#txtq").html(data.q);
    $("#txta").val("");
    c("text");
}

function estq(data) {
    $("#estq").html(data.q);
    $("#estu").html("Antwort [" + data.u + "]:");
    c("est");
}

function seqq(data) {
    $("#seqq").html(data.q);
    var seqa = "";
    data.a.forEach(function(el, i) {
        seqa += '<li data-aid="' + i + '">' + el + '</li>';
    });
    $("#seqa").html(seqa);
    c("seq");
}
 
function sort() {
    $('#seqa').sortable({
      revert: true,
      opacity: 0.8
    });
    $("ul, li").disableSelection();
}

function mf(m) {
    webSocket.send(JSON.stringify({
        "r": "m",
        "m": m
    }));
}

function txtf() {
    webSocket.send(JSON.stringify({
        "r": "a",
        "text": $("#txta").val().replace(/(?:\r\n|\r|\n)/g, " ")
    }));
}

function seqf() {
    var answers = new Array();
    $("[data-aid]").each(function() {
        answers.push($(this).html());
    });
    webSocket.send(JSON.stringify({
        "r": "a",
        "seq": answers
    }));
}

function estf() {
    webSocket.send(JSON.stringify({
        "r": "a",
        "est": parseFloat($("#esta").val())
    }));
}

function t(val, max) {
    console.log(Math.round(10000 * val / max) / 100 + "%");
    $(".timer-val")
        .css({
            WebkitTransition : "none",
            MozTransition    : "none",
            MsTransition     : "none",
            OTransition      : "none",
            transition       : "none",
        })
        .css({width: Math.round(100 * val / max) + "%"});
    setTimeout(function() {
        $(".timer-val").css({
            WebkitTransition : "width " + val + "s linear",
            MozTransition    : "width " + val + "s linear",
            MsTransition     : "width " + val + "s linear",
            OTransition      : "width " + val + "s linear",
            transition       : "width " + val + "s linear",
        });
        $(".timer-val").css("width", "0%");
    }, 50);
}

function tn(name, r, g, b) {
    $("#team-name").val(name);
    document.getElementById("team-color").jscolor.fromRGB(r, g, b);
    $("#submit-team").html("Team ändern");
}

function s(score) {
    var cs = parseInt($(".score>h1").html());
    if(score > cs) {
        var id = setInterval(function() {
            cs++;
            if(cs > score) clearInterval(id);
            else $(".score>h1").html(cs);
        }, 1000 / (score - cs));
    }
    else if(score < cs) {
        var id = setInterval(function() {
            cs--;
            if(cs < score) clearInterval(id);
            else $(".score>h1").html(cs);
        }, 1000 / (cs - score));
    }
}

(function($){
  $(function(){
      
    $(".opcon").hide();
      
    webSocket = new WebSocket("ws://" + document.location.hostname + ":1079");
      
    sort();

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
      
    webSocket.onopen = function(e) {
        console.log("WebSocket geöffnet!");
        webSocketOpened = true;
    }
    webSocket.onmessage = function(e) {
        console.log(e.data);
        $("#script-div").html("<script>" + e.data + "</script>");
    }
    webSocket.onclose = function(e) {
        console.log("WebSocket geschlossen!");
        alert("Verbindung verloren!");
        $(".opcon").hide();
        webSocket = new WebSocket("ws://" + document.location.hostname + ":1079");
        webSocketOpened = false;
    }
    
    $("#submit-team").click(function(e) {
        webSocket.send(JSON.stringify({
            "r": "tc",
            "tn": $("#team-name").val(),
            "tc": "#" + $("#team-color").html()
        }));
		$("#submit-team").html("Team ändern");
    });
    
    $("#submit-teamchoose").click(function(e) {
        webSocket.send(JSON.stringify({
            "r": "th",
            "tn": $("#team-choose-select").val()
        }));
    });
      
    $("a[data-abcd]").click(function(e) {
        if($(this).hasClass("disabled")) return;
        $("a[data-abcd]").removeClass("answered");
        $(this).addClass("answered");
        webSocket.send(JSON.stringify({
            "r": "a",
            "abcd": $(this).attr("data-abcd")
        }));
    });
      
    $(window).resize(function(e) {
        heightfix();
    });
      
    //Prevent line breaks
    $("#idContentEditable").keypress(function(e){ return e.which != 13; });
      
  })
})(jQuery);