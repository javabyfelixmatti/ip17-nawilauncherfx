package de.mafel.nawilauncher.net;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.scenes.components.ProgressDialog;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.util.Optional;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author matti
 */
public class Updater {
    
    final String UPDATE_URL = "http://src.felixkleinsteuber.de/NaWiLauncher/";
    
    public static final boolean DISABLE_UPDATER = false;
    
    
    public Updater() {
        if (!DISABLE_UPDATER) {
            try {
                try {
                    Files.delete(new File("updater.jar").toPath());
                } catch (IOException ex){}
                URL url = new URL(UPDATE_URL + "version.txt");
                BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                String newestVersion = reader.readLine();
                LogHelper.info("Aktuelle Version: " + Main.getBuildNumber());
                LogHelper.info("Neue Version: " + newestVersion);
                
                if (Long.parseLong(newestVersion) > Main.getBuildNumber()) {
                    Optional<ButtonType> result = null;
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Bestätigung");
                    alert.setHeaderText("Es ist eine neue Version verfügbar.");
                    alert.setContentText("Es ist eine neue Version verfügbar. Build " + newestVersion + " (Sie besitzen Build " + Main.getBuildNumber() + ") Möchten Sie sie herunterladen? ");
                    result = alert.showAndWait();
                    if (result.get() == ButtonType.OK){
                        ProgressDialog pd = new ProgressDialog("Nach Updates suchen...");
                        Task<Void> task = new Task() {
                            @Override
                            protected Object call() throws Exception {
                                updateProgress(1, 3);
                                //pd.setTitle("Download...");
                                URL url = new URL(UPDATE_URL + newestVersion + ".jar");
                                ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                                File updateFile = new File("update.jar");
                                FileOutputStream fos = new FileOutputStream(updateFile);
                                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                                updateProgress(2, 3);
                                url = new URL(UPDATE_URL + "updater.jar");
                                rbc = Channels.newChannel(url.openStream());
                                File updaterFile = new File("updater.jar");
                                fos = new FileOutputStream(updaterFile);
                                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                                updateProgress(3, 3);
                                File mainFile = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
                                String execution = "java -jar " + updaterFile.getAbsolutePath().replace(" ", "%20") + " " + mainFile.getAbsolutePath().replace(" ", "%20") + " " + updateFile.getAbsolutePath().replace(" ", "%20");
                                System.out.println(execution);
                                Runtime.getRuntime().exec(execution);
                                System.exit(0);
                                return null;
                            }
                        };
                        pd.activate(task);
                        Thread thrd = new Thread(task);
                        thrd.start();
                    }
                }
            } catch (MalformedURLException ex) {
                ExceptionDialog ed = new ExceptionDialog(ex, "Kein gültige URL.");
            } catch (IOException ex) {
                System.out.println("Konnte nicht auf Updates prüfen");
            } catch (NumberFormatException ex) {
                System.out.println("Konnte nicht auf Updates prüfen");
            }
        }
    }
}
