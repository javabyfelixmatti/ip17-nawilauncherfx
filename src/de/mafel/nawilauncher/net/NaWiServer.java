package de.mafel.nawilauncher.net;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import de.mafel.nawilauncher.LogHelper;
import static de.mafel.nawilauncher.LogHelper.error;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;

/**
 *
 * @author MATFRI1208
 */
public class NaWiServer implements HttpHandler {

    private HttpServer httpServer;
    private WebSocketServer webSocketServer;
    private NetErrorHandler handler;
    private final ReadOnlyBooleanWrapper state = new ReadOnlyBooleanWrapper(false);
    private int httpPort;

    public NaWiServer() {
        httpServer = null;
        webSocketServer = null;
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop)); //Nur zur Sicherheit; Server wird in der Main#stop()-Methode heruntergefahren
    }

    public boolean getState() {
        return state.get();
    }

    public ReadOnlyBooleanProperty stateProperty() {
        return ReadOnlyBooleanProperty.readOnlyBooleanProperty(state);
    }

    public WebSocketServer getWebSocketServer() {
        return webSocketServer;
    }

    public void start() throws IOException, IllegalStateException {
        if (getState() || httpServer != null) {
            throw new IllegalStateException("Server already running");
        }
        //Besten Port für HTTP Kommunikation auswählen
        httpPort = 80;
        try {
            httpServer = HttpServer.create(new InetSocketAddress(httpPort), 0);
        } catch(IOException ex) {
            httpPort = 1080;
            for(;httpPort < 1091; httpPort++) {
                try {
                    httpServer = HttpServer.create(new InetSocketAddress(httpPort), 0);
                } catch(IOException ex2) {
                    continue;
                }
                break;
            }
        }
        httpServer.createContext("/", this);
        httpServer.start();
        //WebSocketServer
        webSocketServer = new WebSocketServer();
        webSocketServer.start();
        //Logging WebSocketServer
        try {
            handler = new NetErrorHandler();
            NetErrorFormatter formatter = new NetErrorFormatter();
            handler.setEncoding("UTF-8");
            handler.setFormatter(formatter);
            handler.setLevel(Level.INFO);
            LogHelper.getLogger().addHandler(handler);
        } catch(UnsupportedEncodingException | SecurityException | UnknownHostException ex) {
            error(ex);
        }
        state.setValue(true);
        LogHelper.info("NaWiServer gestartet auf " + getIP());
    }

    public final void stop() {
        if (!getState()) {
            return;
        }
        LogHelper.info("Beende Server...");
        httpServer.stop(0);
        httpServer = null;
        try {
            webSocketServer.stop(1);
        } catch (InterruptedException ex) {
            LogHelper.error(ex);
        }
        webSocketServer = null;
        try {
            handler.shutdown();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(NaWiServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        handler = null;
        state.setValue(false);
    }

    @Override
    public void handle(HttpExchange he) throws IOException {
        try {
            String url = he.getRequestURI().toASCIIString();
            LogHelper.info("HTTP Anfrage auf " + url);
            switch(url) {
                case "/": url = "/index.html"; break;
                case "/console/": case "/console": url = "/console.html"; break;
            }
            InputStream is = getClass().getResourceAsStream("webfiles" + url.replace("..", "_"));
            if (is != null) {
                try {
                    String response = "";
                    int len;
                    byte[] buffer = new byte[64];
                    while ((len = is.read(buffer)) != - 1) {
                        response += new String(buffer, 0, len);
                    }
                    is.close();
                    String[] splitFileName = url.split("\\.");
                    sendStream(he, response, getMimeType(splitFileName[splitFileName.length - 1]), 200);
                } catch (Exception ex) {
                    String errorStr = "500 - Internal Server Error<br>" + ex.getMessage();
                    for (StackTraceElement el : ex.getStackTrace()) {
                        errorStr += "<br>" + el.toString();
                    }
                    sendError(he, errorStr, 500);
                }
            } else {
                sendError(he, "404 - Not Found", 404);
            }
        } catch (NullPointerException ex) {
            sendError(he, "404 - Not Found", 404);
            LogHelper.error(ex);
        }
    }

    public void sendError(HttpExchange he, String error, int httpCode) {
        try {
            he.getResponseHeaders().add("Content-type", "text/html");
            String response = "<b>Error: </b>" + error;
            he.sendResponseHeaders(httpCode, response.length());
            OutputStream os = he.getResponseBody();
            byte[] responseBytes = response.getBytes();
            for (int i = 0; i < responseBytes.length; i += 64) {
                int left = responseBytes.length - i;
                os.write(response.getBytes(), i, left > 64 ? 64 : left);
            }
            os.close();
        } catch (IOException ex) {
            LogHelper.error(ex);
        }
    }

    public void sendStream(HttpExchange he, String response, String mimeType, int status) {
        try {
            he.getResponseHeaders().add("Content-type", mimeType);
            he.sendResponseHeaders(status, 0);
            try (BufferedOutputStream out = new BufferedOutputStream(he.getResponseBody())) {
                try (ByteArrayInputStream bis = new ByteArrayInputStream(response.getBytes())) {
                    byte[] buffer = new byte[64];
                    int count;
                    while ((count = bis.read(buffer)) != -1) {
                        out.write(buffer, 0, count);
                    }
                }
            }
        } catch (IOException ex) {
            LogHelper.error(ex);
        }
    }

    private String getMimeType(String fileEnding) {
        switch (fileEnding.toLowerCase()) {
            case "css":
                return "text/css";
            case "html":
            case "htm":
            case "php":
                return "text/html";
            default:
                return "text/html";
        }
    }

    public String getIP() {
        if (!getState()) {
            return "Nicht gestartet";
        }
        try {
            return getLocalIP().getHostAddress() + (httpPort == 80 ? "" : ":" + httpPort);
        } catch (UnknownHostException | SocketException ex) {
            LogHelper.error(ex);
            return "Nicht verbunden";
        }
    }
    
    public void forceCollectAllAnswers(Runnable finished) {
        if(webSocketServer != null) webSocketServer.forceCollectAllAnswers(finished);
        else finished.run();
    }

    /**
     * Workaround zum Herausfinden der präferierten IP-Adresse.
     * 
     * <code>InetAddress.getLocalHost</code> gibt standardmäßig die IP-Adresse des ersten Netzwerkadapters zurück --> u.a. Probleme mit installierter Virtual Machine
     */
    private static InetAddress getLocalIP() throws UnknownHostException, SocketException {
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            return socket.getLocalAddress();
        }
    }

}
