package de.mafel.nawilauncher.net.plickers;

import de.mafel.nawilauncher.LogHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersPollResult {

    private JSONObject result;

    public PlickersPollResult(JSONObject result) {
        this.result = result;
    }
    
    public PlickersPollResult(String result) {
        this.result = new JSONObject(result);
        System.out.println(this.result.toString(2));
    }
    
    public JSONObject getResult() {
        return result;
    }
    
    /**
     * Überprüft, ob die Frage kompatibel mit einer NaWiLauncher-ABCD-Frage ist.
     * 
     * Dies ist wahr, wenn es vier Antwortmöglichkeiten gibt, von denen genau eine die richtige ist.
     *
     * @return true wenn kompatibel
     */
    public boolean isCompatible() {
        try {
            JSONArray choices = result.getJSONObject("snapshot").getJSONArray("choices");
            if(choices.length() != 4) return false;
            boolean correct = false;
            for(Object obj : choices) {
                if(!(obj instanceof JSONObject)) return false;
                if(((JSONObject) obj).getBoolean("correct")) {
                    if(!correct) correct = true;
                    else return false;
                }
            }
            return correct;
        } catch(JSONException ex) {
            return false;
        }
    }
    
    /**
     * Gibt einen Array der Länge 4 zurück, welcher die Anzahl der gegebenen
     * Antworten für die Antwortmöglichkeiten A,B,C,D in dieser Reihenfolge
     * enthält.
     *
     * @return Antworten
     */
    public int[] getAnswerCounts() {
        int[] answers = new int[4];
        if(!result.has("responsesByCard")) return answers;
        JSONObject responsesByCard = result.getJSONObject("responsesByCard");
        for(String key : responsesByCard.keySet()) {
            JSONObject student = responsesByCard.getJSONObject(key);
            if(student.get("student") == JSONObject.NULL) continue;
            switch(student.getString("answer")) {
                case "A": answers[0]++; break;
                case "B": answers[1]++; break;
                case "C": answers[2]++; break;
                case "D": answers[3]++; break;
                default: LogHelper.warning("Warnung: Ungültige Antwort '" + responsesByCard.getJSONObject(key).getString("answer") + "' für Umfrage von Plickers erhalten");
            }
        }
        return answers;
    }
    
    public int getAnswerCount() {
        return result.getInt("assignedResponseCount");
    }

    @Override
    public String toString() {
        return "PlickersPollResult[" + result.toString(2) + "]";
    }
    
}
