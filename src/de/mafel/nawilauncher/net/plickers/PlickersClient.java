package de.mafel.nawilauncher.net.plickers;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.data.Settings;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author Felix
 */
public class PlickersClient {
    
    public static final String PLICKERS_URL_CONTROL = "https://api.plickers.com/control",
            PLICKERS_URL_POLL = "https://api.plickers.com/polls/",
            PLICKERS_URL_POLLS = "https://api.plickers.com/polls",
            PLICKERS_URL_SECTIONS = "https://api.plickers.com/sections",
            PLICKERS_URL_SESSIONS = "https://api.plickers.com/sessions",
            PLICKERS_URL_SESSION = "https://api.plickers.com/sessions/",
            PLICKERS_URL_META = "https://api.plickers.com/meta",
            PLICKERS_URL_USER = "https://api.plickers.com/users/",
            PLICKERS_URL_QUESTIONS = "https://api.plickers.com/questions";
    
    public static String getDate() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(Calendar.getInstance().getTime());
    }
    
    public static void disableSSLCertCheck() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
            @Override
            public X509Certificate[] getAcceptedIssuers(){return null;}
            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType){}
            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType){}
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    
    private String authToken, userId, sessionId;
    private boolean inactive = false;

    public PlickersClient() throws Exception {
        this.authToken = Settings.getSettings().get("plickers.authcode", null);
        this.sessionId = Settings.getSettings().get("plickers.sessionid", null);
        if(authToken == null || sessionId == null) throw new NullPointerException("Keine gespeichertene Login-Daten vorhanden.");
        this.userId = queryMeta().getUserId(); //Überprüft AuthCode, wirft Fehler wenn dieser ungültig ist --> erneute Anmeldung nötig
        disableSSLCertCheck();
    }
    
    public PlickersClient(String email, String password) throws PlickersCommunicationException {
        disableSSLCertCheck();
        //Login & fetch authcode
        HttpsURLConnection con = null;
        try {
            //HTTPRequest konstruieren
            URL urlurl = new URL(PLICKERS_URL_SESSIONS);
            String encodedData = "email=" + URLEncoder.encode(email, "UTF-8") + "&password=" + URLEncoder.encode(password, "UTF-8");
            con = (HttpsURLConnection) urlurl.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json, text/plain, */*");
            con.setRequestProperty("Cache-Control", "no-cache");
            con.setRequestProperty("x-api-version", "1.4.0");
            con.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
            con.setUseCaches(false);
            con.setDoOutput(true);

            //Request senden
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(encodedData);
            wr.close();
            
            if(con.getResponseCode() == 401) throw new PlickersCommunicationException("Falsches Passwort.");
            if(con.getResponseCode() == 422) throw new PlickersCommunicationException("Ungültige oder unbekannte E-Mail-Adresse");
            
            //Antwort lesen
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
              response.append(line);
              response.append('\n');
            }
            rd.close();
            JSONObject obj = new JSONObject(response.toString());
            authToken = obj.getString("token");
            userId = obj.getString("user");
            sessionId = obj.getString("id");
            Settings.getSettings().put("plickers.authcode", authToken);
            Settings.getSettings().put("plickers.sessionid", sessionId);
            LogHelper.info("[PlickersClient] Erfolgreich eingeloggt.");
        } catch (MalformedURLException ex) {
            throw new PlickersCommunicationException("Ungültige Control-URL", ex);
        } catch (IOException ex) {
            throw new PlickersCommunicationException(ex);
        } catch(JSONException ex) {
            throw new PlickersCommunicationException("Ungültige Serverantwort", ex);
        } finally {
            if(con != null) con.disconnect();
        }
    }
    
    public void logout() throws PlickersCommunicationException, IllegalStateException {
        if(inactive || sessionId == null || sessionId.isEmpty()) throw new IllegalStateException("Der Client ist nicht aktiv bzw. besitzt keine aktive Session");
        //Login & fetch authcode
        HttpsURLConnection con = null;
        try {
            //HTTPRequest konstruieren
            URL urlurl = new URL(PLICKERS_URL_SESSION + sessionId);
            con = (HttpsURLConnection) urlurl.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json, text/plain, */*");
            con.setRequestProperty("Cache-Control", "no-cache");
            con.setRequestProperty("x-api-version", "1.4.0");
            con.setRequestProperty("x-auth-token", this.authToken);
            con.setUseCaches(false);
            
            if(con.getResponseCode() >= 300) throw new PlickersCommunicationException("Logout fehlgeschlagen.");
            Settings.getSettings().remove("plickers.authcode");
            Settings.getSettings().remove("plickers.sessionid");
            LogHelper.info("[PlickersClient] Erfolgreich ausgeloggt.");
            inactive = true;
        } catch (MalformedURLException ex) {
            throw new PlickersCommunicationException("Ungültige Session-URL", ex);
        } catch (IOException ex) {
            throw new PlickersCommunicationException(ex);
        } catch(JSONException ex) {
            throw new PlickersCommunicationException("Ungültige Serverantwort", ex);
        } finally {
            if(con != null) con.disconnect();
        }
    }
    
    public PlickersUserResult queryUser() throws Exception {
        return queryUser(userId);
    }
    
    public PlickersUserResult queryUser(String userId) throws Exception {
        return new PlickersUserResult(queryPlickers(PLICKERS_URL_USER + userId));
    }
    
    public final PlickersMetaResult queryMeta() throws Exception {
        return new PlickersMetaResult(queryPlickers(PLICKERS_URL_META));
    }
    
    public PlickersControlResult queryControl() throws Exception {
        return new PlickersControlResult(queryPlickers(PLICKERS_URL_CONTROL));
    }
    
    public PlickersPollResult queryPoll(String pollId) throws Exception {
        return new PlickersPollResult(queryPlickers(PLICKERS_URL_POLL + pollId));
    }
    
    public PlickersSectionsResult querySections() throws Exception {
        return querySections(10, true);
    }
    
    public PlickersSectionsResult querySections(int limit, boolean populated) throws Exception {
        return new PlickersSectionsResult(queryPlickers(PLICKERS_URL_SECTIONS + "?limit=" + limit + "&populated=" + (populated ? "true" : "false")));
    }
    
    public PlickersPollsResult queryPolls() throws Exception {
        return queryPolls(10);
    }
    
    public PlickersPollsResult queryPolls(int limit) throws Exception {
        return new PlickersPollsResult(queryPlickers(PLICKERS_URL_POLLS + "?limit=" + limit));
    }
    
    public PlickersPollsResult queryPolls(int limit, String section) throws Exception {
        return new PlickersPollsResult(queryPlickers(PLICKERS_URL_POLLS + "?limit=" + limit + "&section=" + section));
    }
    
    public PlickersNewQuestionResult queryNewQuestion(PlickersNewQuestionRequest request) throws Exception {
        return new PlickersNewQuestionResult(queryPlickers(PLICKERS_URL_QUESTIONS, request.get()));
    }
    
    public PlickersAddToQueueResult queryAddToQueue(PlickersAddToQueueRequest request) throws Exception {
        return new PlickersAddToQueueResult(queryPlickers(PLICKERS_URL_POLLS, request.get()));
    }
    
    public final String queryPlickers(String url) throws Exception {
        if(authToken == null) throw new NullPointerException("Der AuthToken ist null.");
        if(inactive) throw new IllegalStateException("Der Client ist inaktiv.");
        LogHelper.debug("[PlickersClient] Anfrage an " + url);
        HttpsURLConnection con = null;
        try {
            //HTTPRequest konstruieren
            URL urlurl = new URL(url);
            con = (HttpsURLConnection) urlurl.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json, text/plain, */*");
            con.setRequestProperty("Cache-Control", "no-cache");
            con.setRequestProperty("x-api-version", "1.4.0");
            con.setRequestProperty("x-auth-token", this.authToken);
            con.setUseCaches(false);
            
            if(con.getResponseCode() >= 300) throw new PlickersCommunicationException("Ungültiger Response Code: " + con.getResponseCode() + " " + con.getResponseMessage() + " Anfrage: "+ url);
            
            //Antwort lesen
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
              response.append(line);
              response.append('\n');
            }
            rd.close();
            
            String msg = response.toString();
            
            LogHelper.debug("[PlickersClient] Antwort: " + msg);
            
            return msg;
        } catch (MalformedURLException ex) {
            throw new Exception("Ungültige Control-URL", ex);
        } finally {
            if(con != null) con.disconnect();
        }
    }
    
    public final String queryPlickers(String url, JSONObject payload) throws Exception {
        if(authToken == null) throw new NullPointerException("Der AuthToken ist null.");
        if(inactive) throw new IllegalStateException("Der Client ist inaktiv.");
        LogHelper.debug("[PlickersClient] POST Anfrage an " + url + " Payload: " + payload.toString());
        HttpsURLConnection con = null;
        try {
            //HTTPRequest konstruieren
            URL urlurl = new URL(url);
            String data = payload.toString();
            con = (HttpsURLConnection) urlurl.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json, text/plain, */*");
            con.setRequestProperty("Cache-Control", "no-cache");
            con.setRequestProperty("x-api-version", "1.4.0");
            con.setRequestProperty("x-auth-token", this.authToken);
            con.setRequestProperty("Content-Length", String.valueOf(data.length()));
            con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            con.setUseCaches(false);
            con.setDoOutput(true);

            //Request senden
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.close();
            
            if(con.getResponseCode() >= 300) throw new PlickersCommunicationException("Ungültiger Response Code: " + con.getResponseCode() + " " + con.getResponseMessage() + " Anfrage: " + url + " Payload: " + data);
            
            //Antwort lesen
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
              response.append(line);
              response.append('\n');
            }
            rd.close();
            
            String msg = response.toString();
            
            LogHelper.debug("[PlickersClient] Antwort: " + msg);
            
            return msg;
        } catch (MalformedURLException ex) {
            throw new Exception("Ungültige Control-URL", ex);
        } finally {
            if(con != null) con.disconnect();
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public boolean isInactive() {
        return inactive;
    }

    public String getAuthToken() {
        return authToken;
    }
    
    public class PlickersCommunicationException extends Exception {

        public PlickersCommunicationException() {
            super();
        }
        
        public PlickersCommunicationException(String message) {
            super(message);
        }
        
        public PlickersCommunicationException(Throwable cause) {
            super(cause);
        }
        
        public PlickersCommunicationException(String message, Throwable cause) {
            super(message, cause);
        }
        
    }
    
}
