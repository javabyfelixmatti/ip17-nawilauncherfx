package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersUserResult {

    private JSONObject result;

    public PlickersUserResult(JSONObject result) {
        this.result = result;
    }
    
    public PlickersUserResult(String result) {
        this.result = new JSONObject(result);
    }
    
    public JSONObject getResult() {
        return result;
    }
    
    public String getFirstName() {
        return result.getString("firstName");
    }
    
    public String getLastName() {
        return result.getString("lastName");
    }
    
    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }
    
    public String getEmail() {
        return result.getString("email");
    }
    
    public String getId() {
        return result.getString("id");
    }
    
    @Override
    public String toString() {
        return "PlickersUserResult[" + result.toString(2) + "]";
    }
    
}
