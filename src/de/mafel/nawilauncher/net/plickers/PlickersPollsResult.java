package de.mafel.nawilauncher.net.plickers;

import org.json.JSONArray;

/**
 *
 * @author Felix
 */
public class PlickersPollsResult {

    private JSONArray result;

    public PlickersPollsResult(JSONArray result) {
        this.result = result;
    }
    
    public PlickersPollsResult(String result) {
        this.result = new JSONArray(result);
    }
    
    public JSONArray getResult() {
        return result;
    }
    
    @Override
    public String toString() {
        return "PlickersPollsResult[" + result.toString(2) + "]";
    }
    
}
