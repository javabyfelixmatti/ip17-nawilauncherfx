package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersNewQuestionResult {

    private JSONObject result;

    public PlickersNewQuestionResult(JSONObject result) {
        this.result = result;
    }
    
    public PlickersNewQuestionResult(String result) {
        this.result = new JSONObject(result);
    }
    
    public JSONObject getResult() {
        return result;
    }
    
    public String getId() {
        return result.getString("id");
    }
    
    public String getUserId() {
        return result.getString("user");
    }
    
    @Override
    public String toString() {
        return "PlickersNewQuestionResult[" + result.toString(2) + "]";
    }
    
}
