package de.mafel.nawilauncher.net.plickers;

import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersNewQuestionRequest {
    
    private JSONObject data;

    public PlickersNewQuestionRequest(String body, String[] choices) {
        if(choices.length < 1 || choices.length > 4) throw new IllegalArgumentException("Es kann nur 1-4 Antwortmöglichkeiten geben.");
        data = new JSONObject();
        data.put("body", body);
        data.put("version", 0);
        data.put("folder", JSONObject.NULL);
        ArrayList<JSONObject> choicesJson = new ArrayList<>();
        for(String choice : choices) {
            JSONObject choiceJson = new JSONObject();
            choiceJson.put("body", choice);
            choicesJson.add(choiceJson);
        }
        data.put("choices", choicesJson);
    }
    
    public JSONObject get() {
        String date = PlickersClient.getDate();
        data.put("clientModified", date);
        data.put("userCreated", date);
        return data;
    }
    
}
