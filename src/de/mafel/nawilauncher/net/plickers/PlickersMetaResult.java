package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersMetaResult {

    private JSONObject result;

    public PlickersMetaResult(JSONObject result) {
        this.result = result;
    }
    
    public PlickersMetaResult(String result) {
        this.result = new JSONObject(result);
    }
    
    public JSONObject getResult() {
        return result;
    }
    
    public String getUserId() {
        return result.getString("intercomId");
    }
    
    @Override
    public String toString() {
        return "PlickersMetaResult[" + result.toString(2) + "]";
    }
    
}
