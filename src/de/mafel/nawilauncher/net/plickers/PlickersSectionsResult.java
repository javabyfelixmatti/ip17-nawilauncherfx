package de.mafel.nawilauncher.net.plickers;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersSectionsResult {

    private JSONArray result;

    public PlickersSectionsResult(JSONArray result) {
        this.result = result;
    }
    
    public PlickersSectionsResult(String result) {
        this.result = new JSONArray(result);
    }
    
    public JSONArray getResult() {
        return result;
    }
    
    public PlickersClass[] getClasses() {
        System.out.println(toString());
        return result.toList().stream().map(obj -> new PlickersClass(new JSONObject((HashMap<String, Object>) obj))).toArray(PlickersClass[]::new);
    }
    
    @Override
    public String toString() {
        return "PlickersSectionsResult[" + result.toString(2) + "]";
    }
    
}
