package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersStudent {

    private String name, id;
    
    public PlickersStudent(JSONObject data) {
        this.name = data.getString("enteredName");
        this.id = data.getString("id");
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
    
}
