package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersClass {

    private JSONObject data;

    public PlickersClass(JSONObject data) {
        this.data = data;
    }
    
    public JSONObject getData() {
        return data;
    }
    
    public String getId() {
        return data.getString("id");
    }
    
    public String getName() {
        return data.getString("name");
    }
    
    public String getYear() {
        return data.getString("year");
    }
    
    public String getSubject() {
        return data.getString("subject");
    }
    
    public int getStudentCount() {
        return data.getJSONArray("students").length();
    }
    
    public PlickersStudent[] getStudents() {
        return data.getJSONArray("students").toList().stream().map(obj -> new PlickersStudent((JSONObject) obj)).toArray(PlickersStudent[]::new);
    }
    
    @Override
    public String toString() {
        return "PlickersClass[" + data.toString(2) + "]";
    }
    
}
