package de.mafel.nawilauncher.net.plickers;

import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import de.mafel.nawilauncher.LogHelper;
import java.util.ArrayList;
import java.util.function.Consumer;
import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersPusher extends Pusher {
    
    private Channel channel;
    
    private ArrayList<Consumer<PlickersLiveEvent>> eventListeners = new ArrayList<>();

    public PlickersPusher(String userId) {
        super("18bdf306eb6503760ad4");
        super.connect(new ConnectionEventListener() {
            
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
                LogHelper.error("[Pusher] State changed to " + change.getCurrentState() +
                                   " from " + change.getPreviousState());
            }

            @Override
            public void onError(String message, String code, Exception e) {
                LogHelper.error("[Pusher] There was a problem connecting!");
            }
            
        }, ConnectionState.ALL);

        // Subscribe to a channel
        channel = super.subscribe(userId);
        
        // Bind to listen for events called "my-event" sent to "my-channel"
        channel.bind("poll", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channel, String event, String data) {
                System.out.println("Received event with data: " + data);
            }
        });
    }
    
    public void addLiveEventListener(Consumer<PlickersLiveEvent> listener) {
        eventListeners.add(listener);
    }
    
    public void removeLiveEventListener(Consumer<PlickersLiveEvent> listener) {
        eventListeners.remove(listener);
    }
    
    public class PlickersLiveEvent {
        
        private JSONObject data;
        
        public PlickersLiveEvent(JSONObject data) {
            this.data = data;
        }
        
    }

}
