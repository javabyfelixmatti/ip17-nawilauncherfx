package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersControlResult {

    private JSONObject result;

    public PlickersControlResult(JSONObject result) {
        this.result = result;
    }
    
    public PlickersControlResult(String result) {
        this.result = new JSONObject(result);
    }
    
    public JSONObject getResult() {
        return result;
    }
    
    public String getCurrentPoll() {
        return result.getString("currentPoll");
    }
    
    @Override
    public String toString() {
        return "PlickersControlResult[" + result.toString(2) + "]";
    }
    
}
