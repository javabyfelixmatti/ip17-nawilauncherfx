package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersAddToQueueResult {

    private JSONObject result;

    public PlickersAddToQueueResult(JSONObject result) {
        this.result = result;
    }
    
    public PlickersAddToQueueResult(String result) {
        this.result = new JSONObject(result);
    }
    
    public JSONObject getResult() {
        return result;
    }
    
    public String getId() {
        return result.getString("id");
    }
    
    public String getUserId() {
        return result.getString("user");
    }
    
    public String getSectionId() {
        return result.getString("section");
    }
    
    public String getQuestionId() {
        return result.getString("question");
    }
    
    @Override
    public String toString() {
        return "PlickersNewQuestionResult[" + result.toString(2) + "]";
    }
    
}
