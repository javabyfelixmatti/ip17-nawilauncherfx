package de.mafel.nawilauncher.net.plickers;

import org.json.JSONObject;

/**
 *
 * @author Felix
 */
public class PlickersAddToQueueRequest {

    private JSONObject data;

    public PlickersAddToQueueRequest(String questionId, String classId) {
        data = new JSONObject();
        data.put("question", questionId);
        data.put("section", classId);
    }
    
    public JSONObject get() {
        String date = PlickersClient.getDate();
        data.put("clientModified", date);
        data.put("planned", date);
        return data;
    }
    
}
