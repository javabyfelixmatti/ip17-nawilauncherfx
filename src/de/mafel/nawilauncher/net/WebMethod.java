package de.mafel.nawilauncher.net;

/**
 *
 * @author Felix
 */
public enum WebMethod {
    TEAM, INDIVIDUAL;
}
