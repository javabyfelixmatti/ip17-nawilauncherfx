package de.mafel.nawilauncher.net;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.scenes.Scenes;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import javafx.application.Platform;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.json.JSONArray;

/**
 *
 * @author Felix
 */
public class NetErrorWebsocketServer extends org.java_websocket.server.WebSocketServer {

    private final ArrayList<WebSocket> clients = new ArrayList<>();
    
    NetErrorWebsocketServer() throws UnknownHostException {
        super(new InetSocketAddress(1078));
    }
    
    public void sendMessage(JSONArray arr) {
        synchronized(clients) {
            clients.removeIf(c -> !c.isOpen());
            clients.forEach(c -> c.send(arr.toString()));
        }
    }
    
    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        LogHelper.info("NetErrorWebsocket verbunden mit " + conn.getRemoteSocketAddress().getAddress().getHostAddress());
        synchronized(clients) {
            clients.add(conn);
        }
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        LogHelper.info("NetError - " + conn.getRemoteSocketAddress().getAddress().getHostAddress() + " hat die Verbindung verloren. Grund: " + code + " - " + reason);
        synchronized(clients) {
            clients.remove(conn);
        }
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        String[] args = message.split(" ");
        switch(args[0].toLowerCase()) {
            case "score":
                if(args.length < 2) {
                    LogHelper.error("Eingabe: " + message + "\nNetConsole/score - Zu wenig Argumente. Syntax: score <set|add> <string Teamname> <int Punktzahl>");
                    return;
                }
                switch(args[1].toLowerCase()) {
                    case "set":
                        try {
                            Team t = TeamManager.getTeam(args[2].replace("_", " ")).get();
                            int score = Integer.parseInt(args[3]);
                            t.setScore(score);
                            Platform.runLater(Scenes::reloadScene);//aktualisieren
                        } catch(NoSuchElementException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/score set - Team mit dem Namen '" + args[2] + "' nicht gefunden. (Beachte Groß- und Kleinschreibung! Verwende '_' anstatt ' '!)", ex);
                        } catch(NumberFormatException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/score set - Ungültige Punktzahl '" + args[3] + "' (Nur ganze Zahlen erlaubt!)", ex);
                        } catch(ArrayIndexOutOfBoundsException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/score set - Zu wenig Argumente. Syntax: score set <string Teamname> <int Punktzahl>", ex);
                        }
                        return;
                    case "add":
                        try {
                            Team t = TeamManager.getTeam(args[2].replace("_", " ")).get();
                            int score = Integer.parseInt(args[3]);
                            t.setScore(t.getScore() + score);
                            Platform.runLater(Scenes::reloadScene);//aktualisieren
                        } catch(NoSuchElementException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/score add - Team mit dem Namen '" + args[2] + "' nicht gefunden. (Beachte Groß- und Kleinschreibung! Verwende '_' anstatt ' '!)", ex);
                        } catch(NumberFormatException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/score add - Ungültige Punktzahl '" + args[3] + "' (Nur ganze Zahlen erlaubt!)", ex);
                        } catch(ArrayIndexOutOfBoundsException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/score add - Zu wenig Argumente. Syntax: score add <string Teamname> <int Punktzahl>", ex);
                        }
                        return;
                    default:
                        LogHelper.error("Eingabe: " + message + "\nNetConsole/score - Ungültiges Argument '" + args[1] + "'. Syntax: score <set|add> <string Teamname> <int Punktzahl>");
                        return;
                }
            case "exit":
                if(args.length == 1) {
                    LogHelper.warning("Eingabe: " + message + "\nNetConsole/exit - Sicher? Bestätigen Sie mit 'exit now'.");
                    return;
                } else if(args[1].equalsIgnoreCase("now")) {
                    System.exit(0);
                }
                return;
            case "question":
                if(args.length < 2) {
                    LogHelper.error("Eingabe: " + message + "\nNetConsole/question - Zu wenig Argumente. Syntax: question <unlock> <string Kategorie> <int Punktzahl>");
                    return;
                }
                switch(args[1].toLowerCase()) {
                    case "unlock":
                        try {
                            Category c = QuizData.getCategoryForName(args[2].replace("_", " "));
                            if(c == null) throw new NullPointerException();
                            int score = Integer.parseInt(args[3]);
                            Navigator.getQuestionsDone().remove(new QuestionIdentifier(c, score));
                            Platform.runLater(Scenes::reloadScene);//aktualisieren
                        } catch(NullPointerException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/question unlock - Kategorie mit dem Namen '" + args[2] + "' nicht gefunden. (Beachte Groß- und Kleinschreibung! Verwende '_' anstatt ' '!)", ex);
                        } catch(NumberFormatException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/question unlock - Ungültige Punktzahl '" + args[3] + "' (Nur ganze Zahlen erlaubt!)", ex);
                        } catch(ArrayIndexOutOfBoundsException ex) {
                            LogHelper.error("Eingabe: " + message + "\nNetConsole/question unlock - Zu wenig Argumente. Syntax: question unlock <string Kategorie> <int Punktzahl>", ex);
                        }
                        return;
                    default:
                        LogHelper.error("Eingabe: " + message + "\nNetConsole/question - Ungültiges Argument '" + args[1] + "'. Syntax: question <unlock> <string Kategorie> <int Punktzahl>");
                        return;
                }
            case "nextround":
                TeamManager.setNextActiveTeam();
                Platform.runLater(Scenes::reloadScene);//aktualisieren
                return;
            default:
                LogHelper.error("Eingabe: " + message + "\nNetConsole - Ungültiger Befehl '" + args[0] + "'. Folgende Befehle stehen zur Verfügung: score, question, nextround, exit");
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        LogHelper.error("NetError Fehler bei " + (conn != null && conn.getRemoteSocketAddress() != null ? conn.getRemoteSocketAddress().getAddress().getHostAddress() : "Unbekannter Client"), ex);
    }

}
