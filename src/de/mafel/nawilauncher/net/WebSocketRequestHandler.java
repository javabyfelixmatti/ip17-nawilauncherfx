package de.mafel.nawilauncher.net;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.data.questions.EstimateQuestion;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.questions.SequenceQuestion;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import java.net.InetAddress;
import java.util.List;
import javafx.application.Platform;
import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Verarbeitet eine Anfrage über einen WebSocket. Wird vom WebSocketServer
 * instanziiert.
 *
 * @author Felix
 * @see WebSocketServer
 */
public class WebSocketRequestHandler {
    
    private final WebSocketServer server;
    
    public WebSocketRequestHandler(WebSocketServer server) {
        this.server = server;
    }
    
    /**
     * Verarbeitet eine Anfrage über einen WebSocket. Für Protokollbeschreibung
     * siehe WEBSOCKETPROTOCOL.
     *
     * @param client Client, der Anfrage gestellt hat
     * @param request Gestellte Anfrage als String im JSON-Format
     */
    public void handle(WebSocket client, String request) {
        try {
            InetAddress ip = client.getRemoteSocketAddress().getAddress();
            JSONObject obj = new JSONObject(request);
            String r = obj.getString("r");
            Team t;
            switch(r) {
                case "tc": //Team Create
                    if(server.getMethodForIP(ip) == WebMethod.INDIVIDUAL) {
                        client.send("m('Nur die Teamleiter dürfen ein neues Team erstellen!');c('');");
                        return;
                    }
                    t = new Team(obj.getString("tn"), obj.getString("tc"));
                    if(server.getTeamForClient(client) != null) {
                        TeamManager.replaceTeamLater(server.getTeamForClient(client), t);
                        client.send("m('Euer Team wurde geändert');");
                    } else if(TeamManager.getTeams().stream().filter(te -> te.getName().equalsIgnoreCase(t.getName())).count() == 0) {
                        TeamManager.addTeamLater(t);
                        client.send("m('Willkommen im Spiel, " + t.getName() + "!');");
                    } else {
                        client.send("m('Ein Team mit diesem Namen existiert bereits!');");
                        break;
                    }
                    server.setClientTeam(client, t);
                    server.setIPTeam(ip, t);
                    LogHelper.info("Weise " + ip.getHostAddress() + " Team " + t.getName() + " zu");
                    break;
                case "th": //Team Choose
                    t = TeamManager.getTeam(obj.getString("tn")).orElse(null);
                    if(server.getMethodForIP(ip) == WebMethod.INDIVIDUAL) {
                        server.setClientTeam(client, t);
                        server.setIPTeam(ip, t);
                        server.sendActiveScreen(client);
                        server.sendScore(client);
                        client.send("m('Willkommen im Spiel, Publikumsspieler für " + t.getName() + "!');");
                    } else if(t != null && server.getTeamClientsForTeam(t).isEmpty()) {
                        server.setClientTeam(client, t);
                        server.setIPTeam(ip, t);
                        server.sendActiveScreen(client);
                        server.sendScore(client);
                        client.send("m('Willkommen im Spiel, " + t.getName() + "!');");
                    } else {
                        client.send("m('Das ausgewählte Team ist ungültig oder es ist bereits ein Client mit diesem Team assoziiert.');");
                        server.setIPMethod(ip, null);
                        server.sendActiveScreen(client);
                    }
                    break;
                case "m": //Method
                    int m = obj.getInt("m");
                    if(m >= 0 && m <= 1) {
                        server.setIPMethod(ip, WebMethod.values()[m]);
                        server.setClientTeam(client, null);
                        server.setIPTeam(ip, null);
                        server.sendActiveScreen(client);
                    } else client.send("m('Die ausgewählte Methode ist ungültig!');");
                    break;
                case "a":
                    if(server.getMethodForIP(ip) == null) {
                        client.send("m('Du kannst noch keine Antwort abgeben. (Nicht identifizierte Methode)');");
                        return;
                    }
                    final Team tf = server.getTeamForClient(client);
                    Question q = Navigator.getActiveQuestion();
                    Object answer;
                    try {
                        if(q == null) throw new IllegalStateException("Es ist gerade keine Frage aktiv.");
                        if(q instanceof ABCDQuestion) answer = obj.getInt("abcd");
                        else if(q instanceof TextQuestion) answer = obj.getString("text");
                        else if(q instanceof EstimateQuestion) answer = obj.getDouble("est");
                        else if(q instanceof SequenceQuestion) {
                            List<Object> list = obj.getJSONArray("seq").toList();
                            answer = list.parallelStream().map(Object::toString).toArray(String[]::new);
                        } else throw new IllegalStateException("Es ist gerade kein bekannter Fragetyp aktiv.");
                    } catch(JSONException ex) {
                        client.send("m('Du hast keine gültige Antwort abgegeben.');");
                        if(q instanceof ABCDQuestion) answer = -1;
                        else if(q instanceof TextQuestion) answer = "-";
                        else if(q instanceof EstimateQuestion) answer = Double.NaN;
                        else if(q instanceof SequenceQuestion) answer = new String[]{};
                        else throw new IllegalStateException("Es ist gerade kein bekannter Fragetyp aktiv.");
                    }
                    final Object fans = answer;
                    if(server.getMethodForIP(ip) == WebMethod.TEAM) Platform.runLater(() -> tf.setAnswer(fans));
                    else if(server.getMethodForIP(ip) == WebMethod.INDIVIDUAL) server.setIndividualAnswer(client, fans);
                    break;
                default:
                    throw new UnsupportedOperationException("Die Request-Methode [r] '" + r + "' wird nicht unterstützt.");
            }
        } catch(JSONException ex) {
            LogHelper.error("Konnte Anfrage von " + client.getRemoteSocketAddress().getAddress().getHostAddress() + "nicht verarbeiten [ungültiges JSON]: " + ex.getMessage(), ex);
        } catch (Exception ex) {
            LogHelper.error(ex);
        }
        
    }
    
}
