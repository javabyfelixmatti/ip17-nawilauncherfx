package de.mafel.nawilauncher;

import de.mafel.nawilauncher.data.GameBackup;
import de.mafel.nawilauncher.fileio.MediaStore;
import de.mafel.nawilauncher.input.GlobalKeyHandler;
import de.mafel.nawilauncher.net.NaWiServer;
import de.mafel.nawilauncher.net.Updater;
import de.mafel.nawilauncher.net.plickers.PlickersClient;
import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Hauptklasse, welche alle nötigen Instanzen erstellt und das Programm startet.
 * 
 * @see Main#getMainScene() Main.getMainScene()
 * @see Main#getStage() Main.getStage()
 * @see Main#getServer() Main.getServer()
 * @author Felix, Matti
 */
public class Main extends Application {
    
    private static Scene mainScene;
    private static Stage stage;
    private static NaWiServer server;
    private static GlobalKeyHandler keyHandler;
    private static PlickersClient plickersClient = null;
    
    private static final long BUILD = 156;
        
    @Override
    public void start(Stage stage) throws Exception {
        LogHelper.info("Starte Logging...");
        
        keyHandler = new GlobalKeyHandler();
        Parent root = FXMLLoader.load(getClass().getResource("scenes/MainScene.fxml"));
        Main.stage = stage;
        mainScene = new Scene(root);
        mainScene.addEventHandler(KeyEvent.KEY_RELEASED, keyHandler);
        
        stage.setScene(mainScene);
        stage.show();
        
        stage.setFullScreenExitHint("Drücken Sie erneut F11, um den Vollbildmodus zu verlassen");
        stage.setFullScreenExitKeyCombination(new KeyCodeCombination(KeyCode.F11));
        
        stage.setMinHeight(720);
        stage.setMinWidth(1280);
        stage.setTitle("NaWiLauncher");
        stage.getIcons().add(new Image(Main.class.getResourceAsStream("res/favicon.png")));
        LogHelper.info("MediaStore bei..." + MediaStore.DIR.toString());
        
        server = new NaWiServer();
        LogHelper.info("Software bei..." + new File("").getAbsolutePath());
        
        GameBackup.checkBackup();
        
        Updater updater = new Updater();
    }

    @Override
    public void stop() throws Exception {
        getServer().stop();
    }

    /**
     * Startet NaWiLauncherFX.
     * 
     * Folgende Startargumente sind dabei zulässig:
     * debug -> Aktiviert Debugausgaben
     * 
     * @param args command line arguments
     */
    public static void main(String[] args) {
        for(String arg : args) if(arg.equalsIgnoreCase("debug")) LogHelper.setDebugMode(true);
        launch(args);
    }

    /**
     * Gibt die MainScene, auf der alle Inhalte als Parents dargestellt werden, zurück.
     *
     * @return MainScene
     * @see Main#getStage() Main.getStage()
     */
    public static Scene getMainScene() {
        return mainScene;
    }

    /**
     * Gibt die Stage, die die MainScene darstellt, zurück.
     *
     * @return Main Stage
     * @see Main#getMainScene() Main.getMainScene()
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * Gibt die Hauptinstanz des NaWiServers zurück.
     *
     * @return NaWiServer
     * @see NaWiServer NaWiServer
     */
    public static NaWiServer getServer() {
        return server;
    }

    /**
     * Gibt die Hauptinstanz des KeyHandlers zurück.
     *
     * @return KeyHandler
     */
    public static GlobalKeyHandler getKeyHandler() {
        return keyHandler;
    }

    /**
     * Gibt die aktuelle Build-Nummer zurück.
     *
     * @return Build
     */
    public static Long getBuildNumber() {
        return BUILD;
    }

    /**
     * Setzt den verwendeten PlickersClient, der mit einem Plickers Account/Auth
     * Code verbunden ist. Darf null sein.
     *
     * @param plickersClient
     */
    public static void setPlickersClient(PlickersClient plickersClient) {
        Main.plickersClient = plickersClient;
    }

    /**
     * Gibt den verwendeten PlickersClient zurück oder null, wenn kein Plickers
     * Konto verbunden wurde.
     *
     * @return PlickersClient
     */
    public static PlickersClient getPlickersClient() {
        return plickersClient;
    }
    
}
