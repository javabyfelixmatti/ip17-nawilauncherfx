package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.data.questions.EstimateQuestion;
import de.mafel.nawilauncher.data.questions.SequenceQuestion;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import de.mafel.nawilauncher.fileio.MediaStore;
import de.mafel.nawilauncher.input.GlobalKeyHandler;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author matti, Felix
 */
public class EditQuizSceneController implements Initializable, DynamicController {
    
    FileChooser fileChooser = new FileChooser();
    File savefile = null, iconFile = null;

    HashMap<Category, ListView<Question>> accordionListViews = new HashMap<>();
    Question selectedQuestion = null;
    boolean tabLoaded = false;
    
    TextField tfQuestion, tfAnswerA, tfAnswerB, tfAnswerC, tfAnswerD;
    
    CheckBox cbA = new CheckBox(), cbB = new CheckBox(), cbC = new CheckBox(), cbD = new CheckBox();
    
    Label lblQuestionAtAnswerTab = new Label(), lblCorrectAnswer = new Label();
    
    Button btnAddMedia = new Button(), btnRemoveMedia = new Button(), btnAnswerAddMedia = new Button(), btnAnswerRemoveMedia = new Button();
    
    ListView<MediaObject> lvMediaManger = new ListView();
    
    ImageView ivMediaPreview = new ImageView();
    
    @FXML
    private TabPane tabPane, questionTabPane;
    
    @FXML
    private Label lblIconFile;
    
    @FXML
    private TextField tfTitle, tfSubtitle, tfGrade, tfAuthor;
    
    @FXML
    private ImageView imgIcon;
    
    @FXML
    private Accordion accCategories;
    
    @FXML
    private TableView<Category> tvCategories;
    
    @FXML
    private TableView<Integer> tvScores;
    
    @FXML
    private TableColumn<Category, String> tcCategory;
    
    @FXML
    private TableColumn<Category, Boolean> tcCategoryType;
    
    @FXML
    private TableColumn<Integer, Integer> tcScores;
    
    @FXML
    private Button btnCAdd, btnCDelete, btnCUp, btnCDown, btnSAdd, btnSDelete, btnSUp, btnSDown;
    
    @FXML
    private ComboBox cbSelectQuestionType, cbScoresystem, cbQuestionsForAllMode, cbQuestionForAllHandling;
    
    @FXML
    private CheckBox cbRules, cbAutoTimerEnd, cbWebDisplayScore;
    
    @FXML
    private HBox hbMediaManager;
    
    private final ObservableList<String> questionsTypes = FXCollections.observableArrayList(
            "ABCD-Frage",
            "Text-Frage",
            "Schätz-Frage",
            "Reihenfolge-Frage"
    ), scoresystems = FXCollections.observableArrayList(
            "NaWiGator-Quiz",
            "Einfach"
    ), questionsForAllModes = FXCollections.observableArrayList(
            "Eine Antwort pro Team (ohne Publikum)",
            "Mehrere Antworten pro Team (Publikum)"
    ), questionForAllHandling = FXCollections.observableArrayList(
            "Wahl immer möglich",
            "nach jeder Runde wählt aktives Team eine Frage aus"
    );
    
    @FXML
    private void btnDoneAction(ActionEvent e) {
        if(QuizData.getCategories().isEmpty() || QuizData.getScores().isEmpty()) {
            e.consume();
            new ExceptionDialog(new IllegalStateException("Es muss mindestens eine Kategorie und eine Punktzahl angegeben sein."),
                "Es muss mindestens eine Kategorie und eine Punktzahl angegeben sein.");
            return;
        }
        applyMetadata();
        Scenes.changeScene("scenes/MainScene.fxml");
    }
    
    private void applyMetadata() {
        QuizData.setQuizTitle(tfTitle.getText());
        QuizData.setQuizSubtitle(tfSubtitle.getText());
        QuizData.setQuizAuthor(tfAuthor.getText());
        QuizData.setGrade(tfGrade.getText());
        QuizData.setQuizIcon(imgIcon.getImage());
        QuizData.debugOutput();
    }
    
    public void keyEvent(KeyEvent event) {
        if(GlobalKeyHandler.CTRL_S.match(event)) applyMetadata();
    }
    
    @FXML
    private void handleBtnContinue1Action(ActionEvent e) {
        tabPane.getSelectionModel().selectNext();
    }
 
    @FXML
    private void handleBtnIconFileAction(ActionEvent e) {
        fileChooser.setTitle("Icon auswählen");
        setupFileChooser(MediaObject.SUPPORTED_MEDIA_FILES_FILTERS[0]);
        iconFile = fileChooser.showOpenDialog(Main.getMainScene().getWindow());
        lblIconFile.setText(iconFile == null ? "Keine ausgewählt" : iconFile.getName());
        if(iconFile != null) try {
            MediaStore.putIcon(iconFile);
            imgIcon.setImage(MediaStore.getIconImage());
            QuizData.setQuizIcon(MediaStore.getIconImage());
        } catch(Exception ex) {
            new ExceptionDialog(ex, "Irgendein Fehler");
        }
        else imgIcon.setImage(null);
    }
    
    private void setupFileChooser(FileChooser.ExtensionFilter... ef) {
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().addAll(ef);
        fileChooser.setSelectedExtensionFilter(ef[0]);
    }
    
    @FXML
    private void handleTabQuestionsSelectionChanged(Event e) {
        if(QuizData.getCategories().isEmpty() || QuizData.getScores().isEmpty()) {
            tabPane.getSelectionModel().select(2);
            e.consume();
            new ExceptionDialog(new IllegalStateException("Es muss mindestens eine Kategorie und eine Punktzahl angegeben sein."),
                "Es muss mindestens eine Kategorie und eine Punktzahl angegeben sein.");
            return;
        }
        QuizData.updateQuestions();
        updateAccordion(accCategories, 0);
    }
    
    @FXML
    private void handleTabMediaManagerSelectionChanged(Event e) {
        updateMediaManagerList();
    }
    
    /**
     * Update categories, scores
     * 
     * @param acc
     * @param expStandard
     */
    public void updateAccordion(Accordion acc, int expStandard) {
        if (QuizData.getCategories().isEmpty()) return;
        ObservableList panes = acc.getPanes();
        panes.clear();
        boolean firstCategory = true;
        for (Category category : QuizData.getCategories()) {
            ObservableList<Question> questionsInCategory = FXCollections.observableList(new ArrayList<>());
            for (int score : QuizData.getScores()) {
                Question question = QuizData.find(category, score);
                if (question != null)  questionsInCategory.add(question);
            }
            ListView<Question> listView = new ListView<>(questionsInCategory);
            listView.prefWidthProperty().bind(listView.widthProperty());
            listView.setCellFactory((ListView<Question> param) -> {
                final ListCell<Question> cell = new ListCell() {
                    private Text text;
                    
                    @Override
                    public void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            text = new Text(item.toString());
                            text.wrappingWidthProperty().bind(listView.prefWidthProperty().subtract(25));
                            setGraphic(text);
                        }
                    }
                };
                
                return cell;
            });
            TitledPane titledPane = new TitledPane(category.getId(), listView);
            listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> questionChanged(oldValue, newValue));
            panes.add(titledPane);
            accordionListViews.put(category, listView);
            if (firstCategory) {
                firstCategory = false;
                listView.getSelectionModel().select(0);
            }
        }
        acc.setExpandedPane(acc.getPanes().get(expStandard));
    }
    
    private void questionChanged(Object oldValue, Object newValue) {
        if ((newValue instanceof Question) && (oldValue instanceof Question))
            if (((Question) newValue).toIdentifier().equals(((Question) oldValue).toIdentifier())) return;
        if (newValue instanceof Question) {
            selectedQuestion = (Question) newValue;
            accordionListViews
                    .entrySet()
                    .stream()
                    .filter(entry -> !selectedQuestion.getCategory().equals(entry.getKey()))
                    .forEach(entry -> entry.getValue().getSelectionModel().clearSelection());
            if (newValue instanceof ABCDQuestion) cbSelectQuestionType.getSelectionModel().select(0);
            else if (newValue instanceof TextQuestion) cbSelectQuestionType.getSelectionModel().select(1);
            else if (newValue instanceof EstimateQuestion) cbSelectQuestionType.getSelectionModel().select(2);
            else if (newValue instanceof SequenceQuestion) cbSelectQuestionType.getSelectionModel().select(3);
            updateQuestionTabs();
        }
        if(oldValue instanceof Question) {
            Question question = (Question) oldValue;
            ObservableList list = accordionListViews.get(question.getCategory()).getItems();
            try {
                list.set(list.indexOf(question), question);
            } catch (ArrayIndexOutOfBoundsException ex) {}
        }
    }
    
    /*
        Questions/Answer Tab
    */
    
    @FXML
    private void handlecbSelectQuestionTypeChanged(Event e) {
        ObservableList<Question> listViewItems = accordionListViews.get(selectedQuestion.getCategory()).getItems();
        int questionIndex = listViewItems.indexOf(selectedQuestion);
        switch(cbSelectQuestionType.getSelectionModel().getSelectedIndex()) {
            case 0:
                if (!(selectedQuestion instanceof ABCDQuestion)) {
                    selectedQuestion = ABCDQuestion.convert(selectedQuestion);
                    QuizData.getQuestions().put(selectedQuestion.toIdentifier(), selectedQuestion);
                }
                break;
            case 1:
                if (!(selectedQuestion instanceof TextQuestion)) {
                    selectedQuestion = TextQuestion.convert(selectedQuestion);
                    QuizData.getQuestions().put(selectedQuestion.toIdentifier(), selectedQuestion);
                }
                break;
            case 2:
                if (!(selectedQuestion instanceof EstimateQuestion)) {
                    selectedQuestion = EstimateQuestion.convert(selectedQuestion);
                    QuizData.getQuestions().put(selectedQuestion.toIdentifier(), selectedQuestion);
                }
                break;
            case 3:
                if (!(selectedQuestion instanceof SequenceQuestion)) {
                    selectedQuestion = SequenceQuestion.convert(selectedQuestion);
                    QuizData.getQuestions().put(selectedQuestion.toIdentifier(), selectedQuestion);
                }
                break;
            default:
                return;
        }
        listViewItems.set(questionIndex, selectedQuestion);
        updateQuestionTabs();
    }
    
    private void updateQuestionTabs() {
        ObservableList<Tab> tabs = questionTabPane.getTabs();
        tabs.clear();
        tabs.addAll(selectedQuestion.getEditQuestionTabs());
    }
    
    //Quiz Settings
    
    @FXML
    private void handleCbScoresystem(ActionEvent event) {
        if(cbScoresystem.getSelectionModel().getSelectedIndex() == 1) {
            //Eventuelle Konvertierung von "Fragen für alle" bei Wechsel von "NaWiGator" nach "Einfach"
            long questionsForAll = QuizData.getCategories().parallelStream().filter(Category::getType).count();
            if(questionsForAll > 0) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Wenn Sie das Punktesystem auf \"Einfach\" ändern, werden " + questionsForAll + " Kategorien von \"Frage für alle\" zu \"Standard\" konvertiert. Forfahren?", ButtonType.YES, ButtonType.NO);
                if(alert.showAndWait().orElse(ButtonType.NO) == ButtonType.NO) {
                    cbScoresystem.getSelectionModel().select(0);
                    return;
                }
                //Ja ausgewählt: "Fragen für alle" entfernen
                QuizData.getCategories().parallelStream().forEach(c -> c.setType(false)); //-> Fortfahren
            }
        }
        QuizData.setScoreSystem(cbScoresystem.getSelectionModel().getSelectedIndex());
    }
    
    @FXML
    private void handleCbQuestionsForAllMode(ActionEvent event) {
        QuizData.setQuestionsForAllMode(cbQuestionsForAllMode.getSelectionModel().getSelectedIndex());
    }
    
    @FXML
    private void handleCbQuestionForAllHandling(ActionEvent event) {
        QuizData.setQuestionForAllHandling(cbQuestionForAllHandling.getSelectionModel().getSelectedIndex());
    }
    
    @FXML
    private void handleCbRules(ActionEvent event) {
        QuizData.setShowRules(cbRules.isSelected());
    }
    
    @FXML
    private void handleCbAutoTimerEnd(ActionEvent event) {
        QuizData.setAutoTimerEnd(cbAutoTimerEnd.isSelected());
    }
    
    @FXML
    private void handleCbWebDisplayScore(ActionEvent event) {
        QuizData.setWebDisplayScore(cbWebDisplayScore.isSelected());
    }
    
    @FXML
    private void handleBtnOpenDatabaseAction(ActionEvent event) {
        LogHelper.debug("Öffne Datenbank...");
        Parent root;
        try {
            root = FXMLLoader.load(Main.class.getResource("scenes/database/QuestionDatabaseScene.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Fragen-Datenbank");
            stage.getIcons().add(new Image(Main.class.getResourceAsStream("res/favicon.png")));
            stage.setScene(new Scene(root));
            stage.show();
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    System.out.println("Schließe Datenbank...");
                    updateAccordion(accCategories, 0);
                    updateQuestionTabs();
                }
            });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //Edit Quiz Kategorien und Punktzahlen
    
    private boolean categoryExists(String string) {
        return QuizData.getCategories().parallelStream().anyMatch(cat -> cat.getId().equalsIgnoreCase(string));
    }
    
    private boolean scoreExists(Integer score) {
        return QuizData.getScores().parallelStream().anyMatch(i -> i.equals(score));
    }
    
    @FXML
    private void handleBtnCAddAction() {
        String string2 = "neue Kategorie";
        for(int i = 2; categoryExists(string2); i++) {
            string2 = "neue Kategorie" + " " + i;
        }
        tvCategories.getItems().add(new Category(string2));
    }
    
    @FXML
    private void handleBtnCDeleteAction() {
        tvCategories.getItems().remove(tvCategories.getSelectionModel().getSelectedIndex());
    }
    
    @FXML
    private void handleBtnCUpAction() {
        if (tvCategories.getSelectionModel().getSelectedIndex() > 0) {
            int i = tvCategories.getSelectionModel().getSelectedIndex();
            if (i-1 >= 0) {
                Category cgt = tvCategories.getItems().get(i);
                QuizData.getCategories().set(i, QuizData.getCategories().get(i-1));
                QuizData.getCategories().set(i-1, cgt);
                tvCategories.getSelectionModel().select(i-1);
            }
        }
    }
    
    @FXML
    private void handleBtnCDownAction() {
        if (tvCategories.getSelectionModel().getSelectedIndex() >= 0) {
            int i = tvCategories.getSelectionModel().getSelectedIndex();
            if (i+1 < QuizData.getCategories().size()) {
                Category cgt = tvCategories.getItems().get(i);
                QuizData.getCategories().set(i, QuizData.getCategories().get(i+1));
                QuizData.getCategories().set(i+1, cgt);
                tvCategories.getSelectionModel().select(i+1);
            }
        }
    }
    
    @FXML
    private void handleBtnSAddAction() {
        Integer score = 10;
        while (scoreExists(score)) score++;
        tvScores.getItems().add(score);
    }
    
    @FXML
    private void handleBtnSDeleteAction() {
        tvScores.getItems().remove(tvScores.getSelectionModel().getSelectedIndex());
    }
    
    @FXML
    private void handleBtnSUpAction() {
        if (tvScores.getSelectionModel().getSelectedIndex() > 0) {
            int i = tvScores.getSelectionModel().getSelectedIndex();
            if (i-1 >= 0) {
                Integer score = tvScores.getItems().get(i);
                QuizData.getScores().set(i, QuizData.getScores().get(i-1));
                QuizData.getScores().set(i-1, score);
                tvScores.getSelectionModel().select(i-1);
            }
        }
    }
    
    @FXML
    private void handleBtnSDownAction() {
        if (tvScores.getSelectionModel().getSelectedIndex() >= 0) {
            int i = tvScores.getSelectionModel().getSelectedIndex();
            if (i+1 < QuizData.getScores().size()) {
                Integer score = tvScores.getItems().get(i);
                QuizData.getScores().set(i, QuizData.getScores().get(i+1));
                QuizData.getScores().set(i+1, score);
                tvScores.getSelectionModel().select(i+1);
            }
        }
    }
    
    
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @FXML
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbSelectQuestionType.setItems(questionsTypes);
        cbScoresystem.setItems(scoresystems);
        cbScoresystem.getSelectionModel().select(0);
        cbQuestionsForAllMode.setItems(questionsForAllModes);
        cbQuestionsForAllMode.getSelectionModel().select(0);
        cbQuestionForAllHandling.setItems(questionForAllHandling);
        cbQuestionForAllHandling.getSelectionModel().select(0);
        Main.getKeyHandler().addPreEventHandler(this::keyEvent);
        
        lvMediaManger.setMinWidth(400D);
        lvMediaManger.setCellFactory(param -> new ListCell<MediaObject>() {
            
                    @Override
                    protected void updateItem(MediaObject item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item != null && item.getMediaPath() != null) {
                            ImageView imageView = null;
                            if (item instanceof ImageObject) {
                                imageView = new ImageView(((ImageObject) item).getImage());
                            } else if (item instanceof SoundObject) {
                                imageView = new ImageView(new Image("/de/mafel/nawilauncher/res/audio.png", true));
                            } else if (item instanceof VideoObject) {
                                imageView = new ImageView(new Image("/de/mafel/nawilauncher/res/playMA.png", true));
                            }
                            setText(QuizData.getMediaName(item.getMediaPath()));
                            imageView.setPreserveRatio(true);
                            imageView.setFitHeight(80);
                            setGraphic(imageView);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                    
                });
        
        VBox vb = new VBox();
        
            ToolBar tb = new ToolBar();
                    tb.setPrefHeight(35D);
                    tb.setPrefWidth(397D);
                
                    Button btnAddMediaManager = new Button("Hinzufügen");
                    btnAddMediaManager.setMnemonicParsing(false);
                    btnAddMediaManager.setOnAction(event -> handleBtnAddMediaAction(event));
                    btnAddMediaManager.setPrefHeight(25D);
                    btnAddMediaManager.setPrefWidth(169D);
                    btnAddMediaManager.getStyleClass().add("dataBtn");
                    
                    Button btnRemoveMedia = new Button("Entfernen");
                    btnRemoveMedia.setMnemonicParsing(false);
                    btnRemoveMedia.setOnAction(event -> handleBtnRemoveMediaAction(event));
                    btnRemoveMedia.setPrefHeight(25D);
                    btnRemoveMedia.setPrefWidth(128D);
                    btnRemoveMedia.getStyleClass().add("dataBtn");
                    btnRemoveMedia.setAlignment(Pos.CENTER);
                
            tb.getItems().addAll(btnAddMediaManager, btnRemoveMedia);
                
        VBox.setVgrow(lvMediaManger, Priority.ALWAYS);
        vb.getChildren().addAll(lvMediaManger, tb);
            
        ivMediaPreview.setPreserveRatio(true);
        ivMediaPreview.fitHeightProperty().bind(lvMediaManger.heightProperty());
        ivMediaPreview.fitWidthProperty().bind(hbMediaManager.widthProperty().subtract(lvMediaManger.widthProperty()).subtract(20));
        lvMediaManger.getSelectionModel().selectedItemProperty().addListener((o, v1, v2) -> {
            //TODO FIXME Für alle Mediatypen Previews bauen und in ListView Thumbnails anzeigen.
            if (v2 instanceof ImageObject && v2 != null && ((ImageObject) v2).getImage() != null) ivMediaPreview.setImage(((ImageObject) v2).getImage());
        });
        lvMediaManger.setOnMouseClicked((MouseEvent mouseEvent) -> {
            if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                if(mouseEvent.getClickCount() == 2){
                    TextInputDialog dialog = new TextInputDialog(QuizData.getMediaName(lvMediaManger.getSelectionModel().getSelectedItem().getMediaPath()));
                    dialog.setTitle("Namen ändern");
                    dialog.setHeaderText("Hier können Sie den Namen ändern");
                    dialog.setContentText("Bitte geben Sie den neuen Namen ein:");
                    Optional<String> result = dialog.showAndWait();
                    result.ifPresent(name -> {
                        QuizData.setMediaName(lvMediaManger.getSelectionModel().getSelectedItem().getMediaPath(), name);
                        updateMediaManagerList();
                    });
                }
            }
        });
        
        hbMediaManager.getChildren().addAll(vb, ivMediaPreview);
        
        //Edit Quiz Kategorien und Punktzahlen
        tcCategory.setCellValueFactory(param ->
            new ReadOnlyObjectWrapper<>(param.getValue().getId())
        );
        tcCategory.setOnEditCommit(e -> {
            e.getRowValue().updateId(e.getNewValue());
        });
        tcCategory.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<String>() {
            @Override
            public String toString(String object) {
                return object;
            }

            @Override
            public String fromString(String string) {
                String string2 = string;
                for(int i = 2; categoryExists(string2); i++) {
                    string2 = string + " " + i;
                }
                return string2;
            }
        }));
        tcCategory.prefWidthProperty().bind(tvCategories.widthProperty().multiply(2.0 / 3.0).subtract(12));
        tvCategories.setTooltip(new Tooltip("Zum Ändern Doppelklick. Zum Bestätigen Enter.\n\"Fragen an alle\" ist nur verfügbar, wenn das Punktesystem \"Nawigator\" ausgewählt wurde."));
        
        tcCategoryType.editableProperty().bind(QuizData.scoreSystemProperty().isEqualTo(QuizData.ScoreSystem.NAWIGATOR));
        tcCategoryType.setCellValueFactory(param ->
            param.getValue().typeProperty()
        );
        tcCategoryType.setOnEditCommit(e -> {
            if(e.getNewValue()) {//Änderung zu "Frage für alle"
                if(QuizData.getQuestionsForCategory(e.getRowValue()).filter(q -> (q instanceof TextQuestion)).findAny().isPresent()) {
                    new ExceptionDialog(new IllegalStateException("TextQuestions sind in Fragen für alle nicht zulässig."), "Textfragen sind in \"Fragen für alle\"-Kategorien nicht zulässig. Bitte entfernen sie alle Textfragen aus dieser Kategorie, bevor Sie fortfahren.");
                    e.getRowValue().setType(false);
                }
            } else e.getRowValue().setType(true);
        });
        tcCategoryType.setCellFactory(CheckBoxTableCell.forTableColumn(row -> tcCategoryType.getCellObservableValue(row)));
        tcCategoryType.prefWidthProperty().bind(tvCategories.widthProperty().divide(3).subtract(12));
        
        tcScores.setCellValueFactory(param -> 
            new ReadOnlyObjectWrapper<>(param.getValue())
        );
        tcScores.setOnEditCommit(e -> {
            QuizData.getScores().set(e.getTablePosition().getRow(), e.getNewValue());
        });
        tcScores.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Integer>() {
            
            @Override
            public String toString(Integer object) {
                return "" + object;
            }

            @Override
            public Integer fromString(String string) {
                Integer score = Integer.parseInt(string);
                while (scoreExists(score)) score++;
                return score;
            }
            
        }));
        tcScores.prefWidthProperty().bind(tvScores.widthProperty().subtract(24));
        
        tvCategories.setItems(QuizData.getCategories());
        tvScores.setItems(QuizData.getScores());
        
        cbQuestionsForAllMode.disableProperty().bind(QuizData.scoreSystemProperty().isNotEqualTo(QuizData.ScoreSystem.NAWIGATOR));
        cbQuestionForAllHandling.disableProperty().bind(QuizData.scoreSystemProperty().isNotEqualTo(QuizData.ScoreSystem.NAWIGATOR));
    }
    
    private void handleBtnAddMediaAction(ActionEvent e) {
        fileChooser.setTitle("Mediendatei auswählen");
        setupFileChooser(MediaObject.SUPPORTED_MEDIA_FILES_FILTERS);
        File mediaFile = fileChooser.showOpenDialog(Main.getMainScene().getWindow());
        if(mediaFile != null) {
            MediaStore.put(mediaFile, mediaFile.getName());
            updateMediaManagerList();
        }
    }
    
    private void handleBtnRemoveMediaAction(ActionEvent e) {
        ObservableList<MediaObject> indices = lvMediaManger.getSelectionModel().getSelectedItems();
        indices.forEach(index -> MediaStore.remove(index.getMediaPath()));
        updateMediaManagerList();
        QuizData.getQuestions().forEach((qi, q) -> {
            indices.forEach(io -> {
                q.getMedia().forEach(io2 -> {
                    if (io2.equals(io)) q.getMedia().remove(io);
                });
            });
        });
    }
    
    private void updateMediaManagerList() {
        lvMediaManger.getItems().clear();
        MediaStore.getEntries().forEach(e -> {
            String mediaExtension = e.split("\\.")[1];
            String mediaName = e.split("\\.")[0];
            System.out.println("e: " + e + ", Extension: " + mediaExtension);
            if ("png.jpg.jpeg.gif.bmp".contains(mediaExtension)) {
                lvMediaManger.getItems().add(new ImageObject(e, mediaName, true));
            } else if ("mp3.wav.aac.wma".contains(mediaExtension)) {
                lvMediaManger.getItems().add(new SoundObject(e, mediaName, true));
            } else if ("mp4.mov.avi".contains(mediaExtension)) {
                lvMediaManger.getItems().add(new VideoObject(e, mediaName, true));
            }     
        });
    }

    @Override
    public void onSceneShown() { 
        tabLoaded = false;
        tfTitle.setText(QuizData.getQuizTitle());
        tfSubtitle.setText(QuizData.getQuizSubtitle());
        tfAuthor.setText(QuizData.getQuizAuthor());
        tfGrade.setText(QuizData.getGrade());
        lblIconFile.setText("");
        imgIcon.setImage(QuizData.getQuizIcon());
        cbScoresystem.getSelectionModel().select(QuizData.getScoreSystemInt());
        cbQuestionsForAllMode.getSelectionModel().select(QuizData.getQuestionsForAllModeInt());
        cbQuestionForAllHandling.getSelectionModel().select(QuizData.getQuestionForAllHandlingInt());
        cbRules.setSelected(QuizData.isShowRules());
        cbAutoTimerEnd.setSelected(QuizData.isAutoTimerEnd());
        cbWebDisplayScore.setSelected(QuizData.isWebDisplayScore());
        //QuizData.getLastEdited()
        updateAccordion(accCategories, 0);
        if(selectedQuestion != null) updateQuestionTabs();
        updateMediaManagerList();
    }
    
        
}
