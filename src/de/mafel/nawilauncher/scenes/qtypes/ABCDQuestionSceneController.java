package de.mafel.nawilauncher.scenes.qtypes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.media.PlayerStage;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.Scenes;
import de.mafel.nawilauncher.scenes.components.GUtils;
import de.mafel.nawilauncher.scenes.components.Popup;
import de.mafel.nawilauncher.scenes.components.Timer;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.scene.media.MediaException;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author Felix
 */
public class ABCDQuestionSceneController implements Initializable, DynamicController {
    
    private int correctAnswer = -1;
    private ABCDQuestion activeQuestion;
    
    Timer timer = new Timer(60, true);
    
    PlayerStage ps = new PlayerStage();
    
    AudioClip ac;
    
    @FXML
    private Label lblBack, lblQuestion, lblCatScore, lblVideo, lblHelp, lblContinue;
    
    @FXML
    private Button btnA, btnB, btnC, btnD;
    
    @FXML
    private AnchorPane mainPane;
    
    @FXML
    private HBox imageBox;
    
    @FXML
    private VBox leftVbox, rightVbox, vbox;
    
    @FXML
    private HBox hboxAnswers, mediaHbar;
    
    @FXML
    private void handleLblBackClicked(MouseEvent e) {
        Navigator.markQuestionDone();
        Navigator.resetActiveQuestion();
        timer.stop();
        Scenes.changeScene("scenes/CategoryScene.fxml");
    }
    
    @FXML
    private void handleLblContinueClicked(MouseEvent e) {
        //Punkte der einzelnen Teams ausrechnen
        if(timer.getCurrentTime() > 0) {
            Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
            infoAlert.initOwner(Main.getStage());
            infoAlert.setHeaderText("Frage beenden?");
            infoAlert.setTitle("Info");
            infoAlert.setContentText("Wollen sie wirklich die Frage auflösen? Die Zeit ist noch nicht um. Dies kann nicht rückgängig gemacht werden.");
            
            ButtonType btOk = new ButtonType("OK");
            ButtonType btCancel = new ButtonType("Abbrechen");
                    
            infoAlert.getButtonTypes().clear();
            infoAlert.getButtonTypes().addAll(btOk, btCancel);
            
            Optional<ButtonType> result = infoAlert.showAndWait();
            if (result.get() != btOk) return;
        }
        finishQuestion();
    }
    
    @FXML
    private void handleLblVideoClicked() {
        ps.openVideo();
    }
    
    @FXML
    private void handleLblHelpHovered() {
        Popup.showTooltip(Main.getStage(), lblHelp, "Für die lokalen Antworten ziehen Sie bitte die entsprechenden Teams links oben auf die Antwort.", null);
    }
    
    private void finishQuestion() {
        if(Navigator.getQuizState() == QuizState.QUESTION) Navigator.setQuizState(QuizState.EXPLANATION);
        timer.stop();
        LogHelper.debug("type=" + activeQuestion.getCategory().getType() + ",mode=" + QuizData.getQuestionsForAllMode().name());
        if(activeQuestion.getCategory().getType() && QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.MULTIPLE_ANSWERS_PER_TEAM) {
            if(Navigator.isPlickersEnabled()) {
                Scenes.changeScene("scenes/plickers/PlickersCollectionScene.fxml");
            } else {
                activeQuestion.createResults();
                TeamManager.resetAnswers();
                Main.getServer().getWebSocketServer().resetIndividualAnswers();
                Scenes.changeScene("scenes/ExplanationScene.fxml");
            }
        } else {
            activeQuestion.createResults();
            TeamManager.resetAnswers();
            Scenes.changeScene("scenes/ExplanationScene.fxml");
        }
    }
    
    public Timer getTimer() {
        return timer;
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        int tlength = TeamManager.getTeams().size();
        HBox teamHBox = new HBox(8);
        teamHBox.setLayoutX(300);
        teamHBox.setLayoutY(14);
        teamHBox.getChildren().addAll(TeamManager.getAllTeamLabels());
        mainPane.getChildren().add(teamHBox);
        
        OnDragOverHandler onDragOverHandler = new OnDragOverHandler();
        OnDragEnteredHandler onDragEnteredHandler = new OnDragEnteredHandler();
        OnDragExitedHandler onDragExitedHandler = new OnDragExitedHandler();
        
        btnA.setOnDragOver(onDragOverHandler);
        btnA.setOnDragEntered(onDragEnteredHandler);
        btnA.setOnDragExited(onDragExitedHandler);
        btnA.setOnDragDropped(new OnDragDroppedHandler(0));
        
        btnB.setOnDragOver(onDragOverHandler);
        btnB.setOnDragEntered(onDragEnteredHandler);
        btnB.setOnDragExited(onDragExitedHandler);
        btnB.setOnDragDropped(new OnDragDroppedHandler(1));
        
        btnC.setOnDragOver(onDragOverHandler);
        btnC.setOnDragEntered(onDragEnteredHandler);
        btnC.setOnDragExited(onDragExitedHandler);
        btnC.setOnDragDropped(new OnDragDroppedHandler(2));
        
        btnD.setOnDragOver(onDragOverHandler);
        btnD.setOnDragEntered(onDragEnteredHandler);
        btnD.setOnDragExited(onDragExitedHandler);
        btnD.setOnDragDropped(new OnDragDroppedHandler(3));
        
        leftVbox.prefWidthProperty().bind(Main.getMainScene().widthProperty().divide(2));
        rightVbox.prefWidthProperty().bind(Main.getMainScene().widthProperty().divide(2));
        GUtils.bindFontSizeToBounds(lblQuestion);
        GUtils.bindFontSizeToBounds(btnA);
        GUtils.bindFontSizeToBounds(btnB);
        GUtils.bindFontSizeToBounds(btnC);
        GUtils.bindFontSizeToBounds(btnD);
        
        //Timer initialisieren
        mediaHbar.getChildren().add(timer.getTimer());
        timer.timerStateProperty().addListener((o, v1, v2) -> {
            if (activeQuestion.getTimeLimit() > 0 && v1 && !v2 && QuizData.isAutoTimerEnd()) finishQuestion();
        });
        
        lblQuestion.prefHeightProperty().bind(vbox.heightProperty().subtract(hboxAnswers.heightProperty()).divide(3).add(30));
        imageBox.prefHeightProperty().bind(vbox.heightProperty().subtract(hboxAnswers.heightProperty()).multiply(2.0 / 3.0).subtract(30));
        
        //Timer anhalten bei Video
        ps.videoHasEndedProperty().addListener((o, v1, v2) -> {
            if(!v2) timer.stop();
            else timer.resume();
        });
    }    

    @Override
    public void onSceneShown() {
        if(!(Navigator.getActiveQuestion() instanceof ABCDQuestion)) return;
        //QuizState updaten
        Navigator.setQuizState(QuizState.QUESTION);
        
        activeQuestion = (ABCDQuestion) Navigator.getActiveQuestion();
        if(activeQuestion != null) {
            lblQuestion.setText(activeQuestion.getQuestion());
            lblCatScore.setText(activeQuestion.getCategory().getId() + " " + activeQuestion.getScore());
            btnA.setText("A: " + activeQuestion.getAnswers()[0]);
            btnB.setText("B: " + activeQuestion.getAnswers()[1]);
            btnC.setText("C: " + activeQuestion.getAnswers()[2]);
            btnD.setText("D: " + activeQuestion.getAnswers()[3]);
            correctAnswer = activeQuestion.getCorrectAnswer();            
        }
        
        //Medien
        if(!activeQuestion.getMedia().isEmpty()) try {
            GUtils.createImageBox(imageBox, activeQuestion);
            imageBox.setVisible(true);
            imageBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        } catch(MediaException ex) {
            new ExceptionDialog(ex, "Die Mediendatei konnte nicht eingefügt werden: " + ex.getMessage()); 
        }
        else {
            imageBox.setVisible(false);
        }
        
        lblContinue.setText(activeQuestion.getCategory().getType() && QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.MULTIPLE_ANSWERS_PER_TEAM && Navigator.isPlickersEnabled() ? "Antworten sammeln" : "Weiter");
        
        //VideoBtn updaten
        lblVideo.setVisible(false);
        mediaHbar.getChildren().retainAll(lblVideo, timer.getTimer()); //alles entfernen außer Timer, Video
        activeQuestion.getMedia().forEach(item -> {
            if (item instanceof VideoObject) {
                lblVideo.setVisible(true);
                ps.setVideo((VideoObject) item);
            } else if (item instanceof SoundObject) {
                Label label = new Label(QuizData.getMediaName(item.getMediaPath()));
                label.setPrefHeight(42);
                label.setPrefWidth(120);
                label.setAlignment(Pos.CENTER);
                label.getStyleClass().add("movieDisplayLabel");
                label.setOnMouseClicked(event -> {
                    ac = ((SoundObject) item).getSound();
                    if (ac.isPlaying()) {
                        ac.stop();
                        label.setGraphic(null);
                    } else {
                        ac.play();
                        label.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Play.png"), 30, 30, true, true)));
                    }
                });
                mediaHbar.getChildren().add(label);
            }
        });
        //Timer updaten & starten
        if(activeQuestion.getTimeLimit() > 0) {
            timer.showTimer();
            timer.startOnClick(activeQuestion.getTimeLimit());
        } else {
            timer.hideTimer();
        }
        //Automatische Größenanpassung
        GUtils.adjustFontSizeToBounds(lblQuestion);
        GUtils.adjustFontSizeToBounds(btnA);
        GUtils.adjustFontSizeToBounds(btnB);
        GUtils.adjustFontSizeToBounds(btnC);
        GUtils.adjustFontSizeToBounds(btnD);
    }
    
    protected class OnDragOverHandler implements javafx.event.EventHandler<DragEvent> {
        
        @Override
        public void handle(DragEvent event) {
            /* data is dragged over the target */
            /* accept it only if it is not dragged from the same node 
             * and if it has a string data */
            if (event.getGestureSource() != event.getSource() && event.getDragboard().hasString()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }

            event.consume();
        }
        
    }
    
    protected class OnDragEnteredHandler implements javafx.event.EventHandler<DragEvent> {
        
        @Override
        public void handle(DragEvent event) {
            if(!(event.getSource() instanceof Button)) return;
            Button src = (Button) event.getSource();
            if(event.getGestureSource() != src && event.getDragboard().hasString()) src.getStyleClass().add("btndragover");
        }
        
    }
    
    protected class OnDragExitedHandler implements javafx.event.EventHandler<DragEvent> {
        
        @Override
        public void handle(DragEvent event) {
            if(!(event.getSource() instanceof Button)) return;
            Button src = (Button) event.getSource();
            src.getStyleClass().remove("btndragover");
        }
        
    }
    
    protected class OnDragDroppedHandler implements javafx.event.EventHandler<DragEvent> {
        
        private int answer;
        
        public OnDragDroppedHandler(int answer) {
            this.answer = answer;
        }
        
        @Override
        public void handle(DragEvent event) {
            if (timer.isTimerState()) {
                Dragboard db = event.getDragboard();
                boolean success = db.hasString();
                if(success) try {
                    int teamIdTransmitted = Integer.parseInt(db.getString());
                    Team t = TeamManager.getTeam(teamIdTransmitted);
                    t.setAnswer(answer);
                } catch(NumberFormatException ex) {
                    new ExceptionDialog(ex, "Es ist ein Fehler beim Verarbeiten der Drag And Drop Informationen aufgetreten.");
                    event.setDropCompleted(false);
                }
                event.setDropCompleted(success);
                event.consume();
            }
        }
        
    }
    
}
