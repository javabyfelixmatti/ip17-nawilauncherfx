package de.mafel.nawilauncher.scenes.qtypes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.TeamResult;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.scenes.Scenes;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Felix
 */
public class TextQuestionControlSceneController implements Initializable, DynamicController {

    @FXML
    private Label lblExplanation, lblContinue, lblQuestion, lblCatScore;
    
    @FXML
    private HBox hbox;
    
    @FXML
    private Accordion accAnswers;
    
    private HashMap<Team, Boolean> answerCorrect = new HashMap<>();
    private Question activeQuestion = null;
    
    @FXML
    private void handleLblBackClicked(MouseEvent e) {
        Navigator.markQuestionDone();
        Navigator.resetActiveQuestion();
        Scenes.changeScene("scenes/CategoryScene.fxml");
    }
    
    @FXML
    private void handleLblContinueClicked(MouseEvent e) {
        if(lblContinue.isDisable()) return;
        switch(QuizData.getScoreSystem()) {
            case SIMPLE:
                answerCorrect
                        .entrySet()
                        .stream()
                        .filter(entry -> entry.getValue()) //Filtere alle korrekten Antworten
                        .forEach(entry -> {
                            TeamResult tr = Navigator.getResult(entry.getKey());
                            if(tr == null) return;
                            tr.setScoreDiff(activeQuestion.getScore());
                            tr.setCorrect(true);
                        }); //Gib Punkte
                break;
            case NAWIGATOR:
                Team activeTeam = TeamManager.getActiveTeam();
                boolean activeTeamCorrect = answerCorrect.getOrDefault(activeTeam, false);
                if(activeTeamCorrect) {
                    TeamResult tr = Navigator.getResult(activeTeam);
                    tr.setScoreDiff(activeQuestion.getScore());
                    tr.setCorrect(true);
                }
                answerCorrect
                        .entrySet()
                        .stream()
                        .filter(entry -> !entry.getKey().equals(activeTeam) && entry.getValue()) //Filtere alle korrekten Antworten
                        .forEach(entry -> {
                            TeamResult tr = Navigator.getResult(entry.getKey());
                            if(tr == null) return;
                            tr.setScoreDiff(activeTeamCorrect ? 0 : activeQuestion.getScore());
                            tr.setCorrect(true);
                        }); //Gib Punkte
                break;
        }
        TeamManager.setNextActiveTeam();
        Scenes.changeScene("scenes/ExplanationScene.fxml");
    }
    
    public TitledPane getAnswerPane(Team t) {
        TitledPane tp = new TitledPane();
        tp.setText(t.getName());
        VBox contents = new VBox(20);
            TeamResult result = Navigator.getResult(t);
            Label answerLabel = new Label(result == null || result.getAnswer() == null ? "-" : result.getAnswer());
            answerLabel.setMaxHeight(Double.MAX_VALUE);
            answerLabel.setMaxWidth(Double.MAX_VALUE);
            answerLabel.setWrapText(true);
            //HBox mit den Buttons
            HBox buttons = new HBox(20);
                //Richtig/Falsch Buttons
                Button btnCorrect = new Button("Richtig"), btnWrong = new Button("Falsch");
                btnCorrect.setMaxWidth(300.0);
                btnWrong.setMaxWidth(300.0);
                btnCorrect.setOnAction(e -> {
                    if(!btnCorrect.getStyleClass().contains("selected")) btnCorrect.getStyleClass().add("selected");
                    btnWrong.getStyleClass().remove("selected");
                    answerCorrect.put(t, true);
                    checkDone();
                    //zu nächster Pane springen
                    ObservableList<TitledPane> panes = accAnswers.getPanes();
                    int currentIndex = panes.indexOf(tp);
                    GUtils.adjustFontSizeToBounds(answerLabel);
                    if(currentIndex < 0 || currentIndex >= panes.size() - 1) return;
                    accAnswers.setExpandedPane(panes.get(currentIndex + 1));
                });
                btnWrong.setOnAction(e -> {
                    if(!btnWrong.getStyleClass().contains("selected")) btnWrong.getStyleClass().add("selected");
                    btnCorrect.getStyleClass().remove("selected");
                    answerCorrect.put(t, false);
                    checkDone();
                    //zu nächster Pane springen
                    ObservableList<TitledPane> panes = accAnswers.getPanes();
                    int currentIndex = panes.indexOf(tp);
                    GUtils.adjustFontSizeToBounds(answerLabel);
                    if(currentIndex < 0 || currentIndex >= panes.size() - 1) return;
                    accAnswers.setExpandedPane(panes.get(currentIndex + 1));
                });
            buttons.getChildren().addAll(btnCorrect, btnWrong);
            buttons.setAlignment(Pos.TOP_RIGHT);
        VBox.setVgrow(answerLabel, Priority.ALWAYS);
        contents.getChildren().addAll(answerLabel, buttons);
        contents.setPadding(new Insets(10));
        contents.setMaxHeight(Double.MAX_VALUE);
        contents.setMaxWidth(Double.MAX_VALUE);
        contents.setFillWidth(true);
        VBox.setVgrow(answerLabel, Priority.ALWAYS);
        tp.setContent(contents);
        GUtils.bindFontSizeToBounds(answerLabel);
        tp.heightProperty().addListener((o, v1, v2) -> GUtils.adjustFontSizeToBounds(answerLabel));
        return tp;
    }
    
    private void checkDone() {
        if(answerCorrect.size() == TeamManager.getTeams().size())
            lblContinue.setDisable(false);
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        GUtils.bindFontSizeToBounds(lblExplanation);
        lblExplanation.maxWidthProperty().bind(hbox.widthProperty().divide(2));
        lblExplanation.minWidthProperty().bind(hbox.widthProperty().divide(2));
    }    

    @Override
    public void onSceneShown() {
        activeQuestion = Navigator.getActiveQuestion();
        lblContinue.setDisable(true);
        lblExplanation.setText(Navigator.getActiveQuestion().getExplanation());
        lblQuestion.setText(activeQuestion.getQuestion());
        lblCatScore.setText(activeQuestion.getCategory().getId() + " " + activeQuestion.getScore());
        ObservableList<TitledPane> answerPanes = accAnswers.getPanes();
        accAnswers.setExpandedPane(null);
        answerPanes.clear();
        answerPanes.addAll(
                TeamManager.getTeams()
                        .parallelStream()
                        .map(this::getAnswerPane)
                        .toArray(TitledPane[]::new)
        );
        accAnswers.setExpandedPane(answerPanes.get(0));
        //Hard coded event call to adjust font size
        Platform.runLater(() -> {
            try {
                GUtils.adjustFontSizeToBounds(((Label) ((VBox) answerPanes.get(0).getContent()).getChildren().get(0)));
            } catch (Exception e) {
                LogHelper.error(e);
            }
        });
    }
    
}
