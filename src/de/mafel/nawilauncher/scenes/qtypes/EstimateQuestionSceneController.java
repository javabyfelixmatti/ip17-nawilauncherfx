package de.mafel.nawilauncher.scenes.qtypes;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.data.questions.EstimateQuestion;
import de.mafel.nawilauncher.media.PlayerStage;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.Scenes;
import de.mafel.nawilauncher.scenes.components.GUtils;
import de.mafel.nawilauncher.scenes.components.Popup;
import de.mafel.nawilauncher.scenes.components.Timer;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.scene.media.MediaException;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author Felix
 */
public class EstimateQuestionSceneController implements Initializable, DynamicController {
    
    private EstimateQuestion activeQuestion;
    
    Timer timer = new Timer(60, false);
    
    PlayerStage ps = new PlayerStage();
    
    AudioClip ac;
    
    @FXML
    private Label lblBack, lblQuestion, lblCatScore, lblVideo, lblHelp;
    
    @FXML
    private AnchorPane mainPane;
    
    @FXML
    private HBox imageBox, mediaHbar;
    
    @FXML
    private VBox vbox;
    
    @FXML
    private void handleLblBackClicked(MouseEvent e) {
        Navigator.markQuestionDone();
        Navigator.resetActiveQuestion();
        timer.stop();
        Scenes.changeScene("scenes/CategoryScene.fxml");
    }
    
    @FXML
    private void handleLblContinueClicked(MouseEvent e) {
        //Punkte der einzelnen Teams ausrechnen
        if(timer.getCurrentTime() > 0) {
            Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
            infoAlert.initOwner(Main.getStage());
            infoAlert.setHeaderText("Frage beenden?");
            infoAlert.setTitle("Info");
            infoAlert.setContentText("Wollen sie wirklich die Frage auflösen? Die Zeit ist noch nicht um. Dies kann nicht rückgängig gemacht werden.");
            
            ButtonType btOk = new ButtonType("Ok");
            ButtonType btCancel = new ButtonType("Abbrechen");
                    
            infoAlert.getButtonTypes().clear();
            infoAlert.getButtonTypes().addAll(btOk, btCancel);
            
            Optional<ButtonType> result = infoAlert.showAndWait();
            if (result.get() != btOk) return;
        }
        finishQuestion();
    }
    
    @FXML
    private void handleLblHelpHovered() {
        Popup.showTooltip(Main.getStage(), lblHelp, "Lokal: Für die Auswertung der Antworten klicken Sie anschließend auf der nächsten Seite auf Richtig oder Falsch.", null);
    }
    
    @FXML
    private void handleLblVideoClicked() {
        ps.openVideo();
    }
    
    private void finishQuestion() {
        timer.stop();
        Main.getServer().forceCollectAllAnswers(() -> {
            activeQuestion.createResults();
            TeamManager.resetAnswers();
            if(Navigator.getQuizState() == QuizState.QUESTION) Navigator.setQuizState(QuizState.EXPLANATION);
            //Scene changen
            Platform.runLater(() -> Scenes.changeScene("scenes/ExplanationScene.fxml"));
        });
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        int tlength = TeamManager.getTeams().size();
        HBox teamHBox = new HBox(8);
        teamHBox.setLayoutX(300);
        teamHBox.setLayoutY(14);
        teamHBox.getChildren().addAll(TeamManager.getAllTeamLabels());
        mainPane.getChildren().add(teamHBox);
        GUtils.bindFontSizeToBounds(lblQuestion);
        
        //Timer initialisieren
        mediaHbar.getChildren().add(timer.getTimer());
        timer.timerStateProperty().addListener((o, v1, v2) -> {
            if (QuizData.isAutoTimerEnd() && v1 && !v2) finishQuestion();
        });
        
        lblQuestion.prefHeightProperty().bind(vbox.heightProperty().divide(3).add(30));
        imageBox.prefHeightProperty().bind(vbox.heightProperty().multiply(2.0 / 3.0).subtract(30));
        
        //Timer anhalten bei Video
        ps.videoHasEndedProperty().addListener((o, v1, v2) -> {
            if(!v2) timer.stop();
            else timer.resume();
        });
    }    

    @Override
    public void onSceneShown() {
        if(!(Navigator.getActiveQuestion() instanceof EstimateQuestion)) return;
        activeQuestion = (EstimateQuestion) Navigator.getActiveQuestion();
        if(activeQuestion != null) {
            lblQuestion.setText(activeQuestion.getQuestion());
            lblCatScore.setText(activeQuestion.getCategory().getId() + " " + activeQuestion.getScore());
        }
        
        if(!activeQuestion.getMedia().isEmpty()) try {
            GUtils.createImageBox(imageBox, activeQuestion);
            imageBox.setVisible(true);
            imageBox.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        } catch(MediaException ex) {
            new ExceptionDialog(ex, "Die Mediendatei konnte nicht eingefügt werden: " + ex.getMessage());
        }
        else {
            imageBox.setVisible(false);
        }
        
        //QuizState updaten
        Navigator.setQuizState(QuizState.QUESTION);
        
         //VideoBtn updaten
        lblVideo.setVisible(false);
        mediaHbar.getChildren().retainAll(lblVideo, timer.getTimer()); //alles entfernen außer Timer, Video
        activeQuestion.getMedia().forEach(item -> {
            if (item instanceof VideoObject) {
                lblVideo.setVisible(true);
                ps.setVideo((VideoObject) item);
            } else if (item instanceof SoundObject) {
                Label label = new Label(QuizData.getMediaName(item.getMediaPath()));
                label.setPrefHeight(42);
                label.setPrefWidth(120);
                label.setAlignment(Pos.CENTER);
                label.getStyleClass().add("movieDisplayLabel");
                label.setOnMouseClicked(event -> {
                    ac = ((SoundObject) item).getSound();
                    if (ac.isPlaying()) {
                        ac.stop();
                        label.setGraphic(null);
                    } else {
                        ac.play();
                        label.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/de/mafel/nawilauncher/res/Play.png"), 30, 30, true, true)));
                    }
                });
                mediaHbar.getChildren().add(label);
            }
        });
        //Timer updaten & starten
        if(activeQuestion.getTimeLimit() > 0) {
            timer.showTimer();
            timer.startOnClick(activeQuestion.getTimeLimit());
        } else {
            timer.hideTimer();
        }
        /*if (!ps.isVideoSet()) {
            //Timer starten
            if(activeQuestion.getTimeLimit() > 0) {
                timer.startOnClick(activeQuestion.getTimeLimit());
            } else {
                timer.hideTimer();
            }
        } else {
            ps.videoHasEndedProperty().addListener((o, v1, v2) -> {
                if (v2) {
                    if(activeQuestion.getTimeLimit() > 0) {
                        timer.startOnClick(activeQuestion.getTimeLimit());
                    } else {
                        timer.hideTimer();
                    }
                }
            });
            ps.openVideo();
        }*/
        
        GUtils.adjustFontSizeToBounds(lblQuestion);
    }
    
}
