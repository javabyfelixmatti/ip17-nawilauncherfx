package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.Main;
import static de.mafel.nawilauncher.Main.setPlickersClient;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.net.NaWiServer;
import de.mafel.nawilauncher.net.plickers.PlickersClient;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class PresentationSettingsSceneController implements Initializable, DynamicController {
    
    @FXML
    private Button btnBack, btnStartQuiz, btnAddTeam, btnEditTeam, btnRemoveTeam, btnStartServer;
    
    @FXML
    private Label lblIpHelp, lblIP, lblQuizTitle, lblQuizSubtitle, lblQuizGrade, lblQuizAuthor;
    
    @FXML
    private ListView<Team> teamList;
    
    @FXML
    private CheckBox cbEnablePlickers;
    
    @FXML
    private void btnStartAction(ActionEvent e) {
        //Überprüfe Teams
        if(TeamManager.getTeams().isEmpty()) {
            new ExceptionDialog(new IllegalStateException("Es muss mindestens ein Team bestehen."), "Bitte fügen Sie mindestens ein Team hinzu, um das Spiel starten zu können.\n\nHinweis: Untersuchungen haben ergeben, dass der Spielspaß mit mehr als einem Team maßgeblich steigt.");
            return;
        } else if(TeamManager.getTeams().size() == 1) {
            ButtonType continueBT = new ButtonType("Egal.", ButtonBar.ButtonData.OK_DONE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Hinweis: Untersuchungen haben ergeben, dass der Spielspaß mit mehr als einem Team maßgeblich steigt.", continueBT, ButtonType.CANCEL);
            if(alert.showAndWait().orElse(null) != continueBT) return;
        }
        //Überprüfe Angaben
        String warnings = "", corrections = "";
        if(!Main.getServer().getState()) {
            warnings += "\n\nDer Server wurde nicht gestartet. Das Spielerlebnis ist ohne den Webserver sehr eingeschränkt.";
            corrections += "\nDen Webserver starten.";
        }
        if(!warnings.isEmpty()) {
            ButtonType continueBT = new ButtonType("Fortfahren", ButtonBar.ButtonData.OK_DONE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Folgende Probleme wurden erkannt:" + warnings + "\n\nWenn Sie auf 'Fortfahren' klicken, wird Folgendes automatisch durchgeführt:" + corrections, continueBT, ButtonType.CANCEL);
            if(alert.showAndWait().orElse(null) != continueBT) return;
            if(!Main.getServer().getState()) btnStartServerAction(null);
        }
        //Plickers
        if(cbEnablePlickers.isSelected()) {
            if(Main.getPlickersClient() == null || Main.getPlickersClient().isInactive()) {
                try {
                    PlickersClient pc = new PlickersClient();
                    setPlickersClient(pc);
                } catch (Exception ex) {}
            }
            //Alle WebClients, die als Einzelspieler angemeldet sind, rausschmeißen
            Main.getServer().getWebSocketServer().kickIndividualClients();
            try {
                Stage plickersStage = new Stage();
                plickersStage.initModality(Modality.APPLICATION_MODAL);
                plickersStage.initOwner(Main.getStage());
                plickersStage.setTitle("Plickers Verwaltung");
                plickersStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("/de/mafel/nawilauncher/scenes/plickers/PlickersControlScene.fxml"))));
                plickersStage.showAndWait();
            } catch (IOException ex) {
                LogHelper.error(ex);
            }
            //Überprüfe, ob allen Teams eine Plickers Klasse zugeordnet ist
            if(TeamManager.getTeams().parallelStream().anyMatch(t -> t.getPlickersClass() == null)) return;
        }
        //Quiz starten
        try {
            Scenes.unloadScene("scenes/qtypes/ABCDQuestionScene.fxml");
            Scenes.unloadScene("scenes/qtypes/TextQuestionScene.fxml");
            Scenes.unloadScene("scenes/qtypes/EstimateQuestionScene.fxml");
            Scenes.unloadScene("scenes/qtypes/SequenceQuestionScene.fxml");
            Scenes.unloadScene("scenes/CategoryScene.fxml");
            Navigator.resetQuestionsDone();
            TeamManager.forEachTeam(t -> t.setScore(0));
            TeamManager.setActiveForAllTeam(0);
            TeamManager.setActiveTeam(0);
            TeamManager.setRound(1);
            if(QuizData.getCategories().isEmpty()) throw new Exception("Es muss mindestens eine Kategorie angegeben sein. Um eine Kategorie hinzuzufügen, klicken Sie im Hauptmenü auf 'Quiz bearbeiten'.");
            if(QuizData.getScores().isEmpty()) throw new Exception("Es muss mindestens eine Punktzahl angegeben sein. Um eine Punktzahl hinzuzufügen, klicken Sie im Hauptmenü auf 'Quiz bearbeiten'.");
            if (QuizData.isShowRules()) Scenes.changeScene("scenes/RulesScene.fxml"); 
            else Scenes.changeScene("scenes/CategoryScene.fxml", true);
        } catch(Exception ex) {
            new ExceptionDialog(ex, ex.getMessage());
        }
    }  
    
    @FXML
    private void btnStartServerAction(ActionEvent e) {
        try {
            Main.getServer().start();
            lblIpHelp.setText("Bitte gib dies in den Browser ein:");
            lblIP.setText(Main.getServer().getIP());
        } catch(IOException ex) {
            new ExceptionDialog(ex, "Der Server konnte nicht gestartet werden. Das Quiz wird ohne Webunterstützung ausgeführt.");
        } catch(IllegalStateException ex) {
            new ExceptionDialog(ex, "Der Server läuft bereits!");
        }
    }
    
    @FXML
    private void btnBackAction(ActionEvent e) {
        Scenes.changeScene("scenes/MainScene.fxml");
    }
    
    @FXML
    private void btnAddTeamAction(ActionEvent e) {
        Dialog<Pair<String, String>> dialog = new Dialog();
        dialog.setTitle("Team hinzufügen");
        ButtonType type = new ButtonType("Hinzufügen", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(type, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0,10,0,10));

        TextField name = new TextField();
        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.BLUE);

        grid.add(new Label("Name:"), 0, 0);
        grid.add(name, 1, 0);
        grid.add(new Label("Farbe:"), 0, 1);
        grid.add(colorPicker, 1, 1);

        dialog.getDialogPane().setContent(grid);
        Platform.runLater(() -> name.requestFocus());

        dialog.setResultConverter(dialogBtn -> {
            if (dialogBtn == type) {
                return new Pair<>(name.getText(), colorPicker.getValue().toString());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();
        result.ifPresent(pair -> {
            if(TeamManager.getTeams().parallelStream().filter(te -> te.getName().equalsIgnoreCase(pair.getKey())).count() == 0)
                TeamManager.addTeam(new Team(pair.getKey(), pair.getValue()));
            else new ExceptionDialog(new IllegalArgumentException("Ein Team mit diesem Namen existiert bereits!"), "Ein Team mit diesem Namen existiert bereits!");
        }); 
    }
    
    @FXML
    private void btnEditTeamAction(ActionEvent e) {
        try {
            Dialog<Pair<String, String>> dialog = new Dialog();
            dialog.setTitle("Team bearbeiten");
            ButtonType type = new ButtonType("ändern", ButtonBar.ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().addAll(type, ButtonType.CANCEL);

            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(0,10,0,10));

            TextField name = new TextField(TeamManager.getTeam(teamList.getSelectionModel().getSelectedIndex()).getName());
            final ColorPicker colorPicker = new ColorPicker(TeamManager.getTeam(teamList.getSelectionModel().getSelectedIndex()).getColor());

            grid.add(new Label("Name:"), 0, 0);
            grid.add(name, 1, 0);
            grid.add(new Label("Farbe:"), 0, 1);
            grid.add(colorPicker, 1, 1);

            dialog.getDialogPane().setContent(grid);
            Platform.runLater(() -> name.requestFocus());

            dialog.setResultConverter(dialogBtn -> {
                if (dialogBtn == type) {
                    return new Pair<>(name.getText(), colorPicker.getValue().toString());
                }
                return null;
            });

            Optional<Pair<String, String>> result = dialog.showAndWait();
            result.ifPresent(pair -> {
                TeamManager.editTeam(new Team(pair.getKey(), pair.getValue()), teamList.getSelectionModel().getSelectedIndex());

            });
        } catch (ArrayIndexOutOfBoundsException ex) {
            //ignore
        }
    } 
    
    @FXML
    private void btnRemoveTeamAction(ActionEvent e) {
        try {
            int i = teamList.getSelectionModel().getSelectedIndex();
            TeamManager.removeTeam(teamList.getSelectionModel().getSelectedItem());
        } catch(ArrayIndexOutOfBoundsException ex) {
            //ignore
        }
    }
    
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        teamList.setItems(TeamManager.getTeams());
        teamList.setCellFactory(param -> new ListCell<Team>() {
            @Override
            protected void updateItem(Team item, boolean empty) {
                super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                if (empty || item == null || item.getColor() == null) {
                    setText(null);
                } else {
                    setBackground(new Background(new BackgroundFill(item.getColor(), CornerRadii.EMPTY, Insets.EMPTY)));
                    setTextFill(GUtils.getTextColor(item.getColor()));
                    Team sel = teamList.getSelectionModel().getSelectedItem();
                    setText((sel != null && sel.equals(item) ? "> " : "") + item.getName());
                }
            }
            
        });
        btnStartServer.disableProperty().bind(Main.getServer().stateProperty());
        cbEnablePlickers.selectedProperty().bindBidirectional(Navigator.plickersEnabledProperty());
        
        teamList.setOnMouseClicked(event -> {
            teamList.refresh();
        });
        
        GUtils.bindFontSizeToBounds(lblQuizTitle);
        GUtils.bindFontSizeToBounds(lblQuizSubtitle);
    }
    
    @Override
    public void onSceneShown() {
        Navigator.setQuizState(QuizState.SETTINGS);
        lblQuizTitle.setText(QuizData.getQuizTitle());
        lblQuizSubtitle.setText(QuizData.getQuizSubtitle());
        lblQuizAuthor.setText(QuizData.getQuizAuthor());
        if ("".equals(QuizData.getGrade())) {
            lblQuizGrade.setOpacity(0);
        } else {
            lblQuizGrade.setText("Klasse: " + QuizData.getGrade());
            lblQuizGrade.setOpacity(1);
        }
        if (Main.getServer().getState() == true) {
            lblIpHelp.setText("Bitte gib dies in den Browser ein:");
            lblIP.setText(Main.getServer().getIP());
        }
        
        GUtils.adjustFontSizeToBounds(lblQuizTitle);
        GUtils.adjustFontSizeToBounds(lblQuizSubtitle);
    }
    
}
