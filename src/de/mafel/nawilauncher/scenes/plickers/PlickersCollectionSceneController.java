package de.mafel.nawilauncher.scenes.plickers;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.TeamResult;
import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.net.plickers.PlickersAddToQueueRequest;
import de.mafel.nawilauncher.scenes.Scenes;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Felix
 */
public class PlickersCollectionSceneController implements Initializable, DynamicController {
    
    @FXML
    private Label lblCatScore, lblContinue;
    
    @FXML
    private StackedBarChart<String, Integer> chart;
    
    @FXML
    private CategoryAxis xAxis;
    
    private XYChart.Series<String, Integer> series = new XYChart.Series<>(), antiseries = new XYChart.Series<>();
    private HashMap<Team, XYChart.Data<String, Integer>> values = new HashMap<>(), antivalues = new HashMap<>();
    private HashMap<Team, Boolean> complete = new HashMap<>();
    
    private boolean threadActive = true;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        xAxis.setCategories(FXCollections.observableArrayList(TeamManager.getTeams().parallelStream().map(t -> t.getName()).toArray(String[]::new)));
        TeamManager.forEachTeam(t -> {
            values.put(t, new XYChart.Data<>(t.getName(), 0));
            antivalues.put(t, new XYChart.Data<>(t.getName(), t.getPlickersClass().getStudentCount()));
            complete.put(t, Boolean.FALSE);
        });
        series.getData().addAll(values.values());
        antiseries.getData().addAll(antivalues.values());
        chart.getData().addAll(series, antiseries);
    }

    @Override
    public void onSceneShown() {
        TeamManager.forEachTeam(t -> {
            values.get(t).setYValue(0);
            antivalues.get(t).setYValue(t.getPlickersClass().getStudentCount());
            complete.put(t, Boolean.FALSE);
        });
        lblCatScore.setText(Navigator.getActiveQuestion().getCategory().getId());
        TeamManager.getTeams().stream().forEach(t -> {
            PlickersAddToQueueRequest request = new PlickersAddToQueueRequest(Navigator.getPlickersQuestionId(), t.getPlickersClass().getId());
            try {
                t.setPlickersPollId(Main.getPlickersClient().queryAddToQueue(request).getId());
            } catch (Exception ex) {
                LogHelper.error(ex);
            }
        });
        threadActive = true;
        new Thread(new Task<Void>() {
            
            @Override
            protected Void call() throws Exception {
                do {
                    TeamManager.getTeams().stream().forEach(t -> {
                        try {
                            int count = Main.getPlickersClient().queryPoll(t.getPlickersPollId()).getAnswerCount();
                            int students = t.getPlickersClass().getStudentCount();
                            if(count >= students) complete.put(t, Boolean.TRUE);
                            values.get(t).setYValue(count);
                            antivalues.get(t).setYValue(students - count);
                        } catch (Exception ex) {
                            LogHelper.error(ex);
                        }
                    });
                    Thread.sleep(5000);
                } while(threadActive && !complete.values().parallelStream().filter(b -> !b).findAny().orElse(Boolean.TRUE));
                return null;
            }
            
        }).start();
    }
    
    @FXML
    private void handleLblContinueClicked(MouseEvent e) {
        //Punkte der einzelnen Teams ausrechnen
        if(!complete.values().parallelStream().filter(b -> !b).findAny().orElse(Boolean.TRUE)) {
            Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
            infoAlert.initOwner(Main.getStage());
            infoAlert.setHeaderText("Scan beenden?");
            infoAlert.setTitle("Info");
            infoAlert.setContentText("Die Scans wurden noch nicht beendet. Sind Sie sicher, dass Sie fortfahren möchten, bevor alle Scans abgeschlossen wurden?");
            
            ButtonType btOk = new ButtonType("Ok");
            ButtonType btCancel = new ButtonType("Abbrechen");
                    
            infoAlert.getButtonTypes().clear();
            infoAlert.getButtonTypes().addAll(btOk, btCancel);
            
            Optional<ButtonType> result = infoAlert.showAndWait();
            if (result.get() != btOk) return;
        }
        threadActive = false;
        //Ergebnisse sammeln
        final int correct = ((ABCDQuestion) Navigator.getActiveQuestion()).getCorrectAnswer();
        final HashMap<Team, Double> correctness = new HashMap<>();
        TeamManager.getTeams().stream().forEach(t -> {
            try {
                int count = Main.getPlickersClient().queryPoll(t.getPlickersPollId()).getAnswerCounts()[correct];
                int students = t.getPlickersClass().getStudentCount();
                correctness.put(t, count / (double) students);
            } catch (Exception ex) {
                LogHelper.error(ex);
            }
        });
        //Beste(s) Team(s) ermitteln
        double max = 0; final ArrayList<Team> maxt = new ArrayList<>();
        for(Team t : correctness.keySet()) {
            if(correctness.get(t) > max) {
                maxt.clear();
                maxt.add(t);
                max = correctness.get(t);
            } else if(correctness.get(t) == max) maxt.add(t);
        }
        //Ergebnisse bestimmen
        final int score = Navigator.getActiveQuestion().getScore();
        TeamManager.getTeams().stream().forEach(t -> {
            Navigator.setResult(t, new TeamResult(Math.round(100 * correctness.get(t)) + "% richtig", maxt.contains(t) ? score : 0, false));
        });
        TeamManager.resetAnswers();
        TeamManager.setNextActiveTeam();
        //Scene Change
        Scenes.changeScene("scenes/ExplanationScene.fxml");
    }
    
}
