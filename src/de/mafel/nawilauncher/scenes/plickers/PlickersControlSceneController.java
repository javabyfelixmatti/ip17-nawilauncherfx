package de.mafel.nawilauncher.scenes.plickers;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.net.plickers.PlickersClass;
import de.mafel.nawilauncher.net.plickers.PlickersClient;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.scenes.components.PlickersLoginDialog;
import java.net.URL;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.stage.Stage;
import javafx.util.Pair;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Felix
 */
public class PlickersControlSceneController implements Initializable {
    
    @FXML
    private Label lblStatus;
    
    @FXML
    private Button btnLogin;
    
    @FXML
    private TableView<Team> tableView;
    
    @FXML
    private TableColumn<Team, String> tcTeams;
    
    @FXML
    private TableColumn<Team, PlickersClass> tcPlickersClass;
    
    private ObservableList<PlickersClass> availableClasses = FXCollections.observableArrayList();
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        updateStatus();
        tableView.setItems(TeamManager.getTeams());
        tcTeams.setCellValueFactory(cdf -> cdf.getValue().nameProperty());
        tcPlickersClass.setCellValueFactory(cdf -> cdf.getValue().plickersClassProperty());
        tcPlickersClass.setCellFactory(ChoiceBoxTableCell.forTableColumn(new StringConverter<PlickersClass>() {
            @Override
            public String toString(PlickersClass object) {
                return object == null ? "Keine" : object.getName();
            }

            @Override
            public PlickersClass fromString(String string) {
                return null;
            }
        }, availableClasses));
        tableView.getSelectionModel().select(0);
    }
    
    private void updateStatus() {
        Platform.runLater(() -> {
            lblStatus.setText("Verbinde...");
            tableView.setDisable(true);
        });
        new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                PlickersClient pc = Main.getPlickersClient();
                if(pc == null || pc.isInactive()) {
                    Platform.runLater(() -> {
                        lblStatus.setText("Nicht eingeloggt");
                        btnLogin.setText("Einloggen");
                    });
                    return null;
                }
                try {
                    final String fullName = pc.queryUser().getFullName();
                    final PlickersClass[] classes = pc.querySections().getClasses();
                    availableClasses.clear();
                    availableClasses.addAll(classes);
                    Platform.runLater(() -> {
                        lblStatus.setText("Eingeloggt als " + fullName);
                        tableView.setDisable(false);
                        btnLogin.setText("Ausloggen");
                    });
                } catch(Exception ex) {
                    Platform.runLater(() -> lblStatus.setText("Fehler beim Authentifizieren"));
                    new ExceptionDialog(ex, "Fehler beim Authentifizieren: " + ex.getMessage());
                }
                return null;
            }
        }.run();
    }
    
    @FXML
    private void btnLoginAction(ActionEvent e) {
        PlickersClient pc = Main.getPlickersClient();
        if(pc == null || pc.isInactive()) {
            PlickersLoginDialog dialog = new PlickersLoginDialog();
            try {
                final Pair<String, String> loginCredentials = dialog.showAndWait().get();
                lblStatus.setText("Verbinde...");
                new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        try {
                            PlickersClient pc = new PlickersClient(loginCredentials.getKey(), loginCredentials.getValue());
                            Main.setPlickersClient(pc);
                        } catch (PlickersClient.PlickersCommunicationException ex) {
                            new ExceptionDialog(ex, "Fehler beim Login: " + ex.getMessage());
                        }
                        updateStatus();
                        return null;
                    }
                }.run();
            } catch (NoSuchElementException ex) {

            }
        } else {
            lblStatus.setText("Verbinde...");
            new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    try {
                        pc.logout();
                    } catch (PlickersClient.PlickersCommunicationException | IllegalStateException ex) {
                        new ExceptionDialog(ex, "Fehler beim Logout: " + ex.getMessage());
                    }
                    updateStatus();
                    return null;
                }
            }.run();
        }
    }
    
    @FXML
    private void btnDoneAction(ActionEvent e) {
        //Überprüfe, ob allen Teams eine Plickers Klasse zugeordnet ist
        Team teamWithoutClass = TeamManager.getTeams().parallelStream().filter(t -> t.getPlickersClass() == null).findAny().orElse(null);
        if(teamWithoutClass != null) {
            new Alert(Alert.AlertType.ERROR, "Es muss jedem Team eine Plickers Klasse zugeordnet sein.\n\nFolgendem Team ist keine Klasse zugewiesen: " + teamWithoutClass.getName(), ButtonType.OK).showAndWait();
            return;
        }
        if(TeamManager.getTeams().parallelStream().map(t -> t.getPlickersClass()).distinct().count() != TeamManager.getTeams().size()) {
            new Alert(Alert.AlertType.ERROR, "Jede Plickers Klasse darf nur genau einem Team zugewiesen werden.", ButtonType.OK).showAndWait();
            return;
        }
        ((Stage) btnLogin.getScene().getWindow()).close();
    }
    
    @FXML
    private void btnCancelAction(ActionEvent e) {
        TeamManager.getTeams().parallelStream().forEach(t -> t.setPlickersClass(null));
        ((Stage) btnLogin.getScene().getWindow()).close();
    }
    
}
