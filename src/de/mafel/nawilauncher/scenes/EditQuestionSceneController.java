package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.data.questions.EstimateQuestion;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.questions.SequenceQuestion;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import de.mafel.nawilauncher.database.Database;
import de.mafel.nawilauncher.scenes.database.QuestionDatabaseSceneController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class EditQuestionSceneController implements Initializable {

    @FXML
    TabPane tpQuestion;
    
    @FXML
    ComboBox cbType;
    
    ObservableList<String> types = FXCollections.observableArrayList("ABCD-Frage", "Text-Frage", "Schätz-Frage", "Reihenfolge-Frage");
    
    
    @FXML
    private void handleBtnSaveAction() {
        ((Stage) tpQuestion.getScene().getWindow()).close();
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbType.setItems(types);
        cbType.getSelectionModel().selectedIndexProperty().addListener((o, v1, v2) -> {
                questionTypeChanged();
        });
        Question selectedQuestion = QuestionDatabaseSceneController.getSelectedQuestion();
        if (selectedQuestion != null) {
            if (selectedQuestion instanceof ABCDQuestion) cbType.getSelectionModel().select(0);
            if (selectedQuestion instanceof TextQuestion) cbType.getSelectionModel().select(1);
            if (selectedQuestion instanceof EstimateQuestion) cbType.getSelectionModel().select(2);
            if (selectedQuestion instanceof SequenceQuestion) cbType.getSelectionModel().select(3);
            tpQuestion.getTabs().clear();
            tpQuestion.getTabs().addAll(selectedQuestion.getEditQuestionTabs());
        }
    }    
    
    
    private void questionTypeChanged() {
        Question selectedQuestion = QuestionDatabaseSceneController.getSelectedQuestion();
        int index = Database.getQuestions().indexOf(selectedQuestion);
        switch(cbType.getSelectionModel().getSelectedIndex()) {
            case 0:
                if (!(selectedQuestion instanceof ABCDQuestion)) {
                    selectedQuestion = ABCDQuestion.convert(selectedQuestion);
                    Database.getQuestions().set(index, selectedQuestion);
                }
                break;
            case 1:
                if (!(selectedQuestion instanceof TextQuestion)) {
                    selectedQuestion = TextQuestion.convert(selectedQuestion);
                    Database.getQuestions().set(index, selectedQuestion);
                }
                break;
            case 2:
                if (!(selectedQuestion instanceof EstimateQuestion)) {
                    selectedQuestion = EstimateQuestion.convert(selectedQuestion);
                    Database.getQuestions().set(index, selectedQuestion);
                }
                break;
            case 3:
                if (!(selectedQuestion instanceof SequenceQuestion)) {
                    selectedQuestion = SequenceQuestion.convert(selectedQuestion);
                    Database.getQuestions().set(index, selectedQuestion);
                }
                break;
            default:
                return;
        }
        
        tpQuestion.getTabs().clear();
        tpQuestion.getTabs().addAll(selectedQuestion.getEditQuestionTabs());
    }
    
    
    
}
