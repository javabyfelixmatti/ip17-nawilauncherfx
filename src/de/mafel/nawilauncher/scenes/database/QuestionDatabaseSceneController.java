package de.mafel.nawilauncher.scenes.database;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.data.questions.EstimateQuestion;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.questions.SequenceQuestion;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import de.mafel.nawilauncher.database.Database;
import de.mafel.nawilauncher.fileio.NWDFormat;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class QuestionDatabaseSceneController implements Initializable, DynamicController {

    @FXML
    TableView<Question> tvData;
    
    @FXML
    Button btnEditQuestion;
    
    @FXML
    TableColumn<Question, String> tcQuestion, tcQuestionType;
    
    @FXML
    Label lblOpenedData;
    
    public static Question selectedQuestion = null;
    
    
    @FXML
    private void handleBtnAddQuestionAction(ActionEvent e) {
        ABCDQuestion q = ABCDQuestion.placeholder();
        Database.getQuestions().add(q);
        tvData.getSelectionModel().select(q);
        selectedQuestion = q;
        Parent root;
        try {
            root = FXMLLoader.load(Main.class.getResource("scenes/EditQuestionScene.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Frage hinzufügen");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.getIcons().add(new Image(Main.class.getResourceAsStream("res/favicon.png")));
            stage.setScene(new Scene(root));
            stage.showAndWait();
            tvData.refresh();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        tvData.getSelectionModel().select(q);
    }
    
    @FXML
    private void handleBtnEditQuestionAction(ActionEvent e) {
        Parent root;
        selectedQuestion = tvData.getSelectionModel().getSelectedItem();
        try {
            root = FXMLLoader.load(Main.class.getResource("scenes/EditQuestionScene.fxml"));
            Stage stage = new Stage();
            
            stage.setTitle("Frage hinzufügen");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.getIcons().add(new Image(Main.class.getResourceAsStream("res/favicon.png")));
            stage.setScene(new Scene(root));
            stage.showAndWait();
            tvData.refresh();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @FXML
    private void handleBtnDeleteQuestionAction(ActionEvent e) {
        Question q = tvData.getSelectionModel().getSelectedItem();
        Database.getQuestions().remove(q);
    }
    
    @FXML
    private void handleBtnImportAction(ActionEvent e) {
        
    }
    
    @FXML
    private void handleBtnExportAction(ActionEvent e) {
        ArrayList<String> questionList = new ArrayList<>();
        QuizData.getQuestions().forEach((k, v) -> {
            String questionShort;
            if (v.getQuestion().length() > 40) {
                questionShort = v.getQuestion().substring(0, 40) + "...";
            } else if (v.getQuestion().length() > 20) { 
                questionShort = v.getQuestion().substring(0, 20) + "...";
            } else if (v.getQuestion().length() > 6) { 
                questionShort = v.getQuestion().substring(0, 6) + "...";
            } else {
                questionShort = "...";
            }
            questionList.add(k.getCategory().toString() + " [" + k.getScore() + "]: " + questionShort);
        });
        Collections.sort(questionList);

        ChoiceDialog<String> dialog = new ChoiceDialog<>(questionList.get(0), questionList);
        dialog.setTitle("Frage exportieren");
        dialog.setHeaderText("Achtung! Wollen Sie die ausgewählte Frage exportieren?");
        dialog.setContentText("Dabei wird die im Fragenbearbeitungsfenster ausgewählte Frage unwiederbringlich überschrieben!");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            
            QuizData.getCategories().forEach(cat -> {
                String catString = result.get().split("\\[")[0].substring(0, result.get().split("\\[")[0].length()-1);
                System.out.println("CategoryString: " + catString);
                if (cat.toString().equals(catString)) {
                    Category category = cat;
                    
                    
                    QuizData.getScores().forEach(sc -> {
                        Integer scTmp = Integer.parseInt(result.get().split("\\[")[1].split("\\]")[0]);
                         System.out.println("ScoreString: " + scTmp);
                        if (sc == scTmp) {
                            System.out.println("FOUND!!!");
                            Integer score = sc;
                            Question q = tvData.getSelectionModel().getSelectedItem();
                            QuizData.getQuestions().remove(new QuestionIdentifier(category, score));
                            QuizData.getQuestions().put(new QuestionIdentifier(category, score), q);
                        }
                    });               
                    
                }
            });
            
        }

    }
    
    
    
    
    @FXML
    private void handleBtnSaveDatabaseAction(ActionEvent e) {
        File f = GUtils.getDatabaseFileChooser("Datenbank speichern").showSaveDialog(Main.getStage().getOwner());
        if(f != null) {
            NWDFormat.saveFile(f);
        }
    }
    
    @FXML
    private void handleBtnOpenDatabaseAction(ActionEvent e) {
        File f = GUtils.getDatabaseFileChooser("Datenbank öffnen").showOpenDialog(Main.getStage().getOwner());
        if(f != null) {
            NWDFormat.openFile(f);
        }
    }

    public static Question getSelectedQuestion() {
        return selectedQuestion;
    }

    public static void setSelectedQuestion(Question selectedQuestion) {
        QuestionDatabaseSceneController.selectedQuestion = selectedQuestion;
    }
    
    
    
    
    
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnEditQuestion.setDisable(true);
        tvData.getSelectionModel().selectedIndexProperty().addListener((o, v1, v2) -> {
            btnEditQuestion.setDisable(v2 == null);
            selectedQuestion = tvData.getSelectionModel().getSelectedItem();
        });
        
        Database.getQuestions().addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change c) {
                tvData.getItems().clear();
                tvData.getItems().addAll(Database.getQuestions());
            }
        });
        
        tvData.setFixedCellSize(100.0);
        tvData.setPlaceholder(new Label("Bitte öffnen Sie eine Datenbank oder geben Sie eine Frage ein."));
        
        tcQuestion.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().getQuestion()));
        tcQuestion.setCellFactory((TableColumn<Question, String> param) -> new TableCell<Question, String>() {
            private Text text;
            
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setTextOverrun(OverrunStyle.ELLIPSIS);
                if (item != null && !empty) {
                    text = new Text(item);
                    text.setWrappingWidth(tcQuestion.getWidth()-5);
                    text.autosize();
                    this.setWrapText(true);
                    setGraphic(text);
                } else {
                    setGraphic(null);
                }
            }
        });
        
        tcQuestionType.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(getQuestionType(param.getValue())));
        tcQuestionType.setCellFactory((TableColumn<Question, String> param) -> new TableCell<Question, String>() {
            private Text text;
            
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    text = new Text(item);
                    text.setWrappingWidth(tcQuestionType.getWidth()-5);
                    this.setWrapText(true);
                    setGraphic(text);
                } else {
                    setGraphic(null);
                }
            }
        });
        tvData.getItems().clear();
        tvData.getItems().addAll(Database.getQuestions());
        tvData.refresh();
        
    }    

    @Override
    public void onSceneShown() {
        tvData.getItems().clear();
        tvData.getItems().addAll(Database.getQuestions());
        tvData.refresh();
    }
    
    private String getQuestionType(Question q) {
       if (q instanceof ABCDQuestion) return "ABCD-Frage";
       if (q instanceof TextQuestion) return "Textfrage";       
       if (q instanceof EstimateQuestion) return "Schätzfrage";
       if (q instanceof SequenceQuestion) return "Reihenfolge-Frage";
       return "";
    }
    
}
