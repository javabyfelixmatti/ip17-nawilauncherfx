package de.mafel.nawilauncher.scenes.database;

import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class ImportDatabaseSceneController implements Initializable, DynamicController {

    @FXML
    ListView lvQuestions;
    
    @FXML
    Button btnAll, btnNothing;
            
    ObservableList<Question> selectedQuestions;
    
    /**
     * Selektiert alle Fragen.
     * @param e
     */
    @FXML
    public void handleBtnAll(ActionEvent e) {
        
    }
    
    /**
     * Deselektiert alle Fragen.
     * @param e
     */
    @FXML
    public void handleBtnNothing(ActionEvent e) {
        
    }
    
    
    
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //FIXME
        /*lvQuestions.setCellFactory(CheckBoxListCell.forListView((Question q) -> {
        BooleanProperty observable = new SimpleBooleanProperty();
        observable.addListener((obs, wasSelected, isNowSelected) ->
        
        );
        return observable ;
        }));*/
    }    

    @Override
    public void onSceneShown() {
        
    }
   
}
