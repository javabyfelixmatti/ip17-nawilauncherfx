package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.fileio.NWQFormat;
import de.mafel.nawilauncher.input.GlobalKeyHandler;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 *
 * @author Felix
 */
public class MainSceneController implements Initializable, DynamicController {
    
    @FXML
    private MenuItem btnSaveQuiz;
    
    
    @FXML
    private MenuButton mainMenuButton;
    
    @FXML
    private Button btnStartQuiz, btnAddQuiz, btnShowResults, btnShowHelp;
    
    @FXML
    private Label lblOpenedQuiz, lblVersion;
    
    @FXML
    private void handleBtnStartQuizAction(ActionEvent e) {
        LogHelper.info("Starte Quiz geklickt");
        Scenes.changeScene("scenes/PresentationSettingsScene.fxml");
    }
    
    @FXML
    private void handleBtnEditQuizAction(ActionEvent e) {
        Scenes.changeScene("scenes/EditQuizScene.fxml");
    }
    
    @FXML
    private void handleBtnShowHelpAction(ActionEvent e) {
        Scenes.changeScene("scenes/HelpScene.fxml");
    }
    
    @FXML
    private void handleBtnAddQuizAction() {
        LogHelper.info("Quiz hinzufügen geklickt");
        Scenes.changeScene("scenes/EditQuizScene.fxml");
    }
    
    @FXML
    private void handleBtnExitAction(ActionEvent e) {
        LogHelper.info("Beende...");
        System.exit(0);
    }
    
    @FXML
    private void handleBtnOpenAction() {
        File f = GUtils.getFileChooser("Quiz öffnen").showOpenDialog(Main.getMainScene().getWindow());
        if(f != null) {
            NWQFormat.openFile(f, lblOpenedQuiz);
            btnSaveQuiz.setDisable(false);
            btnStartQuiz.setDisable(false);
        }
    }
    
    @FXML
    private void handleBtnSaveAction() {
        try {
            NWQFormat.saveAndOverwrite(lblOpenedQuiz);
        } catch(IllegalStateException ex) {
            new ExceptionDialog(ex, ex.getMessage());
        }
    }
    
    @FXML
    private void handleBtnSaveAsAction() {
        File f = GUtils.getFileChooser("Quiz speichern").showSaveDialog(Main.getMainScene().getWindow());
        if(f != null) {
            NWQFormat.saveFile(f, lblOpenedQuiz);
        }
    }
    
    @FXML
    private void handleBtnOpenDatabaseAction() {
        LogHelper.info("Öffne Datenbank...");
        Parent root;
        try {
            root = FXMLLoader.load(Main.class.getResource("scenes/database/QuestionDatabaseScene.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Fragen-Datenbank");
            stage.getIcons().add(new Image(Main.class.getResourceAsStream("res/favicon.png")));
            stage.setScene(new Scene(root));
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void handleKeyEvent(KeyEvent e) {
        if(GlobalKeyHandler.CTRL_O.match(e)) lblOpenedQuiz.setText(QuizData.getQuizTitle() + " geöffnet.");
        else if(GlobalKeyHandler.CTRL_S.match(e)) lblOpenedQuiz.setText(QuizData.getQuizTitle() + " gespeichert.");
        btnSaveQuiz.setDisable(false);
        btnStartQuiz.setDisable(false);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Da dies die Startszene ist, wird onSceneShown beim ersten Zeigen nicht ausgeführt
        onSceneShown();
        Main.getKeyHandler().addPostEventHandler(this::handleKeyEvent);
        lblVersion.setText("Build " + Main.getBuildNumber().toString());
    }
    
    
    
    @Override
    public void onSceneShown() {
        Navigator.setQuizState(QuizState.NOT_STARTED);
        //Überprüfe, ob ein Quiz (mit Inhalt) geladen ist
        boolean contents = !QuizData.getQuestions().isEmpty();
        btnSaveQuiz.setDisable(!contents);
        btnStartQuiz.setDisable(!contents);
        if (contents) lblOpenedQuiz.setText(QuizData.getQuizTitle() + " geöffnet.");
    }
    
}
