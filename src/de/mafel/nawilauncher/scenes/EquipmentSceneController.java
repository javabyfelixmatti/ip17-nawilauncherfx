/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class EquipmentSceneController implements Initializable, DynamicController {

    @FXML
    Label lblContinue, lblCatScore, lblBack, lblEquipment;
    
    
    @FXML
    private void handleLblContinueClicked(Event e) {
        Scenes.changeScene(Navigator.getActiveQuestion().getSceneFile());
    }
    
    @FXML
    private void handleLblBackClicked(Event e) {
        Scenes.changeScene("scenes/CategoryScene.fxml");
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @Override
    public void onSceneShown() {
        lblEquipment.setText(Navigator.getActiveQuestion().getEquipment());
        lblCatScore.setText(Navigator.getActiveQuestion().getCategory().getId() + " " + Navigator.getActiveQuestion().getScore());
    }
    
}
