package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.scenes.components.HelpEntry;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class HelpSceneController implements Initializable {

    @FXML
    WebView browser;
    
    @FXML
    TreeView<HelpEntry> treeTopics;
    
    @FXML
    Button btnBack;
    
    @FXML
    private void handleBtnBackAction(ActionEvent e) {
        Scenes.changeScene("scenes/MainScene.fxml");
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        final WebEngine eng = browser.getEngine();
        
        TreeItem<HelpEntry> tiRoot = new TreeItem<>(new HelpEntry("Hilfe", null));
        tiRoot.setExpanded(true);
        treeTopics.setRoot(tiRoot);
        ObservableList<TreeItem<HelpEntry>> list = treeTopics.getRoot().getChildren();
        
        TreeItem<HelpEntry> tiIndex = new TreeItem<>(new HelpEntry("Einführung", "index.html"));
        TreeItem<HelpEntry> tiCreateQuiz = new TreeItem<>(new HelpEntry("Quiz erstellen", "createQuiz.html"));
            TreeItem<HelpEntry> tiQuizTypes = new TreeItem<>(new HelpEntry("Quiztypen", null)); 
                TreeItem<HelpEntry> tiNaWiGator = new TreeItem<>(new HelpEntry("NaWiGator", "qtypesNaWiGator.html"));
                TreeItem<HelpEntry> tiSimple = new TreeItem<>(new HelpEntry("Einfach", "qtypesSimple.html"));
            tiQuizTypes.getChildren().addAll(tiNaWiGator, tiSimple);
            TreeItem<HelpEntry> tiMeta = new TreeItem<>(new HelpEntry("Metadaten", "meta.html"));   
            TreeItem<HelpEntry> tiQuizSettings = new TreeItem<>(new HelpEntry("Quizeinstellungen", "quizSettings.html"));
            TreeItem<HelpEntry> tiCatAndSc = new TreeItem<>(new HelpEntry("Kategorien und Punkte", "categoriesAndScores.html"));
            TreeItem<HelpEntry> tiQ = new TreeItem<>(new HelpEntry("Fragen", "question.html"));
            TreeItem<HelpEntry> tiMediaManager = new TreeItem<>(new HelpEntry("Media Manager", "mediaManager.html"));
        tiCreateQuiz.getChildren().addAll(tiQuizTypes, tiMeta, tiQuizSettings, tiCatAndSc, tiQ, tiMediaManager);
        TreeItem<HelpEntry> tiPresentateQuiz = new TreeItem<>(new HelpEntry("Quiz präsentieren", "presentateQuiz.html"));
            TreeItem<HelpEntry> tiPresentationSettings = new TreeItem<>(new HelpEntry("Präsentationseinstellungen", "presentationsettings.html"));
            TreeItem<HelpEntry> tiRules = new TreeItem<>(new HelpEntry("Regeln", "rules.html"));
            TreeItem<HelpEntry> tiChooseQuestion = new TreeItem<>(new HelpEntry("Fragen auswählen", "chooseQuestion.html"));
            TreeItem<HelpEntry> tiABCDQ = new TreeItem<>(new HelpEntry("ABCD-Frage", "ABCDQ.html"));
            TreeItem<HelpEntry> tiTextQ = new TreeItem<>(new HelpEntry("Text-Frage", "TextQ.html"));
            TreeItem<HelpEntry> tiEstimateQ = new TreeItem<>(new HelpEntry("Schätz-Frage", "EstimateQ.html"));
            TreeItem<HelpEntry> tiSequenceQ = new TreeItem<>(new HelpEntry("Reihenfolge-Frage", "SequenceQ.html"));
            TreeItem<HelpEntry> tiEvaluation = new TreeItem<>(new HelpEntry("Auswertung", "Evaluation.html"));
        tiPresentateQuiz.getChildren().addAll(tiPresentationSettings, tiRules, tiChooseQuestion, tiABCDQ, tiTextQ, tiEstimateQ, tiSequenceQ, tiEvaluation);
        TreeItem<HelpEntry> tiGetHelp = new TreeItem<>(new HelpEntry("Hilfe erhalten", "GetHelp.html"));
        
        list.addAll(tiIndex, tiCreateQuiz, tiPresentateQuiz, tiGetHelp);
        
        treeTopics.getSelectionModel().selectedItemProperty().addListener((ov, v1, v2) -> {
            if(v2.getValue().getUrl() != null) eng.load(getClass().getResource("/de/mafel/nawilauncher/res/helpfiles/" + v2.getValue().getUrl()).toExternalForm());
        });
        
        try {
            eng.load(new File(System.getProperty("user.dir") + "\\help\\index.html").toURI().toURL().toExternalForm());
        } catch (MalformedURLException ex) {
            LogHelper.error(ex);
        }
    }
    
}
