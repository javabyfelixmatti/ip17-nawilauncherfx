package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.GameBackup;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class EvaluationSceneController implements Initializable, DynamicController {

    @FXML
    Label lblExit;
    
    @FXML
    AnchorPane root;
    
    @FXML
    VBox vb;
    
    @FXML
    Label lblFirstName, lblFirst, lblSecondName, lblSecond, lblThirdName, lblThird;
    
    @FXML
    HBox hb;
    
    @FXML
    ImageView ivCrown;
    
    BarChart<String,Number> bc;

    private TableView<ObservableList<String>> table;
    
    @FXML
    private TableColumn<ObservableList<String>, String> tcPlace, tcTeam, tcPunkte;
    
    @FXML
    private void handleLblExitClicked(MouseEvent e) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quiz beenden");
        alert.setHeaderText("Wollen Sie das Quiz wirklich beenden?");
        alert.setContentText("Wenn Sie fortfahren, werden die Ergebniss gelöscht. Die Punktzahlen können nicht wiederhergestellt werden.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            GameBackup.clear();
            Scenes.changeScene("scenes/MainScene.fxml");
            Scenes.unloadScene("scenes/EvaluationScene.fxml");
        }
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table = new TableView<>();
        table.setPrefWidth(424);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
                
        tcPlace = new TableColumn<>("Platz");
        tcPlace.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().get(0)));
        
        tcTeam = new TableColumn<>("Team");
        tcTeam.setPrefWidth(200);
        tcTeam.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().get(1)));
        
        tcPunkte = new TableColumn<>("Punkte");
        tcPunkte.setPrefWidth(70);
        tcPunkte.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().get(2)));
        
        table.getColumns().addAll(tcPlace, tcTeam, tcPunkte);
        
        vb.getChildren().addAll(table);
        
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.tickLabelFontProperty().set(Font.font(20));
        xAxis.setStyle(" -fx-font-size: 1.4em;");
        NumberAxis yAxis = new NumberAxis();
        yAxis.tickLabelFontProperty().set(Font.font(20));
        yAxis.setStyle(" -fx-font-size: 1.4em;");
        
        bc = new BarChart<>(xAxis,yAxis);
        bc.setTitle("Punkte aller Teams");
        bc.setStyle("-fx-font-size: 2em;");
        xAxis.setLabel("Teams");
        yAxis.setLabel("Punkte");
        bc.setLegendVisible(false);
        HBox.setHgrow(bc, Priority.ALWAYS);
        
        hb.getChildren().add(bc);
        
        ivCrown.setImage(new Image(Main.class.getResourceAsStream("res/crown.jpg")));
    }    
    
    @Override
    public void onSceneShown() {
        ObservableList<ObservableList<String>> items = table.getItems();
        items.clear();
        
        TeamManager.getTeams().stream()
                .sorted((Team o1, Team o2) -> Integer.compare(o2.getScore(), o1.getScore()))
                .forEachOrdered(
                        t -> items.add(FXCollections.observableArrayList("" + (items.size()+1), t.getName(), "" + t.getScore()))
                );
        switch (items.size()) {
            case 1:
                lblFirstName.setText(items.get(0).get(1));
                lblFirst.setVisible(true);
                lblSecond.setVisible(false);
                lblThird.setVisible(false);
                break;
                
            case 2:
                lblFirstName.setText(items.get(0).get(1));
                lblSecondName.setText(items.get(1).get(1));
                lblFirst.setVisible(true);
                lblSecond.setVisible(true);
                lblThird.setVisible(false);
                break;
                
            default:
                lblFirstName.setText(items.get(0).get(1));
                lblSecondName.setText(items.get(1).get(1));
                lblThirdName.setText(items.get(2).get(1));
                lblFirst.setVisible(true);
                lblSecond.setVisible(true);
                lblThird.setVisible(true);
                break;
        }
        
        XYChart.Series series = new XYChart.Series<>();       
        items.forEach(t -> series.getData().add(new XYChart.Data(t.get(1), Integer.parseInt(t.get(2)))));
        bc.getData().clear();
        bc.getData().add(series);
    }
    
}
