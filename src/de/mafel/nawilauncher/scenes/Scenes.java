package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.Main;
import java.io.IOException;
import java.util.HashMap;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

/**
 * Laedt Szenen in ein Feld und wechselt zwischen diesen bei Bedarf.
 * 
 * Durch das Ablegen im Arbeitsspeicher geschieht der Wechsel zwischen Szenen schneller.
 * 
 * 
 * @author matti, Felix
 */
public class Scenes {
    
    private static final HashMap<String, Parent> roots = new HashMap<>();
    private static final HashMap<String, Object> controllers = new HashMap<>();
    private static String currentFxmlFile = null;
    
    /**
     * Lädt ein Parent aus einer FXML-Datei und lässt dieses auf der MainScene anzeigen.
     * 
     * Geladene Parents werden gecachet und bei erneutem Aufruf direkt angezeigt, jedoch nicht neugeladen (performancesparend).
     * Ist reload wahr, wird der Cache nicht abgefragt und in jedem Fall der Parent neu aus der FXML-Datei geladen.
     * Beim ersten Laden (bzw. beim Reloaden) wird standardmaessig die initialize()-Funktion des zugehoerigen FXML-Controllers aufgerufen.
     * Bei jedem Anzeigen, auch bei Aufruf aus dem Cache, wird außerdem die Funktion onSceneShown() aufgerufen, sofern der FXML-Controller den DynamicController implementiert.
     *
     * @param fxmlFile Der Pfad der FXML-Datei relativ zur Main-Klasse (i.d.R. scenes/???.fxml)
     * @param reload Wenn wahr, wird der Cache nicht verwendet.
     * 
     * @see DynamicController DynamicController
     * @see DynamicController#onSceneShown() DynamicController::onSceneShown()
     * @see Initializable#initialize(java.net.URL, java.util.ResourceBundle) Initializable:initialize(URL, ResourceBundle)
     */
    public static void changeScene(String fxmlFile, boolean reload) {
        try {
            boolean load = !roots.containsKey(fxmlFile) || reload;
            if(load) {
                FXMLLoader loader = new FXMLLoader(Main.class.getResource(fxmlFile));
                roots.put(fxmlFile, loader.load());
                controllers.put(fxmlFile, loader.getController());
                Main.getMainScene().setRoot(roots.get(fxmlFile));
            }
            Object controller = controllers.get(fxmlFile);
            try {
                ((DynamicController) controller).onSceneShown();
            } catch(ClassCastException ex) {
                LogHelper.warning("Warnung: Controller für " + fxmlFile + " ist kein DynamicController.");
            } catch(Exception ex) {
                LogHelper.error("Fehler beim Ausführen von onSceneShown für " + fxmlFile, ex);
            }
            if(!load) {
                Main.getMainScene().setRoot(roots.get(fxmlFile));
            }
            currentFxmlFile = fxmlFile;
        } catch (IOException ex) {
            LogHelper.error(ex);
        }
    }
    
    /**
     * Lädt ein Parent aus einer FXML-Datei und lässt dieses auf der MainScene anzeigen.
     * 
     * Geladene Parents werden gecachet und bei erneutem Aufruf direkt angezeigt, jedoch nicht neugeladen (performancesparend).
     * Beim ersten Laden (bzw. beim Reloaden) wird standardmaessig die initialize()-Funktion des zugehoerigen FXML-Controllers aufgerufen.
     * Bei jedem Anzeigen, auch bei Aufruf aus dem Cache, wird außerdem die Funktion onSceneShown() aufgerufen, sofern der FXML-Controller den DynamicController implementiert.
     *
     * @param fxmlFile Der Pfad der FXML-Datei relativ zur Main-Klasse (i.d.R. scenes/???.fxml)
     * 
     * @see Scenes#changeScene(javafx.scene.Scene, java.lang.String, boolean) changeScene(Scene, String, boolean)
     * @see DynamicController DynamicController
     * @see DynamicController#onSceneShown() DynamicController::onSceneShown()
     * @see Initializable#initialize(java.net.URL, java.util.ResourceBundle) Initializable:initialize(URL, ResourceBundle)
     */
    public static void changeScene(String fxmlFile) {
        changeScene(fxmlFile, false);
    }
    
    public static boolean unloadScene(String fxmlFile) {
        return controllers.remove(fxmlFile) != null && roots.remove(fxmlFile) != null;
    }
    
    public static void reloadScene() {
        changeScene(currentFxmlFile, true);
    }
    
    public static void setFullscreen(boolean fullscreen) {
        Main.getStage().setFullScreen(fullscreen);
    }
}
