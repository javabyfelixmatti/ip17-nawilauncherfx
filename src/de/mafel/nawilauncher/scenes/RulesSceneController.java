package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class RulesSceneController implements Initializable, DynamicController {

    @FXML
    Label lblRules, lblQuizGrade, lblQuizTitle, lblQuizSubtitle;
    
    @FXML
    Button btnForward;
    
    @FXML
    private void btnForwardAction(ActionEvent event) {
        Scenes.changeScene("scenes/CategoryScene.fxml", true); //CategoryScene wird neugeladen, damit Kategorien und Punktzahlen aktualisiert werden
    }
    
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        GUtils.bindFontSizeToBounds(lblRules);
    }    

    @Override
    public void onSceneShown() {
        Navigator.setQuizState(QuizState.CATEGORIES);
        lblQuizTitle.setText(QuizData.getQuizTitle());
        lblQuizSubtitle.setText(QuizData.getQuizSubtitle());
        if (QuizData.getGrade().isEmpty()) {
            lblQuizGrade.setOpacity(0);
        } else {
            lblQuizGrade.setText("Klasse: " + QuizData.getGrade());
            lblQuizGrade.setOpacity(1);
        }
        lblRules.setText(QuizData.getRules());
        GUtils.adjustFontSizeToBounds(lblRules);
    }
    
    
}
