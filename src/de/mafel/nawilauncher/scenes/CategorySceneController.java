package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.scenes.components.CategoryButton;
import de.mafel.nawilauncher.scenes.components.GUtils;
import de.mafel.nawilauncher.scenes.components.TeamLabel;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class CategorySceneController implements Initializable, DynamicController {

    private ArrayList<CategoryButton> categoryButtons = new ArrayList<>();
    private ArrayList<Label> categoryLabels = new ArrayList<>();
    private HashMap<Team, Label> teamLabels = new HashMap<>();
    
    @FXML
    private AnchorPane mainPane;
    
    @FXML
    private GridPane gridPane;
    
    @FXML
    private Label lblActiveTeam, lblRound, lblRoundStatus;
    
    @FXML
    private HBox hbLblQuestionForAll;
    
    @FXML
    private ImageView leftTopIcon, rightTopIcon;
    
    @FXML
    private void btnBackAction(ActionEvent e) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Quiz beenden");
        alert.setHeaderText("Wollen Sie das Quiz wirklich beenden?");
        alert.setContentText("Wenn Sie fortfahren, werden die Ergebnisse angezeigt. Es können keine weiteren Fragen beantwortet werden.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Scenes.changeScene("scenes/EvaluationScene.fxml");
        }
    }
    
    /*@FXML
    private void btnAction(ActionEvent e) {
        Scenes.changeScene(btnBack.getScene(), "scenes/ABCDQuestionScene.fxml");
    }*/
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<Integer> scores = QuizData.getScores();
        List<Category> categories = QuizData.getCategories();
        
        for(int cat = 0; cat < categories.size(); cat++) {
            Category category = categories.get(cat);
            //Kategorie Label
            Label categoryLabel = new Label(category.getId());
            categoryLabel.setAlignment(Pos.CENTER);
            categoryLabel.getStyleClass().add("categoryLabel");
            categoryLabel.setMaxWidth(Double.MAX_VALUE);
            categoryLabel.setMaxHeight(Double.MAX_VALUE);
            gridPane.add(categoryLabel, 0, cat);
            categoryLabels.add(categoryLabel);
            //Buttons fuer Kategorie
            for(int sc = 0; sc < scores.size(); sc++) {
                CategoryButton cbtn = new CategoryButton(new QuestionIdentifier(category, scores.get(sc)));
                categoryButtons.add(cbtn);
                gridPane.add(cbtn, sc + 1, cat);
            }
            //Constraints der Reihe setzen
            RowConstraints rc = new RowConstraints(50.0, 50.0, Double.MAX_VALUE);
            rc.setVgrow(Priority.ALWAYS);
            gridPane.getRowConstraints().add(rc);
        }
        //Constraints für alle Spalten setzen
        for(int sc = 0; sc <= scores.size(); sc++) {
            ColumnConstraints cc = new ColumnConstraints(100, sc == 0 ? 500 : 100, Double.MAX_VALUE);
            cc.setHgrow(Priority.ALWAYS);
            gridPane.getColumnConstraints().add(cc);
        }
        //Team Labels am unteren Bildschirmrand
        int tlength = TeamManager.getTeams().size();
        HBox teamHBox = new HBox(8);
        teamHBox.setLayoutX(14);
        teamHBox.setLayoutY(644);
        teamHBox.setMaxWidth(HBox.USE_PREF_SIZE);
        for(int i = 0; i < tlength; i++) {
            Team t = TeamManager.getTeams().get(i);
            TeamLabel tlbl = new TeamLabel(t);
            teamHBox.getChildren().add(tlbl);
            teamLabels.put(t, tlbl);
        }
        //Anchors festlegen & hinzufügen
        AnchorPane.setBottomAnchor(teamHBox, 8.0);
        AnchorPane.setLeftAnchor(teamHBox, 14.0);
        AnchorPane.setRightAnchor(teamHBox, 150.0);
        mainPane.getChildren().add(teamHBox);
    
        leftTopIcon.visibleProperty().bind(Main.getMainScene().widthProperty().greaterThan(1000).and(Main.getMainScene().heightProperty().greaterThan(700)));
        rightTopIcon.visibleProperty().bind(Main.getMainScene().widthProperty().greaterThan(1000).and(Main.getMainScene().heightProperty().greaterThan(700)));
        
        gridPane.widthProperty().addListener(this::gridPaneSizeChanged);
        gridPane.heightProperty().addListener(this::gridPaneSizeChanged);
    }
    
    @Override
    public void onSceneShown() {
        categoryButtons.forEach((cbtn) -> {
            cbtn.update();
        });
        teamLabels.forEach((team, label) -> label.setText(team.getName() + " - " + team.getScore()));
        Navigator.setQuizState(QuizState.CATEGORIES);
        if (TeamManager.getActiveTeam() == null) {
            lblActiveTeam.setText(TeamManager.getActiveForAllTeam().getName());
            lblActiveTeam.setBackground(new Background(new BackgroundFill(TeamManager.getActiveForAllTeam().getColor(), new CornerRadii(5), Insets.EMPTY)));
            lblActiveTeam.setTextFill(GUtils.getTextColor(TeamManager.getActiveForAllTeam().getColor()));
            hbLblQuestionForAll.setVisible(true);
            if (QuizData.getQuestionForAllHandling() == QuizData.QuestionForAllHandling.CHOOSE_AFTER_EACH_ROUND) categoryButtons.parallelStream().forEach(cb -> cb.setDisable(!cb.getAssignedQuestion().getCategory().getType()));
        } else {
            lblActiveTeam.setText(TeamManager.getActiveTeam().getName());
            hbLblQuestionForAll.setVisible(false);
            lblActiveTeam.setBackground(new Background(new BackgroundFill(TeamManager.getActiveTeam().getColor(), new CornerRadii(5), Insets.EMPTY)));
            lblActiveTeam.setTextFill(GUtils.getTextColor(TeamManager.getActiveTeam().getColor()));
            if (QuizData.getQuestionForAllHandling() == QuizData.QuestionForAllHandling.CHOOSE_AFTER_EACH_ROUND) categoryButtons.parallelStream().forEach(cb -> cb.setDisable(cb.getAssignedQuestion().getCategory().getType()));
        }
        lblRound.setText("" + TeamManager.getRound());
        if (TeamManager.getActiveTeamInt() == TeamManager.getTeams().size()) {
            lblRoundStatus.setText("ALLE");
        } else lblRoundStatus.setText(TeamManager.getActiveTeamInt()+1 + "/" + TeamManager.getTeams().size());
        if (QuizData.getQuizIcon() != null) leftTopIcon.setImage(QuizData.getQuizIcon());
        System.gc();
        Platform.runLater(() -> gridPaneSizeChanged(null, null, null));
    }
    
    private void gridPaneSizeChanged(ObservableValue<? extends Number> o, Number v1, Number v2) {
        final String style = "-fx-font-size: " + Double.min(categoryButtons.get(0).getHeight() * 0.7, categoryLabels.parallelStream().map(GUtils::getOptimalFontSize).min(Double::compare).orElse(16.0)) + ";";
        Platform.runLater(() -> {
            categoryLabels.forEach(l -> l.setStyle(style));
            categoryButtons.forEach(btn -> btn.setStyle(style));
        });
    }
    
}
