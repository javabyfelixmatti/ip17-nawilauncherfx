package de.mafel.nawilauncher.scenes;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.data.GameBackup;
import de.mafel.nawilauncher.scenes.components.DynamicController;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author matti
 */
public class ExplanationSceneController implements Initializable, DynamicController {

    @FXML
    private Label lblCatScore, lblQuestion, lblAnswer, lblExplanation;
    
    @FXML
    private VBox scoreVbox, answerVbox;
    
    @FXML
    private HBox hbox;

    @FXML
    private TableView<ObservableList<String>> tableTeams;
    
    @FXML
    private TableColumn<ObservableList<String>, String> tcTeams, tcAnswer, tcScore;
    
    @FXML
    private AnchorPane root;
    
    @FXML
    private void handleLblBackClicked(MouseEvent e) {
        Navigator.markQuestionDone();
        Navigator.resetActiveQuestion();
        Scenes.changeScene("scenes/CategoryScene.fxml");
    }
    
    @FXML
    private void handleLblContinueClicked(MouseEvent e) {
        try {
            Navigator.markQuestionDone();
            Navigator.resetActiveQuestion();
            Navigator.transferScore();
        } catch(Exception ex) {
            new ExceptionDialog(ex, "Es konnten keine Punkte vergeben werden, da keine gültigen Ergebnisse vorlagen. Das Quiz wird normal fortgesetzt.");
        }
        try {
            GameBackup.backupGame();
        } catch (NullPointerException ex) {
            System.out.println("Es wurde kein Backup ausgeführt, da die Einstellungen nicht initialisiert wurden.");
        } catch (BackingStoreException ex) {
            LogHelper.error("Das Spiel konnte nicht gesichert werden.", ex);
        }
        Scenes.changeScene("scenes/CategoryScene.fxml");
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcTeams.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().get(0)));
        tcAnswer.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().get(1)));
        tcAnswer.setCellFactory((TableColumn<ObservableList<String>, String> param) -> new TableCell<ObservableList<String>, String>() {
            private Text text;
            
            @Override
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    text = new Text(item);
                    text.setWrappingWidth(tcAnswer.getWidth()-5);
                    this.setWrapText(true);
                    setGraphic(text);
                } else {
                    setGraphic(null);
                }
            }
        });
        tcScore.setCellValueFactory((param) -> new ReadOnlyObjectWrapper<>(param.getValue().get(2)));
        
        GUtils.bindFontSizeToBounds(lblQuestion);
        GUtils.bindFontSizeToBounds(lblAnswer);
        GUtils.bindFontSizeToBounds(lblExplanation);
        
        answerVbox.prefWidthProperty().bind(hbox.widthProperty().multiply(3.0 / 4.0).subtract(50));
        scoreVbox.prefWidthProperty().bind(hbox.widthProperty().divide(4.0).add(30));
        lblQuestion.prefHeightProperty().bind(root.heightProperty().divide(9.0));
        hbox.prefHeightProperty().bind(root.heightProperty().multiply(4.0 / 9.0));
    }    

    @Override
    public void onSceneShown() {
        Question activeQuestion = Navigator.getActiveQuestion();
        lblCatScore.setText(activeQuestion.getCategory().getId() + " " + activeQuestion.getScore());
        lblQuestion.setText(activeQuestion.getQuestion());
        if (!(activeQuestion instanceof TextQuestion)) {
            lblAnswer.setText(activeQuestion.getCorrectAnswerString());
        } else lblAnswer.setText("");
        lblExplanation.setText(activeQuestion.getExplanation());
        
        //Tabelle befüllen
        ObservableList<ObservableList<String>> items = tableTeams.getItems();
        items.clear();
        if (activeQuestion instanceof TextQuestion) {
            Navigator.getResults().forEach((t, result) -> 
                    items.add(FXCollections.observableArrayList(t.getName(), result.isCorrectString(), result.getSignedScoreDiff()))
            );
        } else {
            Navigator.getResults().forEach((t, result) -> 
                    items.add(FXCollections.observableArrayList(t.getName(), result.getAnswer(), result.getSignedScoreDiff()))
            );
        }
    }
    
}
