package de.mafel.nawilauncher.scenes.components;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.TeamManager;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

/**
 *
 * @author matti, Felix
 */
public class Timer {

    int startTime = 0, timeSeconds = 0;
    Timeline timeline, timelinePB;
    AnchorPane ap = null;
    Label lblTimeLimit = new Label();
    ProgressBar pbTimeLimit = new ProgressBar();
    private final BooleanProperty timerStateProperty = new SimpleBooleanProperty(false);
    private boolean isFinalPhase = false, startOnClick = false;

    public boolean isTimerState() {
        return timerStateProperty.get();
    }

    public void setTimerState(boolean value) {
        timerStateProperty.set(value);
    }

    public BooleanProperty timerStateProperty() {
        return timerStateProperty;
    }
    
    public Timer(int startTime, boolean enableLastSeconds) {
        this.startTime = startTime;
        if(enableLastSeconds) TeamManager.forEachTeam(t -> {
            t.answerProperty().addListener((o, v1, v2) -> {
                if(v2 != null) setLastSeconds(30);
            });
        });
    }

    public final void updateStartTime() {
        lblTimeLimit.setText(startTime + "s");
        pbTimeLimit.setProgress(1);
        pbTimeLimit.setStyle(null);
    }
    
    public AnchorPane getTimer() {
        if(ap != null) return ap;
        ap = new AnchorPane();
        ap.setPrefHeight(32.0);
        ap.setPrefWidth(243.0);
        ap.setMaxHeight(42.0);
        
            pbTimeLimit.setLayoutX(2);
            pbTimeLimit.setLayoutY(2);
            pbTimeLimit.setPrefHeight(49);
            pbTimeLimit.setPrefWidth(322);
            pbTimeLimit.setProgress(1);
            AnchorPane.setBottomAnchor(pbTimeLimit, 0D);
            AnchorPane.setLeftAnchor(pbTimeLimit, 0D);
            AnchorPane.setRightAnchor(pbTimeLimit, 0D);
            AnchorPane.setTopAnchor(pbTimeLimit, 0D);
        
            lblTimeLimit.setAlignment(Pos.CENTER);
            lblTimeLimit.setLayoutX(100);
            lblTimeLimit.setLayoutY(2D);
            lblTimeLimit.setPrefHeight(49D);
            lblTimeLimit.setPrefWidth(194);
            lblTimeLimit.setText(startTime + "s");
            lblTimeLimit.setTextAlignment(TextAlignment.CENTER);
            AnchorPane.setBottomAnchor(lblTimeLimit, 0D);
            AnchorPane.setLeftAnchor(lblTimeLimit, 0D);
            AnchorPane.setRightAnchor(lblTimeLimit, 0D);
            AnchorPane.setTopAnchor(lblTimeLimit, 0D);
            lblTimeLimit.setStyle("-fx-font-size: 28.0");
        
        ap.getChildren().clear();
        ap.getChildren().addAll(pbTimeLimit, lblTimeLimit);
        ap.setOnMouseClicked(e -> {
            System.out.println("click " + startOnClick);
            if(startOnClick) {
                startOnClick = false;
                start();
            }
        });
        return ap;
    }
    
    public int getCurrentTime() {
        return timeSeconds;
    }
    
    public void startOnClick() {
        startOnClick = true;
    }
    
    public void startOnClick(int s) {
        startTime = s;
        startOnClick = true;
        updateStartTime();
    }
    
    /**
     * Startet den Timer mit dem übergebenen Startwert.
     * @param s TimeLimit in Sekunden
     */
    public void start(int s) {
        startTime = s;
        updateStartTime();
        start();
    }
    
    public void start() {
        if (startTime <= 0) {
            hideTimer();
            return;
        } else {
            ap.setVisible(true);
        }
        if(timeline != null) timeline.stop();
        if(timelinePB != null) timelinePB.stop();
        timeSeconds = startTime;
 
        // update timerLabel
        lblTimeLimit.setText(getTimeString());
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        pbTimeLimit.setStyle(null);
        timeline.getKeyFrames().addAll(
                new KeyFrame(Duration.seconds(1), event -> {
                    timeSeconds--;
                    // update timerLabel
                    lblTimeLimit.setText(getTimeString());
                    if (timeSeconds <= 0) {
                        timerStateProperty.set(false);
                        pbTimeLimit.setStyle("-fx-text-box-border: dark-red;");
                        pbTimeLimit.setStyle("-fx-control-inner-background: red");
                        stop();
                    }
                })
        );
        timelinePB = new Timeline();
        timelinePB.getKeyFrames().addAll(
                new KeyFrame(Duration.ZERO, new KeyValue(pbTimeLimit.progressProperty(), 1)),
                new KeyFrame(Duration.seconds(timeSeconds), new KeyValue(pbTimeLimit.progressProperty(), 0))
        );
        timeline.playFromStart();
        timelinePB.playFromStart();
        timerStateProperty.set(true);
        
        if(Main.getServer().getState()) Main.getServer().getWebSocketServer().broadcastMessageIfAssigned("t(" + startTime + "," + startTime + ");");
    }
    
    public void resume() {
        if(timeline != null) timeline.playFrom(new Duration((startTime - timeSeconds) * 1000));
        if(timelinePB != null) timelinePB.playFrom(new Duration((startTime - timeSeconds) * 1000));
        Main.getServer().getWebSocketServer().broadcastMessageIfAssigned("t(" + timeSeconds + "," + startTime + ");");
    }
    
    public void stop() {
        if(timeline != null) timeline.stop();
        if(timelinePB != null) timelinePB.stop();
        Main.getServer().getWebSocketServer().broadcastMessageIfAssigned("t(0,0);");
    }
    
    public String getTimeString() {
        int minutes = (int) Math.floor(timeSeconds / 60D);
        int seconds = timeSeconds - minutes * 60;
        if (minutes != 0) {
            return minutes + "min " + seconds + "s";      
        } else {
            return seconds + "s";      
        } 
    }
    
    public void setLastSeconds(int s) {
        if ((!isFinalPhase) && timeSeconds > s) {
            if(Main.getServer().getState()) Main.getServer().getWebSocketServer().broadcastMessageIfAssigned("t(" + s + "," + startTime + ");");
            isFinalPhase = true;
            startTime = s;
            if (startTime <= 0) {
                hideTimer();
                return;
            } else {
                ap.setVisible(true);
            }
            stop();
            timeSeconds = startTime;

            pbTimeLimit.setStyle(null);
            pbTimeLimit.setStyle("-fx-text-box-border: grey;");
            pbTimeLimit.setStyle("-fx-control-inner-background: yellow;");
            
            // update timerLabel
            lblTimeLimit.setText(getTimeString());
            timeline = new Timeline();
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.seconds(1), event -> {
                        timeSeconds--;
                        // update timerLabel
                        lblTimeLimit.setText(getTimeString());
                        if (timeSeconds <= 0) {
                            timerStateProperty.set(false);
                            pbTimeLimit.setStyle("-fx-text-box-border: dark-red;");
                            pbTimeLimit.setStyle("-fx-control-inner-background: red");
                            stop();
                        }
                    })
            );
            timelinePB = new Timeline();
            timelinePB.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, new KeyValue(pbTimeLimit.progressProperty(), 1)),
                    new KeyFrame(Duration.seconds(timeSeconds), new KeyValue(pbTimeLimit.progressProperty(), 0))
            );
            timeline.playFromStart();
            timelinePB.playFromStart();
            timerStateProperty.set(true);
        }
    }
    
    public void hideTimer() {
        timerStateProperty.set(false);
        ap.setVisible(false);
    }
    
    public void showTimer() {
        ap.setVisible(true);
    }
}
