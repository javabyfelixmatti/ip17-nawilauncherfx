/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.mafel.nawilauncher.scenes.components;

import javafx.geometry.Point2D;
import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author matti
 */
public class Popup {
    
    //Nutzen des Tooltips:
    //Popup.showTooltip(Main.getStage(), mainMenuButton, "Dies ist ein Test-Tooltip. Mal sehen wie das aussieht", null);
    
    public static void showTooltip(Stage owner, Control control, String tooltipText, ImageView tooltipGraphic) {
        Point2D p = control.localToScene(0.0, 0.0);

        final Tooltip customTooltip = new Tooltip();
        customTooltip.setStyle("-fx-font-size: 20px");
        customTooltip.setText(tooltipText);

        control.setTooltip(customTooltip);
        customTooltip.setAutoHide(true);

        customTooltip.show(owner, p.getX()
            + control.getScene().getX() + control.getScene().getWindow().getX(), p.getY()
            + control.getScene().getY() + control.getScene().getWindow().getY());

    }
}
