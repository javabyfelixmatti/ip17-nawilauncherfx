package de.mafel.nawilauncher.scenes.components;

import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.data.questions.Question;
import java.io.File;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Labeled;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

/**
 *
 * @author Felix, Matti
 */
public class GUtils {
    
    public static void createImageBox(HBox imageBox, Question activeQuestion) {
        ObservableList<MediaObject> media = activeQuestion.getMedia();
        imageBox.getChildren().clear();
        media.forEach(imageObject -> {
            if (imageObject instanceof ImageObject) {
                imageBox.getChildren().add(createImageView(imageBox, ((ImageObject) imageObject).getImage()));
            }
        });
        imageBox.setAlignment(Pos.CENTER);
    }
    
    private static ImageView createImageView(Region parent, Image image) {
        ImageView imageView = new ImageView(image);
        imageView.fitHeightProperty().bind(parent.heightProperty().subtract(15));
        imageView.setPreserveRatio(true);
        return imageView;
    }
    
    public static Color getTextColor(Color background) {
        if(getBrightness(background)> 0.9) return Color.BLACK;
        else return Color.WHITE;
    }
    
    private static double getBrightness(Color c) {
        return 0.2126 * c.getRed() + 0.7152 * c.getGreen() + 0.0722 * c.getBlue();
    }
    
    public static double getOptimalFontSize(final Labeled l) {
        return getOptimalFontSize(l, 16, 48);
    }
    
    public static double getOptimalFontSize(final Labeled l, int minFontSize, int maxFontSize) {
        //Avoid division by zero
        if(l.getWidth() == 0 || l.getHeight() == 0) return minFontSize;
        //Determine twidth, theight, v
        Text helper = new Text();
        helper.setFont(l.getFont());
        helper.setText(l.getText().replace("\n", ""));
        helper.setWrappingWidth(0);
        helper.setLineSpacing(l.getLineSpacing());
        double twidth = helper.prefWidth(-1);
        double theight = l.getFont().getSize() + l.getLineSpacing();
        final double v = 1.2 * twidth / theight; //1.2 = random factor
        //Adjust theight
        double width = l.getWidth() - l.getPadding().getLeft() - l.getPadding().getRight(), height = l.getHeight() - l.getPadding().getTop() - l.getPadding().getBottom();
        double filledHeight = 0.0;
        minFontSize += l.getLineSpacing();
        maxFontSize += l.getLineSpacing();
        //Adjust up
        do {
            theight++;
        } while((filledHeight = theight * ((int) (theight * v) / (int) width)) <= height && theight < maxFontSize);
        //Adjust down
        do {
            theight--;
        } while((filledHeight = theight * ((int) (theight * v) / (int) width)) >= height && theight > minFontSize);
        if(theight > minFontSize) {
            //Additionally adjust down if necessary because of line breaks (higher performance cost)
            helper.setFont(new Font(l.getFont().getName(), theight - l.getLineSpacing()));
            helper.setWrappingWidth(width);
            helper.setText(l.getText());
            double prefHeight;
            if((prefHeight = helper.prefHeight(width)) > height) do {
                theight--;
                helper.setFont(new Font(l.getFont().getName(), theight - l.getLineSpacing()));
                //System.out.println("theight=" + theight + ",prefHeight=" + prefHeight);
            } while((prefHeight = helper.prefHeight(width)) > height && theight > minFontSize);
        }
        theight *= 0.9; //Lowering the font size some more seems to produce more accurate results
        if(theight < minFontSize) theight = minFontSize;
        return theight - l.getLineSpacing();
    }
    
    public static void adjustFontSizeToBounds(final Labeled l, int minFontSize, int maxFontSize) {
        l.setStyle("-fx-font-size: " + getOptimalFontSize(l, minFontSize, maxFontSize) + ";");
        //System.out.println("width=" + width + ",height=" + height + ",twidth=" + twidth + ",theight=" + theight + ",linespacing=" + l.getLineSpacing());
    }
    
    public static void adjustFontSizeToBounds(final Labeled l) {
        adjustFontSizeToBounds(l, 16, 48);
    }
    
    public static void bindFontSizeToBounds(final Labeled l) {
        ChangeListener<Object> listener = (o, v1, v2) -> new Thread(() -> {
            final double fontsz = getOptimalFontSize(l);
            Platform.runLater(() -> l.setStyle("-fx-font-size: " + fontsz + ";"));
        }).start();
        l.widthProperty().addListener(listener);
        l.heightProperty().addListener(listener);
        l.textProperty().addListener(listener);
        listener.changed(null, null, null);
    }
    
    private static FileChooser fileChooser = null;
    
    public static FileChooser getFileChooser(String title) {
        FileChooser.ExtensionFilter ef = new FileChooser.ExtensionFilter("NaWiLauncher Savefiles (*.nwq)", "*.nwq");
        if(fileChooser == null) {
            fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        }
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().add(ef);
        fileChooser.setSelectedExtensionFilter(ef);
        fileChooser.setTitle(title);
        return fileChooser;
    }
    
    public static FileChooser getDatabaseFileChooser(String title) {
        FileChooser.ExtensionFilter ef = new FileChooser.ExtensionFilter("NaWiLauncher Database-Savefiles (*.nwd)", "*.nwd");
        if(fileChooser == null) {
            fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        }
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().add(ef);
        fileChooser.setSelectedExtensionFilter(ef);
        fileChooser.setTitle(title);
        return fileChooser;
    }
    
    //Fischer-Yates-Verfahren zum zufälligen Sortieren von Arrays, siehe https://de.wikipedia.org/wiki/Zuf%C3%A4llige_Permutation#Fisher-Yates-Verfahren
    public static <T> T[] shuffleArray(T[] array) {
        T[] ar = Arrays.copyOf(array, array.length);
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            T a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;
    }
    
}
