package de.mafel.nawilauncher.scenes.components;

import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Felix
 */
public class TeamLabel extends Label implements javafx.event.EventHandler<MouseEvent> {
    
    private Team team;
    
    public TeamLabel(Team team) {
        super(team.getName());
        this.team = team;
        setAlignment(Pos.CENTER);
        setPrefHeight(42);
        getStyleClass().add("draggableTeamLabel");
        setTextAlignment(TextAlignment.CENTER);
        setPadding(new Insets(10));
        setBackground(new Background(new BackgroundFill(team.getColor(), new CornerRadii(5), Insets.EMPTY)));
        setTextFill(GUtils.getTextColor(team.getColor()));
        setOnDragDetected(this);
        team.answerProperty().addListener((observable, oldValue, newValue) -> Platform.runLater(() -> setText((newValue == null ? "" : "✓ ") + team.getName())));
    }

    @Override
    public void handle(MouseEvent event) {
        if(!(event.getSource() instanceof Label)) return;
        Label src = (Label) event.getSource();
        Dragboard db = src.startDragAndDrop(TransferMode.ANY);

        ClipboardContent content = new ClipboardContent();
        content.putString(TeamManager.getTeamId(team) + "");
        db.setContent(content);

        event.consume();
    }
    
}
