
package de.mafel.nawilauncher.scenes.components;

/**
 *
 * @author matti
 */
public class HelpEntry {
    String name, url;
    
    public HelpEntry(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return getName();
    }
}
