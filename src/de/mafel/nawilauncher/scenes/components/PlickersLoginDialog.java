package de.mafel.nawilauncher.scenes.components;

import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

/**
 *
 * @author Felix
 */
public class PlickersLoginDialog extends Dialog<Pair<String, String>> {
    
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final ButtonType LOGIN_BUTTON_TYPE = new ButtonType("Login", ButtonData.OK_DONE);

    public PlickersLoginDialog() {
        super();
        super.setTitle("Login");
        super.setHeaderText("Zugangsdaten eingeben:");
        super.getDialogPane().getButtonTypes().addAll(LOGIN_BUTTON_TYPE, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField email = new TextField();
        email.setPromptText("E-Mail");
        PasswordField password = new PasswordField();
        password.setPromptText("Passwort");

        grid.add(new Label("E-Mail:"), 0, 0);
        grid.add(email, 1, 0);
        grid.add(new Label("Passwort:"), 0, 1);
        grid.add(password, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = super.getDialogPane().lookupButton(LOGIN_BUTTON_TYPE);
        loginButton.setDisable(true);

        // Validate e-mail
        email.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(password.getText().length() < 6 || !VALID_EMAIL_ADDRESS_REGEX.matcher(newValue).find());
        });
        
        //Validate password
        password.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.length() < 6 || !VALID_EMAIL_ADDRESS_REGEX.matcher(email.getText()).find());
        });

        super.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> email.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        super.setResultConverter(dialogButton -> {
            if (dialogButton == LOGIN_BUTTON_TYPE) {
                return new Pair<>(email.getText(), password.getText());
            }
            return null;
        });
                
    }

    
    
}
