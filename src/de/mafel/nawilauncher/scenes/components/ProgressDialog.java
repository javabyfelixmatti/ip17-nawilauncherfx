package de.mafel.nawilauncher.scenes.components;

import de.mafel.nawilauncher.Main;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Felix
 */
public class ProgressDialog extends Stage {
    
    private final ProgressBar pb = new ProgressBar();
    private final ProgressIndicator pin = new ProgressIndicator();

    public ProgressDialog(String text) {
        super();
        initOwner(Main.getStage());
        initStyle(StageStyle.UTILITY);
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        setTitle(text);
        
        //Progress Bar
        final Label label = new Label(text);
        pb.setProgress(-1F);
        pin.setProgress(-1F);
        
        final HBox hb = new HBox(5);
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().addAll(pb, pin);
        hb.setPadding(new Insets(5));
        hb.setSpacing(5);
        
        Scene s = new Scene(hb);
        s.getStylesheets().add("/de/mafel/nawilauncher/scenes/Main.css");
        
        setScene(s);
        show();
    }
    
    public void activate(final Task<?> task) {
        pb.progressProperty().bind(task.progressProperty());
        pin.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded(event -> close());
    }
    
}
