package de.mafel.nawilauncher.scenes.components;

/**
 *
 * @author Felix
 */
public interface DynamicController {
    
    public void onSceneShown();
    
}
