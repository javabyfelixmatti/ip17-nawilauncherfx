package de.mafel.nawilauncher.scenes.components;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.QuizState;
import de.mafel.nawilauncher.data.questions.TextQuestion;
import de.mafel.nawilauncher.scenes.Scenes;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;

/**
 *
 * @author Felix
 */
public class CategoryButton extends Button implements EventHandler<ActionEvent> {

    private final QuestionIdentifier ASSIGNED_QUESTION;
    private boolean enabled = true;
    
    public CategoryButton(QuestionIdentifier assignedQuestion) {
        super(assignedQuestion.getScore() + "");
        this.ASSIGNED_QUESTION = assignedQuestion;
        super.setAlignment(Pos.CENTER);
        super.setContentDisplay(ContentDisplay.CENTER);
        super.setMnemonicParsing(false);
        super.getStyleClass().add("categoryBtn");
        super.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        super.setMinSize(20.0, 20.0);
        super.setOnAction(this);
        /*super.heightProperty().addListener(height -> {
            setStyle("-fx-font-size: " + (getHeight() * 0.7) + ";");
        });*/
        update();
    }

    public QuestionIdentifier getAssignedQuestion() {
        return ASSIGNED_QUESTION;
    }

    public final void update() {
        if(Navigator.isQuestionDone(ASSIGNED_QUESTION)) {
            if(!getStyleClass().contains("done")) getStyleClass().add("done");
        } else if(getStyleClass().contains("done")) getStyleClass().remove("done");
    }
    
    @Override
    public void handle(ActionEvent event) {
        if(Navigator.isQuestionDone(ASSIGNED_QUESTION)) return;
        if(!Main.getServer().getState() && QuizData.getQuestionFromIdentifier(ASSIGNED_QUESTION) instanceof TextQuestion) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Textfragen sind nicht verfügbar, da der Server nicht gestartet ist. Um Textfragen zu aktivieren, starten Sie den Server und beginnen Sie das Quiz erneut.", ButtonType.OK);
            alert.setTitle("NaWiLauncherFX");
            alert.setHeaderText("Server nicht aktiviert");
            alert.showAndWait();
            return;
        }
        Navigator.setActiveQuestion(ASSIGNED_QUESTION);
        String equipment = QuizData.getQuestionFromIdentifier(ASSIGNED_QUESTION).getEquipment();
        LogHelper.debug("Benötigtes Equipment: " + equipment);
        if (!(equipment.equals("keine") || equipment.equals("")|| equipment.equals("-keine"))) {
            Scenes.changeScene("scenes/EquipmentScene.fxml");
        } else {
            System.out.println(QuizData.getQuestionFromIdentifier(ASSIGNED_QUESTION).getSceneFile());
            Scenes.changeScene(QuizData.getQuestionFromIdentifier(ASSIGNED_QUESTION).getSceneFile());
        }
    }
    
}
