package de.mafel.nawilauncher.fileio;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;
import javafx.scene.image.Image;

/**
 *
 * @author Felix
 */
public class MediaStore {
    
    public static final File DIR = new File(System.getProperty("java.io.tmpdir"), "nwlp\\");
    
    private static int fileCounter = 1;
    
    static {
        DIR.mkdirs();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Versuche Media Store zu leeren...");
            for(int i = 0; i < 3; i++) {
                try {
                    if(MediaStore.clean()) break;
                    System.out.println((i + 1) + ". Fehlversuch");
                } catch(Exception ex) { }
                try {
                    Thread.sleep(1000);
                } catch(InterruptedException ex2) { break; }
            }
            System.out.println("Leeren beendet");
        }));
    }

    public static void setFileCounter(int fileCounter) {
        MediaStore.fileCounter = fileCounter;
    }

    public static int getFileCounter() {
        return fileCounter;
    }
    
    public static String nextFileName() {
        return String.format("%05d", fileCounter++);
    }
    
    public static String currentFileName() {
        return String.format("%05d", fileCounter);
    }
    
    public static final boolean clean() {
        try {
            Files.walkFileTree(DIR.toPath(), new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
                
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }
                
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
                
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
            return true;
        } catch (IOException ex) { }
        return false;
    }
    
    public static String put(File f) {
        try {
            String[] splitFileName = f.getName().split("\\.");
            String fileEnding = splitFileName[splitFileName.length - 1].toLowerCase();
            File target = null;
            do {
                target = new File(DIR, nextFileName() + "." + fileEnding);
            } while(target.exists());
            Files.copy(f.toPath(), target.toPath());
            return target.getName();
        } catch (IOException ex) {
            LogHelper.error(ex);
            return "";
        }
    }
    
    public static String put(File f, String name) {
        try {
            String[] splitFileName = f.getName().split("\\.");
            String fileEnding = splitFileName[splitFileName.length - 1].toLowerCase();
            File target = null;
            target = new File(DIR, name + "." + fileEnding);
            if(target.exists()) target.delete();
            Files.copy(f.toPath(), target.toPath());
            return target.getName();
        } catch (IOException ex) {
            LogHelper.error(ex);
            return "";
        }
    }
    
    public static File getFile(String filename) {
        return new File(DIR, filename);
    }
    
    public static File getIcon() {
        File f;
        if ((f = getFile("icon.png")).exists()) return f;
        if ((f = getFile("icon.jpg")).exists()) return f;
        if ((f = getFile("icon.bmp")).exists()) return f;
        if ((f = getFile("icon.gif")).exists()) return f;
        return null;
    }
    
    public static Image getIconImage() {
        try {
            File f;
            if ((f = getFile("icon.png")).exists()) return new Image(new FileInputStream(f));
            if ((f = getFile("icon.jpg")).exists()) return new Image(new FileInputStream(f));
            if ((f = getFile("icon.bmp")).exists()) return new Image(new FileInputStream(f));
            if ((f = getFile("icon.gif")).exists()) return new Image(new FileInputStream(f));
        } catch (FileNotFoundException ex) {
            LogHelper.error(ex);
        }
        return null;
    }
    
    public static void putIcon(File f) {
        put(f, "icon");
    }
    
    public static URL getURL(String filename) {
        try {
            return new File(DIR, filename).toURI().toURL();
        } catch (MalformedURLException ex) {
            new ExceptionDialog(ex, "Fehler beim Umformen der URL des Bildes " + filename + ": " + ex.getLocalizedMessage());
            return null;
        }
    }
    
    public static List<String> getEntries() {
        return Arrays.asList(DIR.list());
    }
    
    public static void remove(String path) {
        try {
            Files.delete(getFile(path).toPath());
        } catch (IOException ex) {
            LogHelper.error(ex);
        }
    }
    
}
