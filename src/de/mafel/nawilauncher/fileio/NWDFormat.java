package de.mafel.nawilauncher.fileio;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.database.Database;
import de.mafel.nawilauncher.scenes.components.ProgressDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author Felix
 */
public class NWDFormat {
    
    public static final int BUFFER_SIZE = 64;
    
    private static File openFile = null;
    
    public static File getOpenFile() {
        return openFile;
    }
    
    public static boolean isFileOpened() {
        return openFile != null;
    }
    
    public static Thread openFile(File f) {
        return openFile(f, null);
    }
    
    public static Thread openFile(File f, final Label stateLabel) {
        ProgressDialog progressDialog = new ProgressDialog("Öffne " + f.getName() + "...");
        //Öffnet Datei im Hintergrund und gibt Fortschritt regelmäßig an Hauptthread zurück
        Task<Void> task = new Task<Void>() {
            
            @Override
            protected Void call() throws Exception {
                try {
                    updateProgress(0, 3);
                    ZipFile zipFile = new ZipFile(f);
                    updateProgress(1, 3);
                    ZipEntry metadataFile = zipFile.getEntry("metadata.json");
                    if(metadataFile == null) throw new IOException("metadata.json konnte nicht gefunden werden.");
                    readMetadata(zipFile.getInputStream(metadataFile));
                    updateProgress(2, 3);
                    extractAllMedia(zipFile);
                    QuizData.setQuizIcon(MediaStore.getIconImage());
                    zipFile.close();
                    openFile = f;
                    updateProgress(3, 3);
                    if(stateLabel != null) Platform.runLater(() -> stateLabel.setText(QuizData.getQuizTitle() + " geöffnet."));
                } catch (IOException ex) {
                    new ExceptionDialog(ex, "Fehler beim Öffnen des Savefiles: " + ex.getLocalizedMessage());
                }
                return null;
            }
            
        };
        progressDialog.activate(task);
        Thread t = new Thread(task);
        t.start();
        return t;
    }
    
    private static void extractAllMedia(ZipFile zipFile) {
        Collections.list(zipFile.entries()).parallelStream().forEach(entry -> {
            if(entry.getName().startsWith("media\\")) {
                InputStream is = null;
                try {
                    is = zipFile.getInputStream(entry);
                    FileOutputStream os = new FileOutputStream(MediaStore.getFile(getFileNameFromEntryName(entry.getName())));
                    byte[] buffer = new byte[64];
                    int len;
                    while((len = is.read(buffer)) != -1) os.write(buffer, 0, len);
                    is.close();
                    os.flush();
                    os.close();
                } catch (IOException ex) {
                    LogHelper.error(ex);
                } finally {
                    try {
                        if(is != null) is.close();
                    } catch (IOException ex) {
                        LogHelper.error(ex);
                    }
                }
            }
        });
    }
    
    private static String getFileNameFromEntryName(String entryName) {
        String[] splitPath = entryName.split("\\\\");
        return splitPath[splitPath.length - 1];
    }
    
    private static void readMetadata(InputStream is) {
        try {
            //Erzeugen und Auslesen des JSONObjects
            JSONObject metadata = new JSONObject(new JSONTokener(new InputStreamReader(is, "UTF-8")));
            
            //Questions
            JSONArray questions = metadata.getJSONArray("questions");
            try {
                questions.forEach(q -> {
                    Database.getQuestions().add(Question.load(null, (JSONObject) q));
                });
            } catch(NumberFormatException ex) {
                new ExceptionDialog(ex, "Fehler beim Einlesen der Frage. [metadata.json]: Ungültige Punktzahl: " + ex.getLocalizedMessage());
            } catch(ArrayIndexOutOfBoundsException ex) {
                new ExceptionDialog(ex, "Fehler beim Einlesen der Frage. [metadata.json]: Ungültiger Fragenanzahl: " + ex.getLocalizedMessage());
            } catch(JSONException ex) {
                new ExceptionDialog(ex, "Fehler beim Einlesen der Frage. [metadata.json]: Ungültiger Wert: " + ex.getLocalizedMessage());
            }
            
            //Media Store
            try {
                MediaStore.setFileCounter(metadata.getInt("fileCounter"));
            } catch(JSONException ex) {
                MediaStore.setFileCounter(1);
            }
            
        } catch (IOException ex) {
            new ExceptionDialog(ex, "Fehler beim Öffnen der Metadaten [metadata.json]: " + ex.getLocalizedMessage());
        }
    }
    
    public static void saveAndOverwrite() throws IllegalStateException {
        saveAndOverwrite(null);
    }
    
    public static void saveAndOverwrite(final Label stateLabel) throws IllegalStateException {
        if(openFile == null) throw new IllegalStateException("Es wurde keine Datei öffnen. Benutze 'Speichern unter' um eine neue Datei anzulegen.");
        saveFile(openFile, stateLabel);
    }
    
    public static void saveFile(File f) {
        saveFile(f, null);
    }
    
    public static void saveFile(File f, final Label stateLabel) {
        ProgressDialog progressDialog = new ProgressDialog("Speichere " + f.getName() + "...");
        Task<Void> task = new Task<Void>() {
            
            @Override
            protected Void call() throws Exception {
                try {
                    updateProgress(0, 3);
                    ArrayList<String> entries = new ArrayList<>();
                    if(!f.exists()) {
                        f.createNewFile();
                    } else {
                        try (ZipFile zipFile = new ZipFile(f)) {
                            zipFile.stream().forEach(entry -> entries.add(entry.getName()));
                        }
                    }
                    updateProgress(1, 3);
                    try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(f))) {
                        writeMetadata(zos);
                        updateProgress(2, 3);
                        //writeMedia(zos, entries, MediaStore.getEntries());
                    }
                    openFile = f;
                    updateProgress(3, 3);
                } catch (IOException ex) {
                    new ExceptionDialog(ex, "Fehler beim Speichern des Savefiles: " + ex.getLocalizedMessage());
                }
                return null;
            }
            
        };
        progressDialog.activate(task);
        new Thread(task).start();
    }
    
    private static void writeMedia(ZipOutputStream zos, List<String> existing, List<String> toWrite) {
        toWrite.removeAll(existing);
        toWrite.forEach(externalFileName -> {
            try {
                ZipEntry entry = new ZipEntry("media\\" + externalFileName);
                zos.putNextEntry(entry);
                
                FileInputStream fis = new FileInputStream(MediaStore.getFile(externalFileName));
                
                byte[] buffer = new byte[BUFFER_SIZE];
                int length;
                while((length = fis.read(buffer)) > 0) zos.write(buffer, 0, length);
                
                zos.closeEntry();
            } catch (IOException ex) {
                LogHelper.error(ex);
            }
        });
    }
    
    private static void writeMetadata(ZipOutputStream zos) {
        try {
            ZipEntry entry = new ZipEntry("metadata.json");
            zos.putNextEntry(entry);
            
            JSONObject metadata = new JSONObject();
            
            //Fragen speichern
            JSONArray questions = new JSONArray();
            
            ObservableList<Question> list = Database.getQuestions();
            for (int i = 0; i < list.size(); i++) {
                questions.put(list.get(i).toJSONObject());
            }
            
            metadata.put("questions", questions);
            
            //MediaStore
            //metadata.put("fileCounter", MediaStore.getFileCounter());
            
            zos.write(metadata.toString().getBytes("UTF-8"));
            zos.closeEntry();
        } catch (IOException ex) {
            new ExceptionDialog(ex, "Fehler beim Speichern der Metadaten [metadata.json]: " + ex.getLocalizedMessage());
        }
    }
    
}
