package de.mafel.nawilauncher.data;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Rein statische Klasse zum Speichern von Einstellungen mithilfe der Java
 * Preferences.
 *
 * @author Felix
 */
public class Settings {
    
    private static Preferences prefs, settings, backup;
    
    static {
        try {
            prefs = Preferences.userNodeForPackage(Main.class);
            settings = prefs.node("settings");
            backup = prefs.node("backup");
        } catch(SecurityException ex) {
            new ExceptionDialog(ex, "Das Programm hat keine Berechtigungen, Einstellungen zu speichern. Dies hat zur Folge, dass Sie keine quizunabhängigen Programmeinstellungen dauerhaft ändern können. Versuchen Sie gegebenenfalls, das Programm mit Administratorrechten zu starten.");
        }
    }

    /**
     * Gibt die globalen Preferences zurück.
     *
     * @return
     */
    public static Preferences get() {
        return prefs;
    }
    
    /**
     * Gibt die Preferences zum Speichern von Programmeinstellungen zurück.
     *
     * @return
     */
    public static Preferences getSettings() {
        return settings;
    }
    
    /**
     * Gibt die Preferences zum Speichern des Spielstandbackups zurück.
     *
     * @return
     */
    public static Preferences getBackup() {
        return backup;
    }
    
    /**
     * Löscht den gesicherten Spielstand und gibt die Preferences zum Speichern
     * des Spielstandbackups zurück.
     *
     * @return
     */
    public static Preferences clearAndGetBackup() {
        try {
            for(String child : backup.childrenNames()) backup.node(child).removeNode();
        } catch (BackingStoreException ex) {
            LogHelper.error("Fehler beim Leeren der Backup Preferences (Children)", ex);
        }
        try {
            backup.clear();
        } catch (BackingStoreException ex) {
            LogHelper.error("Fehler beim Leeren der Backup Preferences (Clear)", ex);
        }
        return backup;
    }
    
}
