package de.mafel.nawilauncher.data;

/**
 * Klasse, die die eindeutige Zuordnung einer Frage anhand von Kategorie und Punktzahl beschreibt.
 * 
 * Zwei QuestionIdentifier werden als äquivalent angesehen, wenn sie die gleiche Kategorie
 * und Punktzahl speichern.
 *
 * @author Felix
 * @see Question Question
 * @see Category Category
 * @see QuestionIdentifier#QuestionIdentifier(de.mafel.nawilauncher.data.Category, int) QuestionIdentifier(Category, int)
 */
public class QuestionIdentifier {
    
    private final Category category;
    private final int score;

    /**
     * Erstellt einen neuen QuestionIdentifier mit den übergebenen Schlüsseleigenschaften.
     *
     * @param category Zugewiesene Kategorie
     * @param score Zugewiesene Punktzahl
     */
    public QuestionIdentifier(Category category, int score) {
        this.category = category;
        this.score = score;
    }

    /**
     * Gibt die Kategorie des Identifiers zurück.
     *
     * @return Kategorie
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Gibt die Punktzahl des Identifiers zurück.
     *
     * @return Punktzahl
     */
    public int getScore() {
        return score;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof QuestionIdentifier) {
            QuestionIdentifier other = (QuestionIdentifier) obj;
            return getScore() == other.getScore() && getCategory().equals(other.getCategory());
        } else return false;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public String toString() {
        return "[" + category.getId() + "," + score + "]";
    }
    
}
