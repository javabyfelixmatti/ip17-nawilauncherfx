package de.mafel.nawilauncher.data;

/**
 * Beschreibt den Zustand des Quiz.
 *
 * @author Felix, Matti
 */
public enum QuizState {

    /**
     * Das Quiz wurde noch nicht gestartet.
     *
     */
    NOT_STARTED,

    /**
     * Die Quizeinstellungen sind geöffnet; das Quiz wurde noch nicht gestartet.
     *
     */
    SETTINGS,

    /**
     * Die Fragenauswahl ist geöffnet; das Quiz wurde bereits gestartet; es ist
     * keine Frage aktiv.
     *
     */
    CATEGORIES,

    /**
     * Eine Frage ist geöffnet; das Quiz wurde bereits gestartet; es können
     * Antworten abgegeben werden.
     *
     */
    QUESTION,

    /**
     * Eine Frage wurde geöffnet; die Ergebnisse wurden bereits eingesammelt;
     * die Erklärung wird angezeigt.
     *
     */
    EXPLANATION,

    /**
     * Das Quiz wurde beendet; die Auswertung wird angezeigt.
     *
     */
    FINISHED;
}
