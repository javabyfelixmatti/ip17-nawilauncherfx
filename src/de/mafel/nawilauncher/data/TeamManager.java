package de.mafel.nawilauncher.data;

import de.mafel.nawilauncher.scenes.components.TeamLabel;
import java.util.Optional;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Rein statische Klasse, welche für die Verwaltung der Teams zuständig ist.
 *
 * @author Matti, Felix
 * @see Team Team
 */
public class TeamManager {
    
    private static final ObservableList<Team> teams = FXCollections.observableArrayList();
    private static int activeTeam = 0, activeForAllTeam = 0, round = 1;
    
    /*static {
        if (LogHelper.isDEBUG_MODE()) {
            TeamManager.addTeam(new Team("Team 1", "yellow"));
            TeamManager.addTeam(new Team("Team 2", "brown"));
        }
    }*/
    
    /**
     * Registriert das übergebene Team.
     * 
     * Achtung: Nicht thread-safe! --> siehe <code>addTeamLater()</code>
     *
     * @param team Neues Team
     * @see TeamManager#addTeamLater(de.mafel.nawilauncher.data.Team) TeamManager.addTeamLater(Team)
     */
    public static void addTeam(final Team team) {
        teams.add(team);
    }
    
    /**
     * Entfernt das übergebene Team.
     * 
     * Achtung: nicht thread-safe! --> siehe <code>removeTeamLater()</code>
     *
     * @param team Zu löschendes Team
     */
    public static void removeTeam(final Team team) {
        teams.remove(team);
    }
    
    /**
     * Registriert das übergebene Team im Hauptthread.
     * 
     * Achtung: Erfolgt der Aufruf nicht im Hauptthread, wird das Team nicht sofort
     * hinzugefügt und kann deshalb nicht sofort über den TeamManager angesprochen werden!
     *
     * @param team Neues Team
     * @see TeamManager#addTeam(de.mafel.nawilauncher.data.Team) TeamManager.addTeam()
     */
    public static void addTeamLater(final Team team) {
        Platform.runLater(() -> addTeam(team));
    }
    
    /**
     * Entfernt das übergebene Team im Hauptthread.
     * 
     * Achtung: Erfolgt der Aufruf nicht im Hauptthread, wird das Team nicht sofort
     * gelöscht und kann deshalb vorerst weiter über den TeamManager angesprochen werden!
     *
     * @param team Zu löschendes Team
     * @see TeamManager#addTeam(de.mafel.nawilauncher.data.Team) TeamManager.addTeam()
     */
    public static void removeTeamLater(final Team team) {
        Platform.runLater(() -> removeTeam(team));
    }
    
    /**
     * Ersetzt das am übergebenen Index registrierte Team durch das neu übergebene.
     *
     * Achtung: Im Hauptthread bzw. mit <code>Platform.runLater()</code> aufrufen!
     * 
     * @param team Neues Team
     * @param oldIndex Index
     * @see TeamManager#replaceTeam(de.mafel.nawilauncher.data.Team, de.mafel.nawilauncher.data.Team) TeamManager.replaceTeam(Team, Team)
     * @see Platform#runLater(java.lang.Runnable) Platform.runLater(Runnable)
     */
    public static void editTeam(final Team team, final int oldIndex) {
        teams.set(oldIndex, team);
    }
    
    /**
     * Ersetzt das übergebene alte Team durch das neue.
     *
     * Achtung: nicht thread-safe! --> siehe <code>replaceTeamLater()</code>
     *
     * @param oldTeam Altes Team
     * @param newTeam Neues Team
     * @see TeamManager#replaceTeamLater(de.mafel.nawilauncher.data.Team, de.mafel.nawilauncher.data.Team) TeamManager.replaceTeamLater(Team, Team)
     * @see TeamManager#editTeam(de.mafel.nawilauncher.data.Team, int) TeamManager.editTeam(Team, int)
     */
    public static void replaceTeam(final Team oldTeam, final Team newTeam) {
        teams.set(teams.indexOf(oldTeam), newTeam);
    }
    
    /**
     * Ersetzt das übergebene alte Team durch das neue im Hauptthread.
     *
     * Achtung: Erfolgt der Aufruf nicht im Hauptthread, wird das Team nicht sofort
     * ersetzt und kann deshalb vorerst weiter unverändert über den TeamManager
     * angesprochen werden!
     *
     * @param oldTeam Altes Team
     * @param newTeam Neues Team
     * @see TeamManager#replaceTeam(de.mafel.nawilauncher.data.Team, de.mafel.nawilauncher.data.Team) TeamManager.replaceTeam(Team, Team)
     * @see TeamManager#editTeam(de.mafel.nawilauncher.data.Team, int) TeamManager.editTeam(Team, int)
     */
    public static void replaceTeamLater(final Team oldTeam, final Team newTeam) {
        Platform.runLater(() -> replaceTeam(oldTeam, newTeam));
    }
    
    /**
     * Gibt eine Observable ArrayList mit allen registrierten Teams zurück.
     *
     * @return Liste aller Teams
     */
    public static ObservableList<Team> getTeams() {
        return teams;
    }
    
    /**
     * Gibt ein Optional zurück, welches das Team mit dem übergebenen Namen oder
     * nichts enthält, wenn kein Team mit diesem Namen gefunden wurde.
     *
     * @param name Gesuchter Name
     * @return Team mit übergebenem Namen oder nichts
     */
    public static Optional<Team> getTeam(String name) {
        return teams.parallelStream().filter(t -> t.getName().equals(name)).findAny();
    }
    
    /**
     * Gibt das Team mit der übergebenen ID oder null zurück, wenn kein Team mit
     * dieser ID gefunden wurde.
     *
     * @param id Gesuchte ID
     * @return Zugehöriges Team
     */
    public static Team getTeam(int id) {
        return teams.get(id);
    }
    
    /**
     * Gibt die ID >= 0 zurück, unter der das übergebene Team registriert ist oder
     * -1, falls das Team nicht registriert ist.
     *
     * @param team
     * @return
     */
    public static int getTeamId(Team team) {
        return teams.indexOf(team);
    }
    
    /**
     * Setzt die Antworten aller Teams zurück.
     *
     * @see Team#setAnswer(java.lang.Object) Team.setAnswer(Object)
     */
    public static void resetAnswers() {
        teams.forEach(t -> t.setAnswer(null));
    }
    
    /**
     * Gibt ein Array zurück, welches genau ein TeamLabel für jedes registrierte Team
     * enthält, jedoch nicht in der gleichen Reihenfolge, wie die Teams registriert
     * sind.
     *
     * @return Array mit allen TeamLabels
     */
    public static TeamLabel[] getAllTeamLabels() {
        return teams.parallelStream().map(Team::getLabel).toArray(TeamLabel[]::new);
    }
    
    /**
     * Gibt das aktuell aktive Team zurück.
     * 
     * @return aktives Team
     */
    public static Team getActiveTeam() {
        return activeTeam == teams.size() ? null : teams.get(activeTeam);
    }
    
    /**
     * Gibt die aktuell aktive Teamnummer zurück.
     * 
     * @return aktives Team als int
     */
    public static int getActiveTeamInt() {
        return activeTeam;
    }
    
    /**
     * Setzt das übergebene Team als aktiv.
     * 
     * Voraussetzung ist, dass das übergebene Team im TeamManager registriert
     * ist. (sonst keine Änderung)
     *
     * @param t Aktives Team
     */
    public static void setActiveTeam(Team t) {
        int i = teams.indexOf(t);
        if(i >= 0) activeTeam = i;
    }
    
    /**
     * Setzt das aktive Team anhand des übergebenen Index.
     *
     * @param t
     */
    public static void setActiveTeam(int t) {
        activeTeam = t;
    }

    /**
     * Setzt das Team, welches als nächstes die Frage für alle auswählt, anhand
     * des übergebenen Index.
     *
     * @param activeForAllTeam
     */
    public static void setActiveForAllTeam(int activeForAllTeam) {
        TeamManager.activeForAllTeam = activeForAllTeam;
    }
    
    /**
     * Setzt das Team, welches als nächstes die Frage für alle auswählt.
     * 
     * Voraussetzung ist, dass das übergebene Team im TeamManager registriert
     * ist. (sonst keine Änderung)
     *
     * @param t
     */
    public static void setActiveForAllTeam(Team t) {
        int i = teams.indexOf(t);
        if(i >= 0) activeForAllTeam = teams.indexOf(t);
    }

    /**
     * Gibt den Index des Teams zurück, welches als nächstes die Frage für alle
     * auswählt.
     *
     * @return Team-Index
     */
    public static int getActiveForAllTeamInt() {
        return activeForAllTeam;
    }
    
    /**
     * Gibt das Team zurück, welches als nächstes die Frage für alle auswählt.
     *
     * @return
     */
    public static Team getActiveForAllTeam() {
        return teams.get(activeForAllTeam);
    }
    
    /**
     * Zählt durch alle Teams durch bis es am Ende angekommen ist und beginnt
     * wieder am Anfang. In diesem Fall wird die nächste Runde eingestellt.
     */
    public static void setNextActiveTeam() {
        if (QuizData.getQuestionForAllHandling() == QuizData.QuestionForAllHandling.CHOOSE_AT_EVERY_TIME) {
            if (activeTeam == teams.size()-1) {
                activeTeam = 0;
                round++;
            } else {
                activeTeam++;
            }
        } else {
            activeTeam++;
            if(activeTeam > teams.size()) {
                round++;
                activeForAllTeam++;
                if(activeForAllTeam >= teams.size()) activeForAllTeam = 0;
                activeTeam = 0;
            }
        }
    }
    
    /**
     * Setzt die aktuelle Runde.
     *
     * @param r Aktuelle Runde
     */
    public static void setRound(int r) {
        round = r;
    }
    
    /**
     * Gibt die Nummer der aktuellen Runde zurück.
     *
     * @return Aktuelle Runde
     */
    public static int getRound() {
        return round;
    }
    
    /**
     * Führt die übergebene Aktion für alle Teams aus.
     *
     * @param action Auszuführende Aktion
     */
    public static void forEachTeam(Consumer<? super Team> action) {
        getTeams().forEach(action);
    }
    
}
