package de.mafel.nawilauncher.data;

/**
 * Repräsentiert das Ergebnis eines Teams, welches die gegebene Antwort und die 
 * daraus resultierende Punktzahl enthält.
 *
 * @author Felix
 * @see TeamResult#TeamResult(java.lang.String, int) TeamResult(String, int)
 */
public class TeamResult {
    
    private String answer;
    private int scoreDiff;
    private boolean correct;

    /**
     * Erstellt ein neues TeamResult, welches die übergebene Antwort und Punktzahl
     * enthält.
     *
     * @param answer Gegebene Antwort
     * @param scoreDiff Erhaltene Punktzahl
     * @param correct Ob Antwort korrekt
     */
    public TeamResult(String answer, int scoreDiff, boolean correct) {
        this.answer = answer;
        this.scoreDiff = scoreDiff;
        this.correct = correct;
    }

    /**
     * Die vom Team gegebene Antwort in Textform.
     *
     * @return Antwort
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Setzt die vom Team gegebene Antwort.
     *
     * @param answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     * Die durch die Antwort erreichte Punktzahländerung. (i.d.R. >= 0)
     *
     * @return Punktzahl
     */
    public int getScoreDiff() {
        return scoreDiff;
    }

    /**
     * Setzt die erreichte Punktzahländerung.
     *
     * @param scoreDiff
     */
    public void setScoreDiff(int scoreDiff) {
        this.scoreDiff = scoreDiff;
    }

    /**
     * Ob die gegebene Antwort richtig ist.
     * 
     * @return 
     */
    public boolean isCorrect() {
        return correct;
    }
    
    /**
     * Gibt "richtig" oder "falsch" zurück, je nachdem, ob die gegebene Antwort
     * korrekt ist oder nicht.
     *
     * @return
     */
    public String isCorrectString() {
        return correct ? "richtig" : "falsch";
    }

    /**
     * Legt fest, ob die gegebene Antwort richtig ist.
     *
     * @param correct
     */
    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
    
    /**
     * Gibt einen String zurück, der die erhaltene Punktzahl inklusive des Vorzeichens
     * (+-) enthält.
     *
     * @return String mit Punktzahl
     */
    public String getSignedScoreDiff() {
        return scoreDiff > 0 ? "+" + scoreDiff : (scoreDiff < 0 ? "-" + scoreDiff : "" + scoreDiff);
    }
    
}
