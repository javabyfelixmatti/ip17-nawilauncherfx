package de.mafel.nawilauncher.data.questions.estimatetolerance;

/**
 *
 * @author Felix
 */
public interface EstimateTolerance {

    public boolean isInTolerance(double val, double correct);
    
}
