package de.mafel.nawilauncher.data.questions.estimatetolerance;

/**
 *
 * @author Felix
 */
public class RelativeEstimateTolerance implements EstimateTolerance {
    
    private double tolerance;
    
    public RelativeEstimateTolerance(double tolerance) {
        this.tolerance = tolerance;
    }

    public double getTolerance() {
        return tolerance;
    }

    public void setTolerance(double tolerance) {
        this.tolerance = tolerance;
    }

    @Override
    public boolean isInTolerance(double val, double correct) {
        return val < correct * tolerance && val * tolerance > correct;
    }
    
}
