package de.mafel.nawilauncher.data.questions.estimatetolerance;

/**
 *
 * @author Felix
 */
public class AbsoluteEstimateTolerance implements EstimateTolerance {
    
    private double tolerance;
    
    public AbsoluteEstimateTolerance(double tolerance) {
        this.tolerance = tolerance;
    }

    public double getTolerance() {
        return tolerance;
    }

    public void setTolerance(double tolerance) {
        this.tolerance = tolerance;
    }

    @Override
    public boolean isInTolerance(double val, double correct) {
        return Math.abs(val - correct) <= tolerance;
    }
    
}
