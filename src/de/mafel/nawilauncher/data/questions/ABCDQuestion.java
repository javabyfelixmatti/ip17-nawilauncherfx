package de.mafel.nawilauncher.data.questions;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.TeamResult;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.net.WebMethod;
import de.mafel.nawilauncher.net.WebSocketServer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.util.Pair;
import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Ein Fragetyp, der mit genau vier Antwortmöglichkeiten (A,B,C,D) beantwortet
 * werden kann. Genau eine Antwortmöglichkeit ist richtig.
 *
 * @author Felix
 */
public class ABCDQuestion extends Question {
    
    /**
     * Konvertiert die übergebene Frage in eine ABCD-Frage. Dabei werden alle
     * Informationen behalten, die sich die Fragetypen teilen.
     *
     * @param q Zu konvertierende Frage
     * @return Entsprechung als ABCD-Frage
     */
    public static ABCDQuestion convert(Question q) {
        return new ABCDQuestion(q.getQuestion(), new String[4], 0, q.getCategory(), q.getScore(), q.getMedia(), q.getTimeLimit(), q.getEquipment());
    }
    
    /**
     * Gibt eine Platzhalter-Frage zurück.
     *
     * @return
     */
    public static ABCDQuestion placeholder() {
        return new ABCDQuestion("Frage", new String[] {"Antwort 1", "Antwort 2", "Antwort 3", "Antwort 4"}, 0, null, 0, null, -1, "keine");
    }
    
    private String[] answers;
    private int correctAnswer;

    /**
     * Erstellt eine neue ABCD-Frage.
     *
     * @param question Fragestellung
     * @param answers Genau vier Antwortmöglichkeiten
     * @param correctAnswer Index der richtigen Antwort (0-3)
     * @param category Kategorie
     * @param score Punktzahl
     * @param media Liste mit Medienobjekten
     * @param timeLimit Zeitvorgabe
     * @param equipment Benötigte Ausrüstung
     */
    public ABCDQuestion(String question, String[] answers, int correctAnswer, Category category, int score, ObservableList<MediaObject> media, int timeLimit, String equipment) {
        super(question, category, score, media, timeLimit, equipment);
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }
    
    /**
     * Liest eine ABCD-Frage aus einem JSONObject ein und ordnet sie einem
     * QuestionIdentifier zu.
     *
     * @param qi QuestionIdentifier
     * @param obj JSONObject
     * @throws JSONException Falsch formatiertes JSON
     */
    public ABCDQuestion(QuestionIdentifier qi, JSONObject obj) throws JSONException {
        super();
        Object[] answersJSON = obj.getJSONArray("answers").toList().toArray();
        answers = new String[4];
        try {
            for(int i = 0; i < answersJSON.length; i++) answers[i] = answersJSON[i].toString();
        } catch(ArrayIndexOutOfBoundsException ex) {
            new ExceptionDialog(ex, "Fehler beim Einlesen der Frage mit dem Identifier " +
                    qi.toString() + " [metadata.json]: Ungültige Antwortenanzahl (" + answersJSON.length +
                    " statt " + answers.length + "): " + ex.getLocalizedMessage());
        }
        super.setQuestion(obj.getString("question"));
        this.correctAnswer = obj.getInt("correct");
        super.setCategory(qi == null ? null : qi.getCategory());
        super.setScore(qi == null ? 0 : qi.getScore());
        super.setTimeLimit(obj.getInt("timelimit"));
        super.setExplanation(obj.getString("explanation"));
        super.setEquipment(obj.has("equipment") ? obj.getString("equipment") : "");
        JSONArray media = obj.getJSONArray("media");
        for(int i = 0; i < media.length(); i++) {
            String mediaExtension = media.getString(i).split("\\.")[1];
            if ("png.jpg.jpeg.gif.bmp".contains(mediaExtension)) {
                super.addMedia(new ImageObject(media.getString(i), "", false));
            } else if ("mp3.wav.aac.wma".contains(mediaExtension)) {
                super.addMedia(new SoundObject(media.getString(i), "", false));
            } else if ("mp4.mov.avi".contains(mediaExtension)) {
                super.addMedia(new VideoObject(media.getString(i), "", false));
            }
        }
    }
    
    @Override
    public String getCorrectAnswerString() {
        return (char) (65 + correctAnswer) + " - " + answers[correctAnswer];
    }

    /**
     * Gibt alle vier Antwortmöglichkeiten als Array zurück.
     *
     * @return
     */
    public String[] getAnswers() {
        return answers;
    }

    /**
     * Setzt alle vier Antwortmöglichkeiten.
     *
     * @param answers
     */
    public void setAnswers(String... answers) {
        this.answers = answers;
    }

    /**
     * Gibt den Index (0-3) der richtigen Antwort zurück.
     *
     * @return Index
     */
    public int getCorrectAnswer() {
        return correctAnswer;
    }
    
    /**
     * Gibt einen Buchstaben von 'A' bis 'D' zurück, der die richtige Antwort
     * beschreibt.
     *
     * @return Richtige Antwort als Buchstabe
     */
    public char getCorrectAnswerChar() {
        return (char) (65 + correctAnswer);
    }

    /**
     * Setzt die richtige Antwortmöglichkeit anhand des übergebenen Index von
     * 0-3.
     *
     * @param correctAnswer
     */
    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public Tab[] getEditQuestionTabs() {
        Pair<Tab, Tab> tabs = ABCDQuestionEditPane.apply(this);
        return new Tab[]{tabs.getKey(), tabs.getValue()};
    }
    

    @Override
    public String getSceneFile() {
        return "scenes/qtypes/ABCDQuestionScene.fxml";
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        obj.put("type", "abcd");
        obj.put("question", getQuestion());
        obj.put("timelimit", getTimeLimit());
        obj.put("explanation", getExplanation());
        JSONArray media = new JSONArray();
        getMedia().forEach(mediaObject -> media.put(mediaObject.getMediaPath()));
        obj.put("media", media);
        obj.put("answers", getAnswers());
        obj.put("correct", getCorrectAnswer());
        obj.put("equipment", getEquipment());
        return obj;
    }

    @Override
    public void sendToClient(WebSocket client) {
        JSONObject info = new JSONObject();
        info.put("q", getQuestion());
        info.put("a", getAnswers());
        if(getCategory().getType() && QuizData.getQuestionsForAllMode() ==QuizData.QuestionsForAllMode.MULTIPLE_ANSWERS_PER_TEAM && Navigator.isPlickersEnabled()) info.put("d", true); //Antworten abgeben deaktivieren wenn Plickers aktiviert ist
        client.send("abcd(" + info.toString() + ");");
    }

    @Override
    public void createResults() {
        QuizData.ScoreSystem scoreSystem = QuizData.getScoreSystem();
        LogHelper.debug("Aktives Scoresystem: " + scoreSystem);
        LogHelper.debug("QuestionsForAllMode: "+ QuizData.getQuestionsForAllMode());
        if(scoreSystem == QuizData.ScoreSystem.SIMPLE || (scoreSystem == QuizData.ScoreSystem.NAWIGATOR && Navigator.getActiveQuestion().getCategory().getType())) {
            if(scoreSystem == QuizData.ScoreSystem.SIMPLE || QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.ONE_ANSWER_PER_TEAM) TeamManager.forEachTeam(t -> {//Nur Teamantworten
                if(t.hasAnswered()) {
                    int answer = t.hasAnswered() ? (Integer) t.getAnswer() : -1;
                    Navigator.setResult(t, new TeamResult((char) (65 + answer) + "", answer == correctAnswer ? Navigator.getActiveQuestion().getScore() : 0, answer == correctAnswer));
                } else {
                    Navigator.setResult(t, new TeamResult("-", 0, false));
                }
            }); else {//Einbindung Publikum (Plickers ist hier ausgeschlossen)
                //Teams inkl. Publikum bewerten
                final HashMap<Team, Double> correctness = new HashMap<>();
                TeamManager.forEachTeam(t -> {
                    WebSocketServer wss = Main.getServer().getWebSocketServer();
                    int correct = 0, incorrect = 0;
                    if(t.hasAnswered() && t.getAnswer() instanceof Integer && (Integer) t.getAnswer() == correctAnswer) correct++; else incorrect++;//Teamantwort
                    //Einzelantworten
                    List<WebSocket> clients = wss.getIndividualClientsForTeam(t);
                    for(WebSocket c : clients)
                        if(wss.hasAnsweredIndividual(c) &&
                                wss.getIndividualAnswer(c) instanceof Integer &&
                                (Integer) wss.getIndividualAnswer(c) == correctAnswer)
                            correct++;
                        else incorrect++;
                    correctness.put(t, correct / (double) (correct + incorrect));
                });
                //Beste(s) Team(s) ermitteln
                double max = 0; final ArrayList<Team> maxt = new ArrayList<>();
                for(Team t : correctness.keySet()) {
                    if(correctness.get(t) > max) {
                        maxt.clear();
                        maxt.add(t);
                        max = correctness.get(t);
                    } else if(correctness.get(t) == max) maxt.add(t);
                }
                if(max == 0) maxt.clear();//Niemand bekommt Punkte, wenn alle Teams 0% haben
                //Ergebnisse bestimmen
                TeamManager.getTeams().stream().forEach(t -> {
                    Navigator.setResult(t, new TeamResult(Math.round(100 * correctness.get(t)) + "% richtig", maxt.contains(t) ? getScore() : 0, false));
                });
            }
        } else if(scoreSystem == QuizData.ScoreSystem.NAWIGATOR) {
            Team activeTeam = TeamManager.getActiveTeam();
            int activeTeamAnswer = activeTeam.hasAnswered() ? (Integer) activeTeam.getAnswer() : -1;
            boolean activeTeamAnswerCorrect = activeTeamAnswer == correctAnswer;
            Navigator.setResult(activeTeam, new TeamResult(activeTeamAnswer < 0 ? "-" : ((char) (65 + activeTeamAnswer) + ""), activeTeamAnswerCorrect ? Navigator.getActiveQuestion().getScore() : 0, activeTeamAnswerCorrect));
            TeamManager.getTeams()
                    .stream()
                    .filter(t -> !t.equals(activeTeam))
                    .forEach(t -> {
                        if(t.hasAnswered()) {
                            int answer = (Integer) t.getAnswer();
                            Navigator.setResult(t, new TeamResult((char) (65 + answer) + "", (!activeTeamAnswerCorrect) && answer == correctAnswer ? Navigator.getActiveQuestion().getScore() : 0, answer == correctAnswer));
                        } else {
                            Navigator.setResult(t, new TeamResult("-", 0, false));
                        }
                    });
        }
        TeamManager.setNextActiveTeam();
    }
    
}
