package de.mafel.nawilauncher.data.questions;

import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.media.MediaObject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Repräsentiert eine Frage.
 *
 * @author Felix
 * @see ABCDQuestion ABCD-Frage
 * @see EstimateQuestion Schätzfrage
 * @see SequenceQuestion Reihenfolge-Frage
 * @see TextQuestion Textfrage
 */
public abstract class Question {
    
    /**
     * Liest eine Frage beliebigen Typs aus einem JSONObject ein und ordnet sie
     * einem QuestionIdentifier zu.
     *
     * @param qi QuestionIdentifier
     * @param obj JSONObject
     * @return Eingelesene Frage
     * @throws JSONException Falsch formatiertes JSON
     */
    public static Question load(QuestionIdentifier qi, JSONObject obj) throws JSONException {
        switch(obj.getString("type")) {
            case "abcd": return new ABCDQuestion(qi, obj);
            case "text": return new TextQuestion(qi, obj);
            case "estimate": return  new EstimateQuestion(qi, obj);
            case "sequence": return new SequenceQuestion(qi, obj);
            default: return null;
        }
    }
    
    private String question, explanation, equipment;
    private ObservableList<MediaObject> media;
    private int timeLimit, score;
    private Category category;
    
    public Question() {
        this.question = "N/A";
        this.media = FXCollections.observableArrayList();
        this.timeLimit = -1;
        this.explanation = "";
        this.category = null;
        this.score = 0;
        this.equipment = "";
    }
    
    public Question(String question, Category category, int score, ObservableList<MediaObject> media, int timeLimit, String equipment) {
        this.question = question;
        this.timeLimit = timeLimit;
        this.explanation = "";
        this.equipment = equipment;
        this.category = category;
        this.score = score;
        this.media = media == null ? FXCollections.observableArrayList() : media;
    }
    
    /**
     * Erstellt neue Tabs mit den erforderlichen Nodes zum Editieren der Frage.
     * 
     * Bindet gleichzeitig die Eigenschaften der Frage an die eingegebenen Werte in den Textfeldern.
     *
     * @return Die erstellten Tabs
     */
    public abstract Tab[] getEditQuestionTabs();
    
    /**
     * Gibt die der Fragenklasse zugewiesene FXML-Datei zurück.
     *
     * @return Zugewiesene FXML-Datei
     */
    public abstract String getSceneFile();
    
    /**
     * Gibt die Eigenschaften der Frage als JSONObject zurück.
     *
     * @return JSONObject, das die Frage repräsentiert
     */
    public abstract JSONObject toJSONObject();
    
    /**
     * Sendet die Informationen an den übergebenen WebSocket Client, um die Frage in der Webform anzuzeigen.
     *
     * @param client Der WebSocket, an den die Informationen übertragen werden sollen
     */
    public abstract void sendToClient(WebSocket client);
    
    /**
     * Erzeugt die Ergebnisse in der Navigator-Klasse anhand der Team-Antworten.
     *
     */
    public abstract void createResults();
    
    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">

    /**
     * Gibt die richtige Antwort als String zurück.
     *
     * @return Richtige Antwort
    */
    public abstract String getCorrectAnswerString();
    
    public String getQuestion() {
        return question;
    }
    
    public void setQuestion(String question) {
        this.question = question;
    }
    
    public ObservableList<MediaObject> getMedia() {
        return media;
    }
    
    public void setMedia(ObservableList<MediaObject> media) {
        this.media = media;
    }
    
    /**
     * Fügt der Frage ein Medienobjekt hinzu.
     *
     * @param media
     */
    public void addMedia(MediaObject media) {
        this.media.add(media);
    }
    
    public int getTimeLimit() {
        return timeLimit;
    }
    
    /**
     * Gibt einen String mit dem Zeitlimit zurück.
     * 
     * Wenn kein Zeitlimit gesetzt ist, wird "-" zurückgegeben.
     *
     * @return
     */
    public String getTimeLimitString() {
        if (timeLimit < 1) return "-";
        return timeLimit + "s";
    }
    
    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }
    
    public String getExplanation() {
        return explanation;
    }
    
    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }
    
    public Category getCategory() {
        return category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }
    
    public int getScore() {
        return score;
    }
    
    public void setScore(int score) {
        this.score = score;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }
    
//</editor-fold>

    @Override
    public String toString() {
        return "[" + score + "] " + question;
    }
    
    /**
     * Gibt einen QuestionIdentifier, welcher die zugeordnete Kategorie und
     * Punktzahl der Frage enthält, zurück.
     *
     * @return
     */
    public QuestionIdentifier toIdentifier() {
        return new QuestionIdentifier(category, score);
    }
    
}
