package de.mafel.nawilauncher.data.questions;

import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.TeamResult;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.util.Pair;
import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TextQuestion extends Question {
    
    
    public TextQuestion(String question, String explanation, Category category, int score, ObservableList<MediaObject> media, int timeLimit, String equipment) {
        super(question, category, score, media, timeLimit, equipment);
        super.setExplanation(explanation);
    }

    public static TextQuestion convert(Question q) {
        return new TextQuestion(q.getQuestion(), q.getExplanation(), q.getCategory(), q.getScore(), q.getMedia(), q.getTimeLimit(), q.getEquipment());
    }
    
    public TextQuestion(QuestionIdentifier qi, JSONObject obj) throws JSONException {
        super();
        super.setQuestion(obj.getString("question"));
        super.setCategory(qi == null ? null : qi.getCategory());
        super.setScore(qi == null ? 0 : qi.getScore());
        super.setTimeLimit(obj.getInt("timelimit"));
        super.setExplanation(obj.getString("explanation"));
        super.setEquipment(obj.has("equipment") ? obj.getString("equipment") : "");
        JSONArray media = obj.getJSONArray("media");
        for(int i = 0; i < media.length(); i++) {
            String mediaExtension = media.getString(i).split("\\.")[1];
            if ("png.jpg.jpeg.gif.bmp".contains(mediaExtension)) {
                super.addMedia(new ImageObject(media.getString(i), "", false));
            } else if ("mp3.wav.aac.wma".contains(mediaExtension)) {
                super.addMedia(new SoundObject(media.getString(i), "", false));
            } else if ("mp4.mov.avi".contains(mediaExtension)) {
                super.addMedia(new VideoObject(media.getString(i), "", false));
            }
        }
    }
    
    @Override
    public Tab[] getEditQuestionTabs() {
        Pair<Tab, Tab> tabs = TextQuestionEditPane.apply(this);
        return new Tab[]{tabs.getKey(), tabs.getValue()};
    }

    @Override
    public String getSceneFile() {
        return "scenes/qtypes/TextQuestionScene.fxml";
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        obj.put("type", "text");
        obj.put("question", getQuestion());
        obj.put("timelimit", getTimeLimit());
        obj.put("explanation", getExplanation());
        obj.put("equipment", getEquipment());
        JSONArray media = new JSONArray();
        getMedia().forEach(mediaObject -> media.put(mediaObject.getMediaPath()));
        obj.put("media", media);
        return obj;
    }

    @Override
    public String getCorrectAnswerString() {
        return getExplanation();
    }

    @Override
    public void sendToClient(WebSocket client) {
        JSONObject info = new JSONObject();
        info.put("q", getQuestion());
        client.send("txtq(" + info.toString() + ");");
    }

    @Override
    public void createResults() {
        TeamManager.getTeams().forEach(t -> {
            try {
                if(t.hasAnswered()) {
                    Navigator.setResult(t, new TeamResult(t.getAnswer().toString(), 0, false));
                } else {
                    Navigator.setResult(t, new TeamResult("-", 0, false));
                }
            } catch(Exception ex) {
                Navigator.setResult(t, new TeamResult("-", 0, false));
            }
        });
    }
}
