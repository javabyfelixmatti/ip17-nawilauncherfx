package de.mafel.nawilauncher.data.questions;

import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.TeamResult;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.data.questions.estimatetolerance.EstimateTolerance;
import de.mafel.nawilauncher.data.questions.estimatetolerance.RelativeEstimateTolerance;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.util.Pair;
import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Eine Schätzfrage, die mit einer Gleitkommazahl beantwortet werden kann. Alle
 * Antworten, die mit einer Toleranz der richtigen Antwort entsprechen, gelten
 * als richtig.
 *
 * @author Felix
 */
public class EstimateQuestion extends Question {
    
    private double correctAnswer;
    private String unit;
    private EstimateTolerance tolerance;
    
    /**
     * Konvertiert die übergebene Frage in eine Schätzfrage. Dabei werden alle
     * Informationen behalten, die sich die Fragetypen teilen.
     *
     * @param q Zu konvertierende Frage
     * @return Entsprechung als Schätzfrage
     */
    public static EstimateQuestion convert(Question q) {
        return new EstimateQuestion(q.getQuestion(), 0, "", q.getCategory(), q.getScore(), q.getMedia(), q.getTimeLimit(), q.getEquipment());
    }

    /**
     * Erstellt eine neue Schätzfrage.
     *
     * @param question Fragestellung
     * @param correctAnswer Richtige Antwort
     * @param unit Einheit
     * @param category Kategorie
     * @param score Punktzahl
     * @param media Liste mit Medienobjekten
     * @param timeLimit Zeitvorgabe
     * @param equipment Benötigte Ausrüstung
     */
    public EstimateQuestion(String question, double correctAnswer, String unit, Category category, int score, ObservableList<MediaObject> media, int timeLimit, String equipment) {
        super(question, category, score, media, timeLimit, equipment);
        this.correctAnswer = correctAnswer;
        this.unit = unit;
        this.tolerance = new RelativeEstimateTolerance(1.25);
    }

    /**
     * Liest eine Schätzfrage aus einem JSONObject ein und ordnet sie einem
     * QuestionIdentifier zu.
     *
     * @param qi QuestionIdentifier
     * @param obj JSONObject
     * @throws JSONException Falsch formatiertes JSON
     */
    public EstimateQuestion(QuestionIdentifier qi, JSONObject obj) throws JSONException {
        super();
        super.setQuestion(obj.getString("question"));
        this.correctAnswer = obj.getDouble("correct");
        this.unit = obj.getString("unit");
        super.setCategory(qi == null ? null : qi.getCategory());
        super.setScore(qi == null ? 0 : qi.getScore());
        super.setTimeLimit(obj.getInt("timelimit"));
        super.setExplanation(obj.getString("explanation"));
        super.setEquipment(obj.has("equipment") ? obj.getString("equipment") : "");
        JSONArray media = obj.getJSONArray("media");
        for(int i = 0; i < media.length(); i++) {
            String mediaExtension = media.getString(i).split("\\.")[1];
            if ("png.jpg.jpeg.gif.bmp".contains(mediaExtension)) {
                super.addMedia(new ImageObject(media.getString(i), "", false));
            } else if ("mp3.wav.aac.wma".contains(mediaExtension)) {
                super.addMedia(new SoundObject(media.getString(i), "", false));
            } else if ("mp4.mov.avi".contains(mediaExtension)) {
                super.addMedia(new VideoObject(media.getString(i), "", false));
            }
        }
        this.tolerance = new RelativeEstimateTolerance(1.25);
    }
    
    @Override
    public Tab[] getEditQuestionTabs() {
        Pair<Tab, Tab> tabs = EstimateQuestionEditPane.apply(this);
        return new Tab[]{tabs.getKey(), tabs.getValue()};
    }

    @Override
    public String getSceneFile() {
        return "scenes/qtypes/EstimateQuestionScene.fxml";
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        obj.put("type", "estimate");
        obj.put("question", getQuestion());
        obj.put("correct", getCorrectAnswer());
        obj.put("unit", getUnit());
        obj.put("timelimit", getTimeLimit());
        obj.put("explanation", getExplanation());
        obj.put("equipment", getEquipment());
        JSONArray media = new JSONArray();
        getMedia().forEach(mediaObject -> media.put(mediaObject.getMediaPath()));
        obj.put("media", media);
        return obj;
    }

    /**
     * Die richtige Antwort (Gleitkommazahl).
     *
     * @return
     */
    public double getCorrectAnswer() {
        return correctAnswer;
    }

    @Override
    public String getCorrectAnswerString() {
        return correctAnswer + " " + unit;
    }
    
    /**
     * Setzt die richtige Antwort (Gleitkommazahl).
     *
     * @param correctAnswer
     */
    public void setCorrectAnswer(double correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    /**
     * Die Einheit der Schätzung.
     *
     * @return
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Setzt die Einheit der Schätzung.
     *
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public void sendToClient(WebSocket client) {
        JSONObject info = new JSONObject();
        info.put("q", getQuestion());
        info.put("u", getUnit());
        client.send("estq(" + info.toString() + ");");
    }

    @Override
    public void createResults() {
        QuizData.ScoreSystem scoreSystem = QuizData.getScoreSystem();
        if(scoreSystem == QuizData.ScoreSystem.SIMPLE || (scoreSystem == QuizData.ScoreSystem.NAWIGATOR && Navigator.getActiveQuestion().getCategory().getType() && QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.ONE_ANSWER_PER_TEAM)) {
            TeamManager.forEachTeam(t -> {
                if(t.hasAnswered()) {
                    double answer = (Double) t.getAnswer();
                    Navigator.setResult(t, new TeamResult(answer + getUnit(), !Double.isNaN(answer) && tolerance.isInTolerance(answer, getCorrectAnswer()) ? Navigator.getActiveQuestion().getScore() : 0, tolerance.isInTolerance(answer, getCorrectAnswer())));
                } else {
                    Navigator.setResult(t, new TeamResult("-", 0, false));
                }
            });
        } else if(scoreSystem == QuizData.ScoreSystem.NAWIGATOR) {
            Team activeTeam = TeamManager.getActiveTeam();
            double activeTeamAnswer = activeTeam.hasAnswered() ? (Double) activeTeam.getAnswer() : Double.NaN;
            boolean activeTeamAnswerCorrect = !Double.isNaN(activeTeamAnswer) && tolerance.isInTolerance(activeTeamAnswer, getCorrectAnswer());
            Navigator.setResult(activeTeam, new TeamResult(activeTeamAnswer + " " + getUnit(), activeTeamAnswerCorrect ? Navigator.getActiveQuestion().getScore() : 0, tolerance.isInTolerance(activeTeamAnswer, getCorrectAnswer())));
            TeamManager.forEachTeam(t -> {
                if(t.hasAnswered()) {
                    double answer = (Double) t.getAnswer();
                    Navigator.setResult(t, new TeamResult(answer + " " + getUnit(), !Double.isNaN(answer) && tolerance.isInTolerance(answer, getCorrectAnswer()) ? Navigator.getActiveQuestion().getScore() : 0, tolerance.isInTolerance(answer, getCorrectAnswer())));
                } else {
                    Navigator.setResult(t, new TeamResult("-", 0, false));
                }
            });
        }
        TeamManager.setNextActiveTeam();
    }
}
