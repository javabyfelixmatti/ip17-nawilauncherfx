package de.mafel.nawilauncher.data.questions;

import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import de.mafel.nawilauncher.data.Category;
import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.data.Navigator;
import de.mafel.nawilauncher.data.QuestionIdentifier;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.data.Team;
import de.mafel.nawilauncher.data.TeamManager;
import de.mafel.nawilauncher.data.TeamResult;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.scenes.components.GUtils;
import java.util.Arrays;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.util.Pair;
import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SequenceQuestion extends Question {
    
    public static SequenceQuestion convert(Question q) {
        if (q instanceof ABCDQuestion) {
            ABCDQuestion qabcd = (ABCDQuestion) q;
            return new SequenceQuestion(qabcd.getQuestion(), qabcd.getAnswers(), qabcd.getExplanation(), qabcd.getCategory(), qabcd.getScore(), q.getMedia(), q.getTimeLimit(), q.getEquipment());
        } else {
            return new SequenceQuestion(q.getQuestion(), q.getExplanation(), q.getCategory(), q.getScore(), q.getMedia(), q.getTimeLimit(), q.getEquipment());
        }
    }
    
    public static String answersToString(String[] answers) {
        String out = answers[0];
        for(int i = 1; i < answers.length; i++) out += " - " + answers[i];
        return out;
    }
    
    String[] answers = {"Antwort 1", "Antwort 2", "Antwort 3"};
    
    public SequenceQuestion(String question, String explanation, Category category, int score, ObservableList<MediaObject> media, int timeLimit, String equipment) {
        super(question, category, score, media, timeLimit, equipment);
        super.setExplanation(explanation);
    }

    public SequenceQuestion(String question, String[] answers, String explanation, Category category, int score, ObservableList<MediaObject> media, int timeLimit, String equipment) {
        super(question, category, score, media, timeLimit, equipment);
        super.setExplanation(explanation);
        setAnswers(answers);
    }
    
    public SequenceQuestion(QuestionIdentifier qi, JSONObject obj) throws JSONException {
        super();
        Object[] answersJSON = obj.getJSONArray("answers").toList().toArray();
        answers = new String[obj.getInt("sequenceLength")];
        try {
            for(int i = 0; i < answersJSON.length; i++) answers[i] = answersJSON[i].toString();
        } catch(ArrayIndexOutOfBoundsException ex) {
            new ExceptionDialog(ex, "Fehler beim Einlesen der Frage mit dem Identifier " +
                    qi.toString() + " [metadata.json]: Ungültige Antwortenanzahl (" + answersJSON.length +
                    " statt " + answers.length + "): " + ex.getLocalizedMessage());
        }
        super.setQuestion(obj.getString("question"));
        super.setCategory(qi == null ? null : qi.getCategory());
        super.setScore(qi == null ? 0 : qi.getScore());
        super.setTimeLimit(obj.getInt("timelimit"));
        super.setExplanation(obj.getString("explanation"));
        super.setEquipment(obj.has("equipment") ? obj.getString("equipment") : "");
        JSONArray media = obj.getJSONArray("media");
        for(int i = 0; i < media.length(); i++) {
            String mediaExtension = media.getString(i).split("\\.")[1];
            if ("png.jpg.jpeg.gif.bmp".contains(mediaExtension)) {
                super.addMedia(new ImageObject(media.getString(i), "", false));
            } else if ("mp3.wav.aac.wma".contains(mediaExtension)) {
                super.addMedia(new SoundObject(media.getString(i), "", false));
            } else if ("mp4.mov.avi".contains(mediaExtension)) {
                super.addMedia(new VideoObject(media.getString(i), "", false));
            }
        }
    }
    
    @Override
    public Tab[] getEditQuestionTabs() {
        Pair<Tab, Tab> tabs = SequenceQuestionEditPane.apply(this);
        return new Tab[]{tabs.getKey(), tabs.getValue()};
    }

    @Override
    public String getSceneFile() {
        return "scenes/qtypes/SequenceQuestionScene.fxml";
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject obj = new JSONObject();
        obj.put("type", "sequence");
        obj.put("question", getQuestion());
        obj.put("sequenceLength", getAnswers().length);
        obj.put("answers", getAnswers());
        obj.put("timelimit", getTimeLimit());
        obj.put("explanation", getExplanation());
        obj.put("equipment", getEquipment());
        JSONArray media = new JSONArray();
        getMedia().forEach(mediaObject -> media.put(mediaObject.getMediaPath()));
        obj.put("media", media);
        return obj;
    }

    @Override
    public String getCorrectAnswerString() {
        String output = answers[0];
        for(int i = 1; i < answers.length; i++) output += " - " + answers[i];
        return output;
    }

    public String[] getAnswers() {
        return answers;
    }

    public final void setAnswers(String[] answers) {
        this.answers = answers;
    }

    @Override
    public void sendToClient(WebSocket client) {
        JSONObject info = new JSONObject();
        info.put("q", getQuestion());
        info.put("a", GUtils.shuffleArray(getAnswers()));
        client.send("seqq(" + info.toString() + ");");
    }

    @Override
    public void createResults() {
        QuizData.ScoreSystem scoreSystem = QuizData.getScoreSystem();
        if(scoreSystem == QuizData.ScoreSystem.SIMPLE || (scoreSystem == QuizData.ScoreSystem.NAWIGATOR && Navigator.getActiveQuestion().getCategory().getType() && QuizData.getQuestionsForAllMode() == QuizData.QuestionsForAllMode.ONE_ANSWER_PER_TEAM)) {
            TeamManager.forEachTeam(t -> {
                if(t.hasAnswered() && t.getAnswer() instanceof String[]) {
                    boolean correct = Arrays.equals(((String[]) t.getAnswer()), getAnswers());
                    Navigator.setResult(t, new TeamResult(SequenceQuestion.answersToString((String[]) t.getAnswer()), correct ? getScore() : 0, correct));
                } else Navigator.setResult(t, new TeamResult("-", 0, false));
            });
        } else if(scoreSystem == QuizData.ScoreSystem.NAWIGATOR) {
            Team activeTeam = TeamManager.getActiveTeam();
            boolean activeTeamAnswered = activeTeam.hasAnswered() && activeTeam.getAnswer() instanceof String[],
                    activeTeamCorrect = activeTeamAnswered && Arrays.equals(((String[]) activeTeam.getAnswer()), getAnswers());
            if(activeTeamCorrect) {
                TeamManager.forEachTeam(t -> {
                    if(t.equals(activeTeam)) {
                        //Volle Punktzahl
                        Navigator.setResult(t, new TeamResult(SequenceQuestion.answersToString((String[]) t.getAnswer()), getScore(), Arrays.equals(((String[]) t.getAnswer()), getAnswers())));
                    } else {
                        //Keine Punkte, da aktives Team richtig geantwortet hat
                        boolean teamAnswered = t.hasAnswered() && t.getAnswer() instanceof String[];
                        Navigator.setResult(t, new TeamResult(teamAnswered ? SequenceQuestion.answersToString((String[]) t.getAnswer()) : "-", 0, Arrays.equals(((String[]) t.getAnswer()), getAnswers())));
                    }
                });
            } else {
                //Für jedes Team einzeln überprüfen, ob richtig geantwortet (wie in SIMPLE)
                TeamManager.forEachTeam(t -> {
                    if(t.hasAnswered() && t.getAnswer() instanceof String[]) {
                        boolean correct = Arrays.equals(((String[]) t.getAnswer()), getAnswers());
                        Navigator.setResult(t, new TeamResult(SequenceQuestion.answersToString((String[]) t.getAnswer()), correct ? getScore() : 0, correct));
                    } else Navigator.setResult(t, new TeamResult("-", 0, false));
                });
            }
        }
        TeamManager.setNextActiveTeam();
    }
    
    
}