package de.mafel.nawilauncher.data.questions;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.QuizData;
import de.mafel.nawilauncher.media.ImageObject;
import de.mafel.nawilauncher.media.MediaObject;
import de.mafel.nawilauncher.media.SoundObject;
import de.mafel.nawilauncher.media.VideoObject;
import de.mafel.nawilauncher.fileio.MediaStore;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.util.Pair;

/**
 *
 * @author matti
 */
public class TextQuestionEditPane {
    
    private static Tab questionTab, answerTab;
    
    private static SimpleObjectProperty<TextQuestion> boundQuestion = new SimpleObjectProperty<>();
    private static boolean initiated = false;
    
    private static ListView<MediaObject> mediaList = null, answerMediaList = null;
    private static FileChooser fileChooser = new FileChooser();
    private static SplitMenuButton btnAddMedia;
    
    private static void init() {
        initiated = true;
        questionTab = new Tab("Frage");
        answerTab = new Tab("Antwort");
        AnchorPane ap = new AnchorPane();
        ap.setPrefHeight(378);
        ap.setPrefWidth(838);
        ap.getStyleClass().add("editQuestionsPanel");
        
            VBox vb1 = new VBox(10);
            AnchorPane.setLeftAnchor(vb1, 20.0);
            AnchorPane.setRightAnchor(vb1, 20.0);
            AnchorPane.setTopAnchor(vb1, 21.0);
            AnchorPane.setBottomAnchor(vb1, 20.0);
        
            TextArea taQuestion = new TextArea();
            taQuestion.setWrapText(true);
            taQuestion.setLayoutX(38);
            taQuestion.setLayoutY(21);
            taQuestion.prefHeightProperty().bind(vb1.heightProperty().divide(6));
            taQuestion.setPrefWidth(793);
            taQuestion.setPromptText("Frage");
            
            taQuestion.setStyle("-fx-font-size: 22.0");
            boundQuestionProperty().addListener((observable, oldValue, newValue) -> taQuestion.setText(getBoundQuestion().getQuestion()));
            taQuestion.textProperty().addListener((observable, oldValue, newValue) -> getBoundQuestion().setQuestion(newValue));
            
            VBox vb = new VBox();
            vb.setAlignment(Pos.CENTER);
            vb.setLayoutX(30D);
            vb.setLayoutY(206D);
            vb.setPrefHeight(260);
            vb.setPrefWidth(397);
            
                Label medien = new Label("Medien");
                medien.setAlignment(Pos.CENTER);
                medien.setStyle("-fx-font-size: 15.0");
            
                mediaList = new ListView();
                mediaList.setPrefHeight(200D);
                mediaList.setPrefWidth(200D);
                boundQuestionProperty().addListener((observable, oldValue, newValue) -> mediaList.setItems(getBoundQuestion().getMedia()));
                mediaList.setCellFactory(param -> new ListCell<MediaObject>() {
                    
                    @Override
                    protected void updateItem(MediaObject item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item != null && item.getMediaPath() != null) {
                            ImageView imageView = null;
                            if (item instanceof ImageObject) {
                                imageView = new ImageView(((ImageObject) item).getImage());
                            } else if (item instanceof SoundObject) {
                                imageView = new ImageView(new Image("/de/mafel/nawilauncher/res/audio.png", true));
                            } else if (item instanceof VideoObject) {
                                imageView = new ImageView(new Image("/de/mafel/nawilauncher/res/playMA.png", true));
                            }
                            setText(QuizData.getMediaName(item.getMediaPath()));
                            imageView.setPreserveRatio(true);
                            imageView.setFitHeight(80);
                            setGraphic(imageView);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }
                    
                });
                mediaList.setOnMouseClicked((MouseEvent mouseEvent) -> {
                    if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                        if(mouseEvent.getClickCount() == 2){
                            TextInputDialog dialog = new TextInputDialog(QuizData.getMediaName(mediaList.getSelectionModel().getSelectedItem().getMediaPath()));
                            dialog.setTitle("Namen ändern");
                            dialog.setHeaderText("Hier können Sie den Namen ändern");
                            dialog.setContentText("Bitte geben Sie den neuen Namen ein:");
                            Optional<String> result = dialog.showAndWait();
                            result.ifPresent(name -> {
                                QuizData.setMediaName(mediaList.getSelectionModel().getSelectedItem().getMediaPath(), name);
                                updateBtnAddMedia();
                                mediaList.refresh();
                            });
                        }
                    }
                });
                
                
                ToolBar tb = new ToolBar();
                tb.setPrefHeight(35D);
                tb.setPrefWidth(397D);
                
                    btnAddMedia = new SplitMenuButton();
                    btnAddMedia.setText("Hinzufügen");
                    btnAddMedia.setMnemonicParsing(false);
                    btnAddMedia.setOnAction(event -> handleBtnAddMediaAction(event));
                    btnAddMedia.setPrefHeight(25D);
                    btnAddMedia.setPrefWidth(169D);
                    btnAddMedia.getStyleClass().add("dataBtn");
                    
                    Button btnRemoveMedia = new Button("Entfernen");
                    btnRemoveMedia.setMnemonicParsing(false);
                    btnRemoveMedia.setOnAction(event -> handleBtnRemoveMediaAction(event));
                    btnRemoveMedia.setPrefHeight(25D);
                    btnRemoveMedia.setPrefWidth(128D);
                    btnRemoveMedia.getStyleClass().add("dataBtn");
                    btnRemoveMedia.setAlignment(Pos.CENTER);
                
                tb.getItems().addAll(btnAddMedia, btnRemoveMedia);
        
            vb.getChildren().addAll(medien, mediaList, tb);
            VBox.setVgrow(mediaList, Priority.ALWAYS);
            
            VBox vbTimer = new VBox();
            vbTimer.setAlignment(Pos.TOP_LEFT);
            
                Label lblTime = new Label("Zeit zum Beantworten (in s):");
                lblTime.setPadding(new Insets(5));
                lblTime.setStyle("-fx-font-size: 15px");
                
                CheckBox cbIsTimeLimit = new CheckBox("Timelimit");
                cbIsTimeLimit.setPadding(new Insets(5, 0, 5, 0));
                boundQuestionProperty().addListener((observable, oldValue, newValue) ->  cbIsTimeLimit.setSelected(getBoundQuestion().getTimeLimit() > 0));
                cbIsTimeLimit.setOnAction(e -> getBoundQuestion().setTimeLimit(cbIsTimeLimit.isSelected() ? 60 : -1));
                
                TextField tfTime = new TextField();
                boundQuestionProperty().addListener((observable, oldValue, newValue) -> tfTime.setText(newValue.getTimeLimit() + ""));
                tfTime.disableProperty().bind(cbIsTimeLimit.selectedProperty().not());
                tfTime.textProperty().addListener((observable, oldValue, newValue) -> {
                            try {
                                getBoundQuestion().setTimeLimit(Integer.parseInt(tfTime.getText()));
                            } catch(NumberFormatException ex) {
                                getBoundQuestion().setTimeLimit(0);
                            }
                });
                
                Label lblEquipment = new Label("Benötigte Hilfsmittel:");
                lblEquipment.setPadding(new Insets(5, 0, 5, 0));
                lblEquipment.setStyle("-fx-font-size: 15px");
                TextArea taEquipment = new TextArea();
                taEquipment.textProperty().addListener((observable, oldValue, newValue) -> getBoundQuestion().setEquipment(taEquipment.getText()));
                boundQuestionProperty().addListener((observable, oldValue, newValue) -> taEquipment.setText(newValue.getEquipment()));
                
            vbTimer.getChildren().addAll(lblTime, cbIsTimeLimit, tfTime, lblEquipment, taEquipment);
            
        
        HBox hbox = new HBox(20);
        hbox.getChildren().addAll(vb, vbTimer);
        HBox.setHgrow(vb, Priority.ALWAYS);
        HBox.setHgrow(vbTimer, Priority.ALWAYS);
        
        VBox.setVgrow(taQuestion, Priority.SOMETIMES);
        VBox.setVgrow(hbox, Priority.ALWAYS);
        vb1.setAlignment(Pos.CENTER);
        vb1.getChildren().addAll(taQuestion, hbox);
        ap.getChildren().addAll(vb1);
        
        questionTab.setContent(ap);
        
        
        //AnswerTab
        Label lblQuestionAtAnswerTab = new Label("- keine Frage eingegeben -");
        lblQuestionAtAnswerTab.setAlignment(Pos.CENTER);
        lblQuestionAtAnswerTab.setTextAlignment(TextAlignment.CENTER);
        AnchorPane.setLeftAnchor(lblQuestionAtAnswerTab, 23D);
        AnchorPane.setRightAnchor(lblQuestionAtAnswerTab, 23D);
        AnchorPane.setTopAnchor(lblQuestionAtAnswerTab, 14D);
        lblQuestionAtAnswerTab.setStyle("-fx-font-size: 22");
        lblQuestionAtAnswerTab.textProperty().bind(taQuestion.textProperty());
        
        VBox vb2 = new VBox();
        vb2.setLayoutY(62D);
        vb2.setPrefHeight(415D);
        AnchorPane.setBottomAnchor(vb2, 10.0);
        AnchorPane.setLeftAnchor(vb2, 10.0);
        AnchorPane.setRightAnchor(vb2, 10.0);
        AnchorPane.setTopAnchor(vb2, 14.0);

            HBox hb = new HBox();
            hb.setPrefHeight(390D);
            hb.setPrefWidth(653D);
            
                VBox vb3 = new VBox();
                vb3.setPrefHeight(17D);
                vb3.setPrefWidth(653D);
                TextArea taExplanation = new TextArea();
                taExplanation.setWrapText(true);
                VBox.setMargin(taExplanation, new Insets(5));

                    Label lblExplanation = new Label("Erklärung");
                    lblExplanation.setPrefHeight(17D);
                    lblExplanation.setPrefWidth(96D);
                    lblExplanation.setStyle("-fx-font-size: 15");
                    lblExplanation.setPadding(new Insets(0, 0, 0, 5));

                    taExplanation.setPrefHeight(316D);
                    taExplanation.setPrefWidth(867D);
                    taExplanation.setStyle("-fx-font-size: 15");
                    boundQuestionProperty().addListener((observable, oldValue, newValue) -> taExplanation.setText(getBoundQuestion().getExplanation()));
                    taExplanation.textProperty().addListener((observable, oldValue, newValue) -> getBoundQuestion().setExplanation(newValue));

                vb3.getChildren().addAll(lblExplanation, taExplanation);
                VBox.setVgrow(taExplanation, Priority.ALWAYS);
                
                VBox vb4 = new VBox();
                vb4.setAlignment(Pos.CENTER);
                vb4.setPrefHeight(260D);
                vb4.setPrefWidth(397D);
                
                    Label lblMedia = new Label("Medien");
                    lblMedia.setStyle("-fx-font-size: 15");
                    
                    answerMediaList = new ListView<>();
                    answerMediaList.setPrefWidth(331D);
                    answerMediaList.setPrefHeight(316D);
                    //TODO Answer Media implementieren
                    
                    ToolBar tob = new ToolBar();
                        Button btnAnswerAddMedia = new Button("Hinzufügen");
                        btnAnswerAddMedia.setMnemonicParsing(false);
                        btnAnswerAddMedia.setPrefHeight(25D);
                        btnAnswerAddMedia.setPrefWidth(137D);
                        btnAnswerAddMedia.getStyleClass().add("dataBtn");
                        btnAddMedia.setOnAction(event -> handleBtnAddMediaAction(event));

                        Button btnAnswerRemoveMedia = new Button("Entfernen");
                        btnAnswerRemoveMedia.setMnemonicParsing(false);
                        btnAnswerRemoveMedia.setAlignment(Pos.CENTER);
                        btnAnswerRemoveMedia.setPrefHeight(25D);
                        btnAnswerRemoveMedia.setPrefWidth(122D);
                        btnAnswerRemoveMedia.getStyleClass().add("dataBtn");
                    
                    tob.getItems().addAll(btnAnswerAddMedia, btnAnswerRemoveMedia);
                
                vb4.setDisable(true);
                vb4.getChildren().addAll(lblMedia, answerMediaList, tob);
                VBox.setVgrow(answerMediaList, Priority.ALWAYS);

            HBox.setMargin(vb3, new Insets(0, 0, 0, 5));
            HBox.setMargin(vb4, new Insets(0, 5, 0, 0));
            hb.getChildren().addAll(vb3, vb4);
            HBox.setHgrow(vb3, Priority.ALWAYS);
            HBox.setHgrow(vb4, Priority.ALWAYS);
        
            vb2.getChildren().addAll(lblQuestionAtAnswerTab, hb);
            VBox.setVgrow(hb, Priority.ALWAYS);
            
        AnchorPane aap = new AnchorPane(vb2);
        aap.setMinHeight(0);
        aap.setMinWidth(0);
        aap.setPrefHeight(180);
        aap.setPrefWidth(200);
        aap.getStyleClass().add("editQuestionsPanel");
        answerTab.setContent(aap);
    }
    
    public static Pair<Tab, Tab> apply(TextQuestion q) {
        if (!initiated) init();
        boundQuestion.setValue(q);
        updateBtnAddMedia();
        return new Pair<>(questionTab, answerTab);
    }
    
    private static void handleBtnAddMediaAction(ActionEvent e) {
        fileChooser.setTitle("Mediendatei auswählen");
        setupFileChooser(MediaObject.SUPPORTED_MEDIA_FILES_FILTERS);
        File mediaFile = fileChooser.showOpenDialog(Main.getMainScene().getWindow());
        if(mediaFile != null) {
            if ("png.jpg.jpeg.gif.bmp".contains(mediaFile.getName().split("\\.")[1].toLowerCase())) {
                getBoundQuestion().addMedia(new ImageObject(MediaStore.put(mediaFile, mediaFile.getName()), mediaFile.getName(), false));
            } else if ("mp3.wav.aac.wma".contains(mediaFile.getName().split("\\.")[1].toLowerCase())) {
                getBoundQuestion().addMedia(new SoundObject(MediaStore.put(mediaFile, mediaFile.getName()), mediaFile.getName(), false));
            } else if ("mp4.mov.avi".contains(mediaFile.getName().split("\\.")[1].toLowerCase())) {
                getBoundQuestion().addMedia(new VideoObject(MediaStore.put(mediaFile, mediaFile.getName()), mediaFile.getName(), false));
            }
            updateBtnAddMedia();
        }
    }
    
    private static void updateBtnAddMedia() {
        List<MenuItem> items = Arrays.asList(MediaStore.getEntries().parallelStream().map(e -> {
            ImageObject io = new ImageObject(e, QuizData.getMediaName(e), true);
            ImageView iv = new ImageView(io.getImage());
            iv.setPreserveRatio(true);
            iv.setFitHeight(40);
            MenuItem mi = new MenuItem(QuizData.getMediaName(e), iv);
            mi.setOnAction(evt -> getBoundQuestion().addMedia(io));
            return mi;
        }).toArray(MenuItem[]::new));
        ObservableList<MenuItem> oldItems = btnAddMedia.getItems();
        if(oldItems.size() != items.size() || !oldItems.containsAll(items)) {
            oldItems.clear();
            oldItems.addAll(items);
        }
    }
    
    private static void handleBtnRemoveMediaAction(ActionEvent e) {
        ObservableList<Integer> indices = mediaList.getSelectionModel().getSelectedIndices();
        indices.forEach(index -> getBoundQuestion().getMedia().remove((int) index));
    }   
    
    private static void setupFileChooser(FileChooser.ExtensionFilter... ef) {
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().addAll(ef);
        fileChooser.setSelectedExtensionFilter(ef[0]);
    }

    public static void setBoundQuestion(TextQuestion q) {
        boundQuestion.setValue(q);
    }
    
    public static TextQuestion getBoundQuestion() {
        return boundQuestion.get();
    }
    
    public static SimpleObjectProperty<TextQuestion> boundQuestionProperty() {
        return boundQuestion;
    }
}
