package de.mafel.nawilauncher.data;

import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.net.plickers.PlickersClass;
import de.mafel.nawilauncher.scenes.components.TeamLabel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

/**
 * Beschreibt ein Teilnehmer-Team.
 *
 * @author Matti, Felix
 */
public class Team {

    private final StringProperty name = new SimpleStringProperty();
    private int score = 0;
    private Color color;
    private String plickersPollId = null;
    private final ObjectProperty<Object> answer = new SimpleObjectProperty<>();
    private final ObjectProperty<PlickersClass> plickersClass = new SimpleObjectProperty<>();
    
    
    /**
     * Erstellt ein neues Team mit dem übergebenen Teamnamen und der übergebenen
     * Farbe.
     *
     * @param name Teamname
     * @param color Teamfarbe
     */
    public Team(String name, String color) {
        this.name.set(name);
        try {
            this.color = Color.web(color);
        } catch(IllegalArgumentException | NullPointerException ex) {
            this.color = Color.WHITE;
        }
    }

    /**
     * Gibt die aktuelle Punktzahl des Teams zurück.
     *
     * @return
     */
    public int getScore() {
        return score;
    }

    /**
     * Aktualisiert die Punktzahl des Teams. Bei Aufruf dieser Funktion wird die
     * neue Punktzahl automatisch an eventuell verbundene Web-Clients gesendet.
     *
     * @param score
     */
    public void setScore(int score) {
        this.score = score;
        if(Main.getServer().getState()) Main.getServer().getWebSocketServer().sendScore(this);
    }
    
    /**
     * Gibt den Namen des Teams zurück.
     *
     * @return
     */
    public String getName() {
        return name.get();
    }

    /**
     * Aktualisiert den Namen des Teams.
     *
     * @param value
     */
    public void setName(String value) {
        name.set(value);
    }

    /**
     * Property, die den Namen des Teams beschreibt.
     *
     * @return
     */
    public StringProperty nameProperty() {
        return name;
    }

    /**
     * Gibt die Teamfarbe als String zurück.
     *
     * @return Teamfarbe als String
     */
    public String getColorString() {
        return color.toString();
    }
    
    /**
     * Gibt die Spielfarbe des Teams zurück.
     *
     * @return Spielfarbe
     */
    public Color getColor() {
        return color;
    }

    /**
     * Aktualisiert die Spielfarbe des Teams.
     *
     * @param color Spielfarbe
     */
    public void setColor(Color color) {
        this.color = color;
    }
    
    /**
     * Fügt die übergebene Anzahl an Punkten hinzu.
     *
     * @param scoreDiff Punktedifferenz
     */
    public void addScore(int scoreDiff) {
        this.score += scoreDiff;
        if(Main.getServer().getState()) Main.getServer().getWebSocketServer().sendScore(this);
    }

    /**
     * Gibt die gegebene Antwort des Teams zurück. Wenn keine Antwort gegeben
     * wurde, wird null zurückgegeben. Der Typ der Antwort hängt vom Typ der
     * aktiven Frage ab.
     *
     * @return
     */
    public Object getAnswer() {
        return answer.get();
    }

    /**
     * Legt die Antwort des Teams fest.
     *
     * @param value
     */
    public void setAnswer(Object value) {
        answer.set(value);
    }

    /**
     * Gibt eine ObjectProperty zurück, welche ein Object enthält, welches die
     * Antwort des Teams auf die aktuelle Frage enthält.
     * 
     * Der Typ der Antwort hängt von der aktuellen Fragenklasse ab:
     * ABCDQuestion - Integer
     * TextQuestion - String
     * EstimateQuestion - Double
     * SequenceQuestion - List aus Strings
     *
     * @return ObjectProperty, welche die Antwort beschreibt
     */
    public ObjectProperty answerProperty() {
        return answer;
    }
    
    /**
     * Gibt true zurück, wenn das Team bereits eine Antwort auf die aktuelle Frage
     * abgegeben hat.
     *
     * @return
     */
    public boolean hasAnswered() {
        return getAnswer() != null;
    }

    /**
     * Gibt ein TeamLabel, welches das aktuelle Team darstellt, zurück.
     *
     * @return TeamLabel
     * @see TeamLabel TeamLabel
     */
    public TeamLabel getLabel() {
        return new TeamLabel(this);
    }
    
    public PlickersClass getPlickersClass() {
        return plickersClass.get();
    }

    public void setPlickersClass(PlickersClass value) {
        plickersClass.set(value);
    }

    public ObjectProperty plickersClassProperty() {
        return plickersClass;
    }

    public String getPlickersPollId() {
        return plickersPollId;
    }

    public void setPlickersPollId(String plickersPollId) {
        this.plickersPollId = plickersPollId;
    }
    
}
