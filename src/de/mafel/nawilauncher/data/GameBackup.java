package de.mafel.nawilauncher.data;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.fileio.NWQFormat;
import de.mafel.nawilauncher.scenes.Scenes;
import de.mafel.nawilauncher.scenes.components.ExceptionDialog;
import java.io.File;
import java.util.NoSuchElementException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Zuständig für das automatische Erstellen von Backups des aktuellen
 * Spielstands. Im Falle eines schwerwiegenden Fehlers/Absturzes kann der
 * zuletzt gesicherte Spielstand wiederhergestellt werden.
 * 
 * Die Backups werden in den Java Preferences gespeichert.
 *
 * @author Felix
 * @see Preferences Java Preferences
 * @see GameBackup#backupGame() Methode zum Anlegen eines Backups
 * @see GameBackup#restoreGame() Methode zum Wiederherstellen eines Backups
 */
public class GameBackup {

    /**
     * Erstellt ein Backup des aktuellen Spielstandes und speichert es in den
     * Java Preferences.
     * 
     * Das erstellte Backup kann mittels der <code>restoreBackup()</code>
     * Methode wieder abgerufen werden.
     *
     * @throws NullPointerException Fehler beim Zugreifen auf den aktuellen Spielstand oder die Java Preferences
     * @throws BackingStoreException Fehler beim Schreiben in die Java Preferences
     */
    public static void backupGame() throws NullPointerException, BackingStoreException {
        Preferences prefs = Settings.clearAndGetBackup(),
                teamsNode = prefs.node("teams"),
                questions = prefs.node("questions");
        prefs.put("filename", NWQFormat.isFileOpened() ? NWQFormat.getOpenFile().getAbsolutePath() : "null");
        prefs.put("quiztitle", QuizData.getQuizTitle());
        TeamManager.forEachTeam(t -> {
            Preferences teamNode = teamsNode.node(t.getName());
            teamNode.putInt("score", t.getScore());
            teamNode.put("color", t.getColorString());
            teamNode.putBoolean("active", TeamManager.getActiveTeam().equals(t));
            teamNode.putBoolean("activeForAll", TeamManager.getActiveForAllTeam().equals(t));
        });
        Navigator.getQuestionsDone().forEach(qi -> {
            questions.putInt(qi.getCategory().getId() + "#" + qi.getScore(), 1);
        });
        prefs.putInt("round", TeamManager.getRound());
        prefs.putBoolean("server", Main.getServer().getState());
        prefs.flush();
    }
    
    /**
     * Löscht den aktuellen Backupstand. Nicht umkehrbar.
     *
     */
    public static void clear() {
        Settings.clearAndGetBackup();
    }
    
    /**
     * Stellt den zuletzt gespeicherten Spielstand wieder her.
     *
     * @throws Exception Kein gespeicherter Spielstand gefunden oder Fehler beim Lesen des Spielstandes
     * @see GameBackup#checkBackup() 
     */
    public static void restoreGame() throws Exception {
        Preferences prefs = Settings.getBackup();
        String filename = prefs.get("filename", null);
        if(filename == null) throw new Exception("Der Spielstand kann leider nicht wiederhergestellt werden.", new NullPointerException("Der Dateiname ('filename') wurde nicht gespeichert."));
        NWQFormat.openFile(new File(filename)).join();
        if(!prefs.get("quiztitle", QuizData.getQuizTitle()).equals(QuizData.getQuizTitle())) LogHelper.warning("Warnung: Der im Backup gespeicherte Quiztitel unterscheidet sich vom Titel des geladenen Quiz oder ist nicht definiert.");
        if(!prefs.nodeExists("teams")) throw new Exception("Die Teams konnten nicht wiederhergestellt werden.", new IllegalStateException("Die Node 'backup/teams' existiert nicht."));
        Preferences teamsNode = prefs.node("teams");
        for(String teamName : teamsNode.childrenNames()) {
            Preferences tp = teamsNode.node(teamName);
            Team t = new Team(teamName, tp.get("color", "white"));
            t.setScore(tp.getInt("score", 0));
            TeamManager.addTeam(t);
            if(tp.getBoolean("active", false)) TeamManager.setActiveTeam(t);
            if(tp.getBoolean("activeForAll", false)) TeamManager.setActiveForAllTeam(t);
        }
        if(!prefs.nodeExists("questions")) throw new Exception("Die bereits erledigten Fragen konnten nicht wiederhergestellt werden. Das restliche Spiel wurde wiederhergestellt.", new IllegalStateException("Die Node 'backup/questions' existiert nicht."));
        Preferences questionsNode = prefs.node("questions");
        for(String key : questionsNode.keys()) {
            try {
                int lastHashtag = key.lastIndexOf("#");
                String cat = key.substring(0, lastHashtag);
                int sc = Integer.parseInt(key.substring(lastHashtag + 1));
                Category category = QuizData.getCategories().stream().filter(c -> c.getId().equals(cat)).findAny().get();
                Navigator.markQuestionDone(new QuestionIdentifier(category, sc));
            } catch(NumberFormatException ex) {
                LogHelper.warning("Warnung: Ungültige Punktzahl bei erledigter Frage mit dem Identifier " + key + ". Frage übersprungen.");
            } catch(NoSuchElementException ex) {
                LogHelper.warning("Warnung: Ungültige Kategorie bei erledigter Frage mit dem Identifier " + key + ". Frage übersprungen.");
            } catch(IndexOutOfBoundsException ex) {
                LogHelper.warning("Warnung: Ungültiger QuestionIdentifier bei erledigter Frage mit dem Identifier " + key + ". Frage übersprungen.");
            }
        }
        TeamManager.setRound(prefs.getInt("round", 1));
        boolean serverState = prefs.getBoolean("server", true);
        if(!Main.getServer().getState() && serverState) Main.getServer().start();
        else if(Main.getServer().getState() && !serverState) Main.getServer().stop();
        Scenes.changeScene("scenes/CategoryScene.fxml", true);
    }
    
    /**
     * Überprüft, ob ein Backup gemacht wurde (das deutet auf ein unerwartetes
     * Spielende bzw Programmabsturz hin), fragt ggf den Nutzer, ob das Spiel
     * wiederhergestellt werden soll und stellt den Spielstand ggf wieder her.
     * 
     * Antwortet der Nutzer mit "Nein", wird das Backup gelöscht.
     * 
     * @see GameBackup#restoreGame() 
     *
     */
    public static void checkBackup() {
        if(Settings.getBackup().get("filename", null) != null && askIfRestoreBackup()) try {
            restoreGame();
        } catch (Exception ex) {
            new ExceptionDialog(ex, "Fehler beim Laden des Backups.");
        }
        else clear();
    }
    
    private static boolean askIfRestoreBackup() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Das letzte Spiel wurde unerwartet beendet. Soll der Spielstand wiederhergestellt werden?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("NaWiLauncher");
        alert.setHeaderText("Unerwartetes Spielende");
        return alert.showAndWait().get() == ButtonType.YES;
    }
    
}
