package de.mafel.nawilauncher.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Repräsentiert eine vom Nutzer erstellte Quizkategorie.
 * 
 * Besitzt einen bestimmten, eindeutigen Namen (ID). Sind die IDs zweier Kategorien äquivalent, werden sie als gleich angesehen.
 *
 * @see Category#getId() Category.getId()
 * @author Matti, Felix
 */
public class Category {
    
    private String id;
    
    //true=Frage an alle
    private final BooleanProperty type = new SimpleBooleanProperty(false);
    
    /**
     * Erzeugt eine neue Kategorie mit dem übergebenen eindeutigen Bezeichner
     * (Namen).
     *
     * @param id Eindeutiger Bezeichner
     */
    public Category(String id) {
        this.id = id;
    }

    /**
     * Gibt den eindeutigen Bezeichner (Namen) dieser Kategorie zurück.
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Gibt zurück, ob es sich um eine Frage für alle handelt.
     * 
     * true = Frage für alle
     * false = Normale Frage
     *
     * @return Fragetyp
     */
    public boolean getType() {
        return type.get();
    }

    /**
     * Legt fest, ob es sich um eine Frage für alle handelt.
     * 
     * true = Frage für alle
     * false = Normale Frage
     * 
     * @param value
     */
    public void setType(boolean value) {
        type.set(value);
    }

    /**
     * Gibt zurück, ob es sich um eine Frage für alle handelt.
     * 
     * true = Frage für alle
     * false = Normale Frage
     *
     * @return Fragetyp-Property
     */
    public BooleanProperty typeProperty() {
        return type;
    }  

    /**
     * Ändert den eindeutigen Bezeichner (Namen) dieser Kategorie.
     * 
     * Es findet keine Überprüfung auf Eindeutigkeit des Bezeichners statt.
     *
     * @param id Neuer Bezeichner
     */
    public void updateId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Category && this.getId().equals(((Category) obj).getId());
    }
    
    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public String toString() {
        return id;
    }
    
}
