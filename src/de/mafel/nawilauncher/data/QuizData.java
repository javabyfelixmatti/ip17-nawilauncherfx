package de.mafel.nawilauncher.data;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.data.questions.ABCDQuestion;
import de.mafel.nawilauncher.data.questions.Question;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Stream;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.image.Image;

/**
 * Rein statische Klasse, welche quizbezogene Daten speichert.
 *
 * @author Matti, Felix
 */
public class QuizData {

    private static String quizTitle = "", quizSubtitle = "", quizAuthor = "", grade = "";
    private static Image quizIcon = null;
    private static Date lastEdited = new Date();
    private static ObservableList<Category> categories = FXCollections.observableArrayList();
    private static ObservableList<Integer> scores = FXCollections.observableArrayList();
    private static HashMap<QuestionIdentifier, Question> questions = new HashMap<>();
    private static final String RULES_NAWIGATOR = "- Jedes Team spielt mindestens 3 Aufgaben (Runden) gegen die anderen Teams. Nach jeder Teamrunde folgt eine Runde an alle.\n" +
"- Der Reihe nach wählen die Teams eine Kategorie und eine Punktzahl aus. Das aktive Team wird von der Software ausgewählt.\n" +
"- Alle Teams erhalten die Frage, welche vom Moderator vorgelesen wird, an der Tafel zu sehen ist und optional an den Geräten der Schüler angeschaut wird.\n" +
"- Bei einigen Fragen werden Videos, Audiodateien oder Bilder angezeigt und vorgespielt\n" +
"- Anschließend lösen alle Teams die Aufgabe\n" +
"- Sobald ein Team fertig ist haben die anderen Gruppen noch 30 Sekunden Zeit. Solange kein Team geantwortet hat gilt die vorher festgelegte Zeit. (gilt nur für aktivierten Timer!)\n" +
"- Ist die Zeit abgelaufen können über die eigenen Geräte der Schüler keine Antworten mehr abgegeben werden.\n" +
"- Bei Teamrunden gilt folgende Zählweise:\n" +
"--> das Team, welches die Frage ausgewählt hat bekommt Punkte wenn es die Aufgabe richtig gelöst hat.\n" +
"--> alle anderen Teams können nur Punkte bekommen wenn die Lösung des \"Auswahlteams\" falsch ist und ihre richtig.\n" +
"- Bei \"Aufgaben an Alle\" bekommen alle Punkte, die die Aufgabe richtig gelöst haben.";
    private static final String RULES_SIMPLE = "- Jedes Team spielt mindestens 3 Aufgaben (Runden) gegen die anderen Teams.\n" +
"- Der Reihe nach wählen die Teams eine Kategorie und eine Punktzahl aus. Das aktive Team wird von der Software ausgewählt.\n" +
"- Alle Teams erhalten die Frage, welche vom Moderator vorgelesen wird, an der Tafel zu sehen ist und optional an den Geräten der Schüler angeschaut wird.\n" +
"- Bei einigen Fragen werden Videos, Audiodateien oder Bilder angezeigt und vorgespielt\n" +
"- Anschließend lösen alle Teams die Aufgabe\n" +
"- Sobald ein Team fertig ist haben die anderen Gruppen noch 30 Sekunden Zeit. Solange kein Team geantwortet hat gilt die vorher festgelegte Zeit. (gilt nur für aktivierten Timer!)\n" +
"- Ist die Zeit abgelaufen können über die eigenen Geräte der Schüler keine Antworten mehr abgegeben werden.\n" +
"- alle Teams, die die Aufgabe richtig gelöst haben bekommen die entsprechenden Punkte";
    private static final ObjectProperty<ScoreSystem> scoreSystem = new SimpleObjectProperty<>(ScoreSystem.SIMPLE);
    private static final ObjectProperty<QuestionsForAllMode> questionsForAllMode = new SimpleObjectProperty<>(QuestionsForAllMode.ONE_ANSWER_PER_TEAM);
    private static final ObjectProperty<QuestionForAllHandling> questionForAllHandling = new SimpleObjectProperty<>(QuestionForAllHandling.CHOOSE_AT_EVERY_TIME);
    private static ObservableMap<String, String> mediaNames = FXCollections.observableHashMap();
    private static boolean showRules = true, autoTimerEnd = false, webDisplayScore = false;
    
    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">

    /**
     * Gibt den Titel des Quiz zurück.
     * 
     * Verpflichtende Angabe.
     *
     * @return Quiztitel
     */
    public static String getQuizTitle() {
        return quizTitle;
    }
    
    /**
     * Setzt den Titel des Quiz.
     * 
     * Verpflichtende Angabe.
     *
     * @param quizTitle Quiztitel
     */
    public static void setQuizTitle(String quizTitle) {
        QuizData.quizTitle = quizTitle;
    }
    
    /**
     * Gibt den Untertitel des Quiz zurück.
     * 
     * Optionale Angabe.
     *
     * @return Quizuntertitel
     */
    public static String getQuizSubtitle() {
        return quizSubtitle;
    }
    
    /**
     * Setzt den Untertitel des Quiz.
     * 
     * Optionale Angabe.
     *
     * @param quizSubtitle Untertitel des Quiz
     */
    public static void setQuizSubtitle(String quizSubtitle) {
        QuizData.quizSubtitle = quizSubtitle;
    }
    
    /**
     * Gibt den Autor des Quiz zurück.
     * 
     * Optionale Angabe.
     *
     * @return Quizautor
     */
    public static String getQuizAuthor() {
        return quizAuthor;
    }
    
    /**
     * Setzt den Autor des Quiz.
     * 
     * Optionale Angabe.
     *
     * @param quizAuthor Quizautor
     */
    public static void setQuizAuthor(String quizAuthor) {
        QuizData.quizAuthor = quizAuthor;
    }
    
    /**
     * Gibt die Klassenstufe zurück, für die das Quiz konzipiert wurde.
     * 
     * Optionale Angabe.
     *
     * @return Anvisierte Klassenstufe des Quiz
     */
    public static String getGrade() {
        return grade;
    }
    
    /**
     * Setzt die Klassenstufe, für die das Quiz konzipiert wurde.
     * 
     * Optionale Angabe.
     *
     * @param grade Anvisierte Klassenstufe des Quiz
     */
    public static void setGrade(String grade) {
        QuizData.grade = grade;
    }
    
    /**
     * Gibt das dem Quiz zugewiesene Icon zurück oder null, falls kein Icon existiert.
     * 
     * Optionale Angabe.
     *
     * @return Icon
     */
    public static Image getQuizIcon() {
        return quizIcon;
    }
    
    /**
     * Setzt das dem Quiz zugewiesene Icon. Darf null sein.
     * 
     * Optionale Angabe.
     *
     * @param quizIcon Icon
     */
    public static void setQuizIcon(Image quizIcon) {
        QuizData.quizIcon = quizIcon;
    }
    
    /**
     * Gibt das Datum zurück, an dem zuletzt Änderungen am Quiz vorgenommen wurden.
     *
     * @return Änderungsdatum
     */
    public static Date getLastEdited() {
        return lastEdited;
    }
    
    /**
     * Setzt das Änderungsdatum auf den übergebenen Wert.
     * 
     * Diese Funktion sollte nur verwendet werden, um das Änderungsdatum
     * zu aktualisieren, wenn Änderungen am Quiz vorgenommen wurden.
     *
     * @param lastEdited Änderungsdatum
     */
    public static void setLastEdited(Date lastEdited) {
        QuizData.lastEdited = lastEdited;
    }
    
    /**
     * Fügt dem Quiz alle übergebenen Kategorien hinzu.
     * 
     * Bestehende Kategorien werden <b>gelöscht</b>.
     *
     * @param categories Collection mit Kategorien
     */
    public static void setCategories(Collection<Category> categories) {
        categories.clear();
        categories.addAll(categories);
    }
    
    /**
     * Gibt eine ObservableList zurück, die alle Kategorien enthält.
     *
     * @return Kategorien als ObservableList
     */
    public static ObservableList<Category> getCategories() {
        return categories;
    }
    
    /**
     * Gibt eine ObservableList zurück, die alle Punktzahlen enthält.
     *
     * @return Punktzahlen als ObservableList
     */
    public static ObservableList<Integer> getScores() {
        return scores;
    }
    
    /**
     * Fügt dem Quiz alle übergebenen Punktzahlen hinzu.
     * 
     * Bestehende Punktzahlen werden <b>gelöscht</b>.
     *
     * @param scores Collection mit Punktzahlen
     */
    public static void setScores(Collection<Integer> scores) {
        scores.clear();
        scores.addAll(scores);
    }

    /**
     * Gibt eine Map zurück, die jedem QuestionIdentifier die passende Question
     * zuweist.
     * 
     * Die Größe der Map entspricht dem Produkt aus Kategorien und Punktzahlen.
     *
     * @return Map mit Questions
     */
    public static HashMap<QuestionIdentifier, Question> getQuestions() {
        return questions;
    }

    /**
     * Überschreibt alle Fragen mit den übergebenen.
     * 
     * Die QuestionIdentifier müssen zu den vorhandenen Kategorien und
     * Punktzahlen passen. Dieser Umstand wird nicht überprüft, die
     * Nichteinhaltung dieser Bedingung kann jedoch zu schwerwiegenden Fehlern
     * im Programm führen.
     *
     * @param questions Map mit Questions
     */
    public static void setQuestions(HashMap<QuestionIdentifier, Question> questions) {
        QuizData.questions = questions;
    }

    /**
     * Gibt die Regeln des aktiven Spielmodus zurück.
     *
     * @return Regeln
     */
    public static String getRules() {
        return getScoreSystem().equals(ScoreSystem.NAWIGATOR) ? RULES_NAWIGATOR : RULES_SIMPLE;
    }

    /**
     * Gibt zurück, ob das Anzeigen von Regeln aktiviert ist.
     *
     * @return <code>true</code>, wenn aktiviert, sonst <code>false</code>
     */
    public static boolean isShowRules() {
        return showRules;
    }

    /**
     * Setzt die Einstellung, ob das Anzeigen von Regeln aktiviert sein soll.
     * 
     * @param showRules Wert
     */
    public static void setShowRules(boolean showRules) {
        QuizData.showRules = showRules;
    }

    /**
     * Gibt zurück, ob das Ablaufen des Timers die Antwortphase automatisch
     * beenden soll.
     *
     * @return <code>true</code>, wenn aktiviert, sonst <code>false</code>
     */
    public static boolean isAutoTimerEnd() {
        return autoTimerEnd;
    }

    /**
     * Setzt die Einstellung, ob das Ablaufen des Timers die Antwortphase
     * automatisch beenden soll.
     *
     * @param autoTimerEnd Wert
     */
    public static void setAutoTimerEnd(boolean autoTimerEnd) {
        QuizData.autoTimerEnd = autoTimerEnd;
    }

    /**
     * Gibt zurück, ob den Teams im Web Interface ihre eigene Punktzahl
     * angezeigt werden soll.
     *
     * @return <code>true</code>, wenn aktiviert, sonst <code>false</code>
     */
    public static boolean isWebDisplayScore() {
        return webDisplayScore;
    }

    /**
     * Setzt die Einstellung, ob den Teams im Web Interface ihre eigene Punktzahl
     * angezeigt werden soll.
     *
     * @param webDisplayScore Wert
     */
    public static void setWebDisplayScore(boolean webDisplayScore) {
        QuizData.webDisplayScore = webDisplayScore;
    }

    /**
     * Gibt das Punktesystem zurück.
     * 
     * @return ScoreSystem
     * @see ScoreSystem QuizData.ScoreSystem
     */
    public static ScoreSystem getScoreSystem() {
        return scoreSystem.get();
    }

    /**
     * Setzt das Punktesystem.
     * 
     * @param value
     * @see ScoreSystem QuizData.ScoreSystem
     */
    public static void setScoreSystem(ScoreSystem value) {
        scoreSystem.set(value);
    }
    
    /**
     * Setzt das Punktesystem anhand eines Index.
     * 
     * @param scoreSystem
     * @see ScoreSystem QuizData.ScoreSystem
     */
    public static void setScoreSystem(int scoreSystem) {
        QuizData.scoreSystem.set(ScoreSystem.values()[scoreSystem]);
    }

    /**
     * Gibt die Property des Punktesystems zurück.
     *
     * @see ScoreSystem QuizData.ScoreSystem
     * @return Property des Punktesystems
     */
    public static ObjectProperty scoreSystemProperty() {
        return scoreSystem;
    }
    
    /**
     * Gibt das Punktesystem als Integer zurück.
     * 
     * Äquivalent zu <code>getScoreSystem().ordinal()</code>
     *
     * @return Punktesystem
     * @see ScoreSystem QuizData.ScoreSystem
     */
    public static int getScoreSystemInt() {
        return getScoreSystem().ordinal();
    }
    
    /**
     * Gibt den Modus für die Beantwortung von Fragen für alle zurück.
     * 
     * Nur relevant, wenn der Spielmodus NAWIGATOR aktiviert ist.
     *
     * @return "Frage für alle"-Modus
     * @see QuestionsForAllMode QuizData.QuestionsForAllMode
     * @see ScoreSystem#NAWIGATOR ScoreSystem.NAWIGATOR
     */
    public static QuestionsForAllMode getQuestionsForAllMode() {
        return questionsForAllMode.get();
    }
    
    /**
     * Gibt den Modus für die Beantwortung von Fragen für alle als Integer zurück.
     * 
     * Nur relevant, wenn der Spielmodus NAWIGATOR aktiviert ist.
     * Äquiuvalent zu <code>getQuestionsForAllMode().ordinal()</code>
     *
     * @return "Frage für alle"-Modus
     * @see QuestionsForAllMode QuizData.QuestionsForAllMode
     * @see ScoreSystem#NAWIGATOR ScoreSystem.NAWIGATOR
     */
    public static int getQuestionsForAllModeInt() {
        return getQuestionsForAllMode().ordinal();
    }

    /**
     * Setzt den Modus für die Beantwortung von Fragen für alle.
     * 
     * Nur relevant, wenn der Spielmodus NAWIGATOR aktiviert ist.
     *
     * @param value "Frage für alle"-Modus
     * @see QuestionsForAllMode QuizData.QuestionsForAllMode
     * @see ScoreSystem#NAWIGATOR ScoreSystem.NAWIGATOR
     */
    public static void setQuestionsForAllMode(QuestionsForAllMode value) {
        questionsForAllMode.set(value);
    }
    
    /**
     * Setzt den Modus für die Beantwortung von Fragen für alle anhand eines Integers.
     * 
     * Nur relevant, wenn der Spielmodus NAWIGATOR aktiviert ist.
     * Äquivalent zu <code>setQuestionsForAllMode(QuizData.QuestionsForAllMode.values()[questionsForAllMode])</code>.
     *
     * @param questionsForAllMode "Frage für alle"-Modus als Integer
     * @see QuestionsForAllMode QuizData.QuestionsForAllMode
     * @see ScoreSystem#NAWIGATOR ScoreSystem.NAWIGATOR
     */
    public static void setQuestionsForAllMode(int questionsForAllMode) {
        QuizData.questionsForAllMode.set(QuestionsForAllMode.values()[questionsForAllMode]);
    }

    /**
     * Gibt die Property des Modus für die Beantwortung von Fragen für alle zurück.
     * 
     * Nur relevant, wenn der Spielmodus NAWIGATOR aktiviert ist.
     *
     * @return "Frage für alle"-Modus Property
     * @see QuestionsForAllMode QuizData.QuestionsForAllMode
     * @see ScoreSystem#NAWIGATOR ScoreSystem.NAWIGATOR
     */
    public static ObjectProperty questionsForAllModeProperty() {
        return questionsForAllMode;
    }

    /**
     * Gibt das Handling für Fragen für alle (jederzeit oder nach jeder Runde)
     * zurück.
     *
     * @return Handling Fragen für alle
     * @see QuestionForAllHandling
     */
    public static QuestionForAllHandling getQuestionForAllHandling() {
        return questionForAllHandling.get();
    }

    /**
     * Gibt einen Integer zurück, der das Handling für Fragen für alle
     * beschreibt.
     * 0: jederzeit
     * 1: nach jeder Runde
     *
     * @return Handling Fragen für alle
     */
    public static int getQuestionForAllHandlingInt() {
        if (questionForAllHandling.get() == QuestionForAllHandling.CHOOSE_AT_EVERY_TIME) return 0;
        else return 1;
    }
    
    /**
     * Legt das Handling von Fragen für alle fest.
     *
     * @param value Handling von Fragen für alle
     * @see QuestionForAllHandling
     */
    public static void setQuestionForAllHandling(QuestionForAllHandling value) {
        questionForAllHandling.set(value);
    }
    
    /**
     * Legt das Handling von Fragen für alle fest.
     * 0: jederzeit auswählbar
     * 1: nach jeder Runde
     *
     * @param value Handling von Fragen für alle
     */
    public static void setQuestionForAllHandling(int value) {
        if (value == 0 ) questionForAllHandling.set(QuestionForAllHandling.CHOOSE_AT_EVERY_TIME);
        else questionForAllHandling.set(QuestionForAllHandling.CHOOSE_AFTER_EACH_ROUND);
    }

    /**
     * Gibt eine Property zurück, die das Handling von Fragen für alle
     * beschreibt.
     *
     * @return Property Handling von Fragen für alle
     * @see QuestionForAllHandling
     */
    public static ObjectProperty questionForAllHandlingProperty() {
        return questionForAllHandling;
    }    
    
//</editor-fold>

    /**
     * Fügt die übergebene Kategorie hinzu.
     *
     * @param category Neue Kategorie
     */
    
    public static void addCategory(Category category) {
        categories.add(category);
    }    
    
    /**
     * Gibt die wichtigsten gespeicherten Metadaten in die Konsole aus.
     *
     */
    public static void debugOutput() {
        LogHelper.debug("title=" + quizTitle + "&subtitle=" + quizSubtitle + "&author=" + quizAuthor + "&grade=" + grade + "&icon=" + (quizIcon == null ? "none" : "set") + "&lastEdited=" + (lastEdited == null ? "never" : lastEdited.toString()) + "&categories=" + categories.toString());
    }
    
    /**
     * Gibt die Frage, die dem übergebenen QuestionIdentifier zugeordnet ist, zurück.
     *
     * @param identifier QuestionIdentifier
     * @return Zugehörige Frage
     */
    public static Question getQuestionFromIdentifier(QuestionIdentifier identifier) {
        return questions.get(identifier);
    }
    
    /**
     * Löscht alle Fragen und weist jeder möglichen Kombination aus den gespeicherten
     * Kategorien und Punktzahlen eine neue Standardfrage des Typs ABCDQuestion zu.
     * 
     * Um bereits existierende Fragen beizubehalten, siehe <code>updateQuestions()</code>
     *
     * @see ABCDQuestion ABCDQuestion
     */
    public static void resetQuestions() {
        questions.clear();
        categories.forEach((category) -> scores.forEach((score) -> {
            questions.put(new QuestionIdentifier(category, score), new ABCDQuestion("Hier Frage eingeben", new String[]{"Antwort A", "Antwort B", "Antwort C", "Antwort D"}, 0, category, score, FXCollections.observableArrayList(), -1, "keine"));
        }));
    }
    
    /**
     * Weist jeder möglichen Kombination aus gespeicherten Kategorien und Punktzahlen
     * eine neue Frage des Typs ABCDQuestion zu. Bereits existierende Fragen werden
     * beibehalten; Fragen mit einer nicht mehr existenten Punktzahl oder Kategorie
     * werden gelöscht.
     * 
     * @see QuizData#resetQuestions() QuizData.resetQuestions()
     */
    public static synchronized void updateQuestions() {
        //Fragen mit nicht mehr vorhandenen Kategorien oder Punktzahlen entfernen
        Object[] keysForRemoval = questions.keySet().stream().filter(qi -> (!categories.contains(qi.getCategory())) || (!scores.contains(qi.getScore()))).toArray();
        //Erst jetzt dürfen die Keys entfernt werden, um eine ConcurrentModificationException zu vermeiden
        for(Object key : keysForRemoval) questions.remove(key);
        //Neue Fragen auffüllen
        categories.stream().forEach((category) -> scores.stream().forEach((score) -> {
            QuestionIdentifier qi = new QuestionIdentifier(category, score);
            if(!questions.containsKey(qi)) questions.put(qi, new ABCDQuestion("Hier Frage eingeben", new String[]{"Antwort A", "Antwort B", "Antwort C", "Antwort D"}, 0, category, score, FXCollections.observableArrayList(), -1, "keine"));
        }));
    }
    
    /**
     * Löscht alle Kategorien und erstellt neue mit den übergebenen Namen.
     *
     * @param categoryStrings ArrayList mit den Namen der neuen Kategorien
     */
    public static void createCategories(ArrayList<String> categoryStrings) {
        categories.clear();
        categories.addAll(categoryStrings.parallelStream().map(c -> new Category(c)).toArray(Category[]::new));
    }
    
    /**
     * Gibt die Frage zurück, welche der übergebenen Kombination aus Kategorie und
     * Punktzahl zugewiesen ist.
     *
     * @param category Kategorie
     * @param score Punktzahl
     * @return Zugewiesene Frage oder null, wenn keine solche Frage gefunden werden kann
     */
    public static Question find(Category category, int score) {
        return questions.get(new QuestionIdentifier(category, score));
    }
    
    /**
     * Gibt einen String zurück, der die Namen aller gespeicherten Kategorien auflistet.
     *
     * @return Kategorien-String
     */
    public static String getCategoriesString() {
        String categoriesString = "";
        if (categories.isEmpty()) return categoriesString;
        categoriesString += categories.get(0).getId();
        for (int i = 1; i < categories.size(); i++) {
            categoriesString += ", " + categories.get(i).getId();
        }
        return categoriesString;
    }
    
    /**
     * Gibt einen String zurück, der alle gespeicherten Punktzahlen auflistet.
     *
     * @return Punktzahlen-String
     */
    public static String getScoresString() {
        String scoresString = "";
        if (scores.isEmpty()) return scoresString;
        scoresString += scores.get(0);
        for (int i = 1; i < scores.size(); i++) {
            scoresString += ", " + scores.get(i);
        }
        return scoresString;
    }
    
    /**
     * Setzt das Datum, an dem das Quiz zuletzt geändert wurde, auf das aktuelle.
     *
     */
    public static void updateLastEdited() {
        lastEdited = new Date();
    }
    
    /**
     * Gibt einen Stream mit allen Questions zurück, die zur übergebenen
     * Kategorie gehören.
     *
     * @param cat Kategorie
     * @return Alle Questions mit Kategorie <code>cat</code>
     */
    public static Stream<Question> getQuestionsForCategory(Category cat) {
        return questions.entrySet().parallelStream().filter(e -> e.getKey().getCategory().equals(cat)).map(e -> e.getValue());
    }
    
    /**
     * Gibt die Kategorie mit dem übergebenen Namen zurück.
     *
     * @param name
     * @return
     */
    public static Category getCategoryForName(String name) {
        return categories.parallelStream().filter(c -> c.getId().equals(name)).findAny().orElse(null);
    }
    
    /**
     * Gibt den zugewiesenen Namen der übergebenen Mediendatei zurück.
     *
     * @param mediaPath Pfad der Mediendatei
     * @return Name der Mediendatei
     */
    public static String getMediaName(String mediaPath) {
        return mediaNames.getOrDefault(mediaPath, mediaPath.split("\\.")[0]);
    }
    
    /**
     * Weist der übergebenen Mediendatei den übergebenen Namen zu.
     *
     * @param mediaPath Pfad der Mediendatei
     * @param mediaName Name der Mediendatei
     */
    public static void setMediaName(String mediaPath, String mediaName) {
        mediaNames.put(mediaPath, mediaName);
    }
    
    /**
     * Repräsentiert das verwendete Punktesystem.
     * 
     * Das Punktesystem entscheidet darüber, welches Team für eine richtige
     * Antwort wie viele Punkte erhält.
     *
     */
    public enum ScoreSystem {

        /**
         * Das Punktesystem des NaWiGator-Wettbewerbs.
         * 
         * Wenn das Team, welches die aktuelle Frage ausgesucht hat, richtig
         * antwortet, bekommt nur dieses die Punkte, alle anderen Teams null
         * Punkte, unabhängig davon, ob sie richtig geantwortet haben. Nur wenn
         * das aktive Team falsch antwortet, bekommen alle anderen Teams, die
         * richtig geantwortet haben, die entsprechende Punktzahl gutgeschrieben.
         *
         * Eine Ausnahme sind die "Fragen für alle"-Kategorien, hier wird das
         * "Einfach"-Punktesystem verwendet
         * 
         * @see ScoreSystem#SIMPLE "Einfach"-Punktesystem
         */
        NAWIGATOR,

        /**
         * Ein einfaches Punktesystem, bei jedes Team, das richtig antwortet,
         * die volle Punktzahl erhält.
         *
         */
        SIMPLE;
    }
    
    /**
     * Gibt an, wie viele Antworten für eine "Frage für alle" pro Team abgegeben
     * werden.
     * 
     * Nur relevant, wenn das NAWIGATOR-Punkesystem aktiv ist. Nur für "Fragen
     * für alle" relevant.
     * 
     * @see ScoreSystem#NAWIGATOR NAWIGATOR-Punktesystem
     *
     */
    public enum QuestionsForAllMode {

        /**
         * Es darf nur eine Antwort pro Team abgegeben werden. Ist diese
         * richtig, erhält das Team die volle Punktzahl.
         *
         */
        ONE_ANSWER_PER_TEAM,

        /**
         * Es darf pro Person eine Antwort abgegeben werden, also mehrere
         * Antworten pro Team. Die Punktzahl ist vom Anteil der richtigen
         * Antworten abhängig.
         *
         */
        MULTIPLE_ANSWERS_PER_TEAM;
    }
    
    /**
     * Gibt an, wann Fragen für alle gewählt werden können.
     *
     */
    public enum QuestionForAllHandling {
        
        /**
         * Aufgaben an Alle werden nicht anders behandelt als andere Fragen und
         * können jederzeit gewählt werden.
         * Das Team mit den meisten richtigen Antworten erhält die 
         * entsprechenden Punkte. Kommt es zu einem Gleichstandm bekommen beide 
         * Teams die Punkte.
         */
        CHOOSE_AT_EVERY_TIME, 
        
        /**
         * Nach jeder Spielrunde gibt es eine Aufgabe an Alle.
         * Das Team, welches diese Frage auswählt, wird in der CategoryScene 
         * angezeigt. Das Team mit den meisten richtigen Antworten erhält 
         * die entsprechenden Punkte. Kommt es zu einem Gleichstand, bekommen 
         * beide Teams die Punkte.
         */
        CHOOSE_AFTER_EACH_ROUND;
    }
        
}
