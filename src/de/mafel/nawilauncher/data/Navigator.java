package de.mafel.nawilauncher.data;

import de.mafel.nawilauncher.LogHelper;
import de.mafel.nawilauncher.Main;
import de.mafel.nawilauncher.data.questions.Question;
import de.mafel.nawilauncher.net.plickers.PlickersNewQuestionRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Rein statische Klasse, die zum Navigieren und zum Speichern diverser GUI-Zustände dient.
 *
 * @author Felix, Matti
 */
public class Navigator {
    
    private static Question activeQuestion = null;
    private static ArrayList<QuestionIdentifier> questionsDone = new ArrayList<>();
    private static HashMap<Team, TeamResult> results = new HashMap<>();
    private static String plickersQuestionId = null;
    private static final ObjectProperty<QuizState> quizState = new SimpleObjectProperty<>();
    private static final BooleanProperty plickersEnabled = new SimpleBooleanProperty();
    
    static {
        quizState.addListener((o, v1, v2) -> LogHelper.debug(v1 + " -> " + v2));
    }

    /**
     * Gibt den aktuellen QuizState zurück.
     *
     * @return Aktueller QuizState
     * @see QuizState QuizState
     * @see Navigator#quizStateProperty() quizStateProperty()
     */
    public static QuizState getQuizState() {
        return quizState.get();
    }

    /**
     * Setzt den QuizState auf den übergebenen Wert.
     *
     * @param value Neuer QuizState
     * @see QuizState QuizState
     * @see Navigator#quizStateProperty() quizStateProperty()
     */
    public static void setQuizState(QuizState value) {
        quizState.set(value);
    }

    /**
     * Gibt die ObjectProperty zurück, welche den aktuellen QuizState enthält.
     *
     * @return Property, welche aktuellen QuizState enthält
     * @see QuizState QuizState
     */
    public static ObjectProperty<QuizState> quizStateProperty() {
        return quizState;
    }
    
    /**
     * Setzt die aktuelle Frage zurück, leert die Liste der schon erledigten Fragen
     * und löscht alle Ergebnisse.
     *
     */
    public static void reset() {
        activeQuestion = null;
        questionsDone.clear();
        results.clear();
    }

    /**
     * Gibt eine HashMap zurück, die jedem Team sein TeamResult zuweist.
     *
     * @return TeamResults
     * @see Team Team
     * @see TeamResult TeamResult
     */
    public static HashMap<Team, TeamResult> getResults() {
        return results;
    }
    
    /**
     * Gibt das Rundenergebnis (TeamResult) des übergebenen Teams zurück.
     *
     * @param t Team
     * @return Rundenergebnis oder null, wenn kein Ergebnis vorliegt
     * @see TeamResult TeamResult
     */
    public static TeamResult getResult(Team t) {
        return results.get(t);
    }

    /**
     * Überschreibt die HashMap, welche jedem Team sein ReamResult zuweist.
     *
     * @param results TeamResults HashMap
     */
    public static void setResults(HashMap<Team, TeamResult> results) {
        Navigator.results = results;
    }
    
    /**
     * Weist einem Team sein TeamResult zu.
     *
     * @param t Team
     * @param result Dem Team zugehöriges TeamResult
     */
    public static void setResult(Team t, TeamResult result) {
        results.put(t, result);
    }

    /**
     * Gibt die aktuell dargestellte/geöffnete Frage zurück.
     *
     * @return Geöffnete Frage
     */
    public static Question getActiveQuestion() {
        return activeQuestion;
    }

    /**
     * Setzt die aktuell dargestellte/geöffnete Frage.
     *
     * @param activeQuestion Geöffnete Frage
     * @see Question Question
     */
    public static void setActiveQuestion(Question activeQuestion) {
        Navigator.activeQuestion = activeQuestion;
    }
    
    /**
     * Setzt die aktuell dargestellte/geöffnete Frage anhand ihres Identifiers.
     *
     * @param identifier Identifier der geöffneten Frage
     * @see QuestionIdentifier QuestionIdentifier
     */
    public static void setActiveQuestion(QuestionIdentifier identifier) {
        setActiveQuestion(QuizData.getQuestionFromIdentifier(identifier));
    }
    
    /**
     * Legt fest, dass aktuell keine Frage geöffnet/dargestellt wird.
     *
     */
    public static void resetActiveQuestion() {
        Navigator.activeQuestion = null;
    }

    /**
     * Gibt eine ArrayList mit allen bereits erledigten Fragen zurück.
     *
     * @return
     */
    public static ArrayList<QuestionIdentifier> getQuestionsDone() {
        return questionsDone;
    }

    /**
     * Setzt die bereits erledigten Fragen.
     *
     * @param questionsDone ArrayList mit bereits erledigten Fragen
     */
    public static void setQuestionsDone(ArrayList<QuestionIdentifier> questionsDone) {
        Navigator.questionsDone = questionsDone;
    }
    
    /**
     * Gibt true zurück, wenn die Frage mit dem übergebenen Identifier bereits erledigt wurde.
     *
     * @param qi QuestionIdentifier
     * @return true, wenn die Frage erledigt wurde
     */
    public static boolean isQuestionDone(QuestionIdentifier qi) {
        return questionsDone.contains(qi);
    }
    
    /**
     * Gibt true zurück, wenn die übergebene Frage bereits erledigt wurde.
     *
     * @param q Frage
     * @return true, wenn die Frage erledigt wurde
     */
    public static boolean isQuestionDone(Question q) {
        return questionsDone.contains(q.toIdentifier());
    }
    
    /**
     * Gibt true zurück, wenn die Frage mit der übergebenen Kategorie und Punktzahl
     * bereits erledig wurde.
     *
     * @param c Kategorie der Frage
     * @param score Punktzahl der Frage
     * @return true, wenn die Frage erledigt wurde
     */
    public static boolean isQuestionDone(Category c, int score) {
        return questionsDone.contains(new QuestionIdentifier(c, score));
    }
    
    /**
     * Legt die übergebene Frage als erledigt fest.
     *
     * @param q Frage
     */
    public static void markQuestionDone(Question q) {
        questionsDone.add(q.toIdentifier());
    }
    
    /**
     * Legt die Frage mit dem übergebenen QuestionIdentifier als erledigt fest.
     *
     * @param qi QuestionIdentifier der Frage
     */
    public static void markQuestionDone(QuestionIdentifier qi) {
        questionsDone.add(qi);
    }
    
    /**
     * Fügt die aktive Frage zu den bereits erledigten Fragen hinzu.
     * 
     * @see Navigator#getActiveQuestion() getActiveQuestion()
     */
    public static void markQuestionDone() {
        questionsDone.add(activeQuestion.toIdentifier());
    }
    
    /**
     * Setzt alle Fragen auf noch nicht erledigt.
     * 
     */
    public static void resetQuestionsDone() {
        questionsDone.clear();
    }
    
    /**
     * Setzt die Ergebnisse (TeamResults) zurück und fügt jedem Team die Punktzahl
     * hinzu, die ihm laut zugehörigem TeamResult zusteht.
     * 
     * @see TeamResult TeamResult
     * @see Navigator#setResult(de.mafel.nawilauncher.data.Team, de.mafel.nawilauncher.data.TeamResult) Navigator.setResult(Team, TeamResult)
     */
    public static void transferScore() {
        results.forEach((team, teamResult) -> team.addScore(teamResult.getScoreDiff()));
        results.clear();
    }

    public static boolean isPlickersEnabled() {
        return plickersEnabled.get();
    }

    public static void setPlickersEnabled(boolean value) {
        plickersEnabled.set(value);
    }

    public static BooleanProperty plickersEnabledProperty() {
        return plickersEnabled;
    }

    public static String getPlickersQuestionId() {
        if(plickersQuestionId == null) createPlickersQuestionId();
        return plickersQuestionId;
    }

    public static void setPlickersQuestionId(String plickersQuestionId) {
        Navigator.plickersQuestionId = plickersQuestionId;
    }
    
    public static boolean createPlickersQuestionId() {
        try {
            PlickersNewQuestionRequest request = new PlickersNewQuestionRequest("NaWiGator " + new SimpleDateFormat("dd.MM.yyyy_HH:mm:ss").format(Calendar.getInstance().getTime()), new String[]{"Antwort", "Antwort", "Antwort", "Antwort"});
            plickersQuestionId = Main.getPlickersClient().queryNewQuestion(request).getId();
            return true;
        } catch(Exception ex) {
            LogHelper.error(ex);
            return false;
        }
    }
    
}
